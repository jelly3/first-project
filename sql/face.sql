/*
 Navicat Premium Data Transfer

 Source Server         : 本地服务器
 Source Server Type    : MySQL
 Source Server Version : 50722
 Source Host           : 127.0.0.1:3306
 Source Schema         : face

 Target Server Type    : MySQL
 Target Server Version : 50722
 File Encoding         : 65001

 Date: 29/06/2021 16:19:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for QRTZ_BLOB_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_BLOB_TRIGGERS`;
CREATE TABLE `QRTZ_BLOB_TRIGGERS` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `blob_data` blob,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`) USING BTREE,
  CONSTRAINT `QRTZ_BLOB_TRIGGERS_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of QRTZ_BLOB_TRIGGERS
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for QRTZ_CALENDARS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_CALENDARS`;
CREATE TABLE `QRTZ_CALENDARS` (
  `sched_name` varchar(120) NOT NULL,
  `calendar_name` varchar(200) NOT NULL,
  `calendar` blob NOT NULL,
  PRIMARY KEY (`sched_name`,`calendar_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of QRTZ_CALENDARS
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for QRTZ_CRON_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_CRON_TRIGGERS`;
CREATE TABLE `QRTZ_CRON_TRIGGERS` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `cron_expression` varchar(200) NOT NULL,
  `time_zone_id` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`) USING BTREE,
  CONSTRAINT `QRTZ_CRON_TRIGGERS_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of QRTZ_CRON_TRIGGERS
-- ----------------------------
BEGIN;
INSERT INTO `QRTZ_CRON_TRIGGERS` VALUES ('RuoyiScheduler', '__TASK_CLASS_NAME__1', 'DEFAULT', '0/10 * * * * ?', 'Asia/Shanghai');
INSERT INTO `QRTZ_CRON_TRIGGERS` VALUES ('RuoyiScheduler', '__TASK_CLASS_NAME__2', 'DEFAULT', '0/15 * * * * ?', 'Asia/Shanghai');
INSERT INTO `QRTZ_CRON_TRIGGERS` VALUES ('RuoyiScheduler', '__TASK_CLASS_NAME__3', 'DEFAULT', '0/20 * * * * ?', 'Asia/Shanghai');
COMMIT;

-- ----------------------------
-- Table structure for QRTZ_FIRED_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_FIRED_TRIGGERS`;
CREATE TABLE `QRTZ_FIRED_TRIGGERS` (
  `sched_name` varchar(120) NOT NULL,
  `entry_id` varchar(95) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `instance_name` varchar(200) NOT NULL,
  `fired_time` bigint(13) NOT NULL,
  `sched_time` bigint(13) NOT NULL,
  `priority` int(11) NOT NULL,
  `state` varchar(16) NOT NULL,
  `job_name` varchar(200) DEFAULT NULL,
  `job_group` varchar(200) DEFAULT NULL,
  `is_nonconcurrent` varchar(1) DEFAULT NULL,
  `requests_recovery` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`sched_name`,`entry_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of QRTZ_FIRED_TRIGGERS
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for QRTZ_JOB_DETAILS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_JOB_DETAILS`;
CREATE TABLE `QRTZ_JOB_DETAILS` (
  `sched_name` varchar(120) NOT NULL,
  `job_name` varchar(200) NOT NULL,
  `job_group` varchar(200) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `job_class_name` varchar(250) NOT NULL,
  `is_durable` varchar(1) NOT NULL,
  `is_nonconcurrent` varchar(1) NOT NULL,
  `is_update_data` varchar(1) NOT NULL,
  `requests_recovery` varchar(1) NOT NULL,
  `job_data` blob,
  PRIMARY KEY (`sched_name`,`job_name`,`job_group`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of QRTZ_JOB_DETAILS
-- ----------------------------
BEGIN;
INSERT INTO `QRTZ_JOB_DETAILS` VALUES ('RuoyiScheduler', '__TASK_CLASS_NAME__1', 'DEFAULT', NULL, 'org.miser.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C770800000010000000017400135F5F5441534B5F50524F504552544945535F5F7372001E6F72672E6D697365722E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E0009787200276F72672E6D697365722E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E078707400007070707400013174000E302F3130202A202A202A202A203F74001172795461736B2E72794E6F506172616D7374000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000001740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E697A0E58F82EFBC8974000133740001317800);
INSERT INTO `QRTZ_JOB_DETAILS` VALUES ('RuoyiScheduler', '__TASK_CLASS_NAME__2', 'DEFAULT', NULL, 'org.miser.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C770800000010000000017400135F5F5441534B5F50524F504552544945535F5F7372001E6F72672E6D697365722E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E0009787200276F72672E6D697365722E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E078707400007070707400013174000E302F3135202A202A202A202A203F74001572795461736B2E7279506172616D7328277279272974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000002740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E69C89E58F82EFBC8974000133740001317800);
INSERT INTO `QRTZ_JOB_DETAILS` VALUES ('RuoyiScheduler', '__TASK_CLASS_NAME__3', 'DEFAULT', NULL, 'org.miser.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C770800000010000000017400135F5F5441534B5F50524F504552544945535F5F7372001E6F72672E6D697365722E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E0009787200276F72672E6D697365722E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E078707400007070707400013174000E302F3230202A202A202A202A203F74003872795461736B2E72794D756C7469706C65506172616D7328277279272C20747275652C20323030304C2C203331362E3530442C203130302974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000003740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E5A49AE58F82EFBC8974000133740001317800);
COMMIT;

-- ----------------------------
-- Table structure for QRTZ_LOCKS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_LOCKS`;
CREATE TABLE `QRTZ_LOCKS` (
  `sched_name` varchar(120) NOT NULL,
  `lock_name` varchar(40) NOT NULL,
  PRIMARY KEY (`sched_name`,`lock_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='程序悲观锁';

-- ----------------------------
-- Records of QRTZ_LOCKS
-- ----------------------------
BEGIN;
INSERT INTO `QRTZ_LOCKS` VALUES ('RuoyiScheduler', 'STATE_ACCESS');
INSERT INTO `QRTZ_LOCKS` VALUES ('RuoyiScheduler', 'TRIGGER_ACCESS');
COMMIT;

-- ----------------------------
-- Table structure for QRTZ_PAUSED_TRIGGER_GRPS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_PAUSED_TRIGGER_GRPS`;
CREATE TABLE `QRTZ_PAUSED_TRIGGER_GRPS` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  PRIMARY KEY (`sched_name`,`trigger_group`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='暂停的trigger';

-- ----------------------------
-- Records of QRTZ_PAUSED_TRIGGER_GRPS
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for QRTZ_SCHEDULER_STATE
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SCHEDULER_STATE`;
CREATE TABLE `QRTZ_SCHEDULER_STATE` (
  `sched_name` varchar(120) NOT NULL,
  `instance_name` varchar(200) NOT NULL,
  `last_checkin_time` bigint(19) NOT NULL,
  `checkin_interval` bigint(19) NOT NULL,
  PRIMARY KEY (`sched_name`,`instance_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='scheduler状态信息';

-- ----------------------------
-- Records of QRTZ_SCHEDULER_STATE
-- ----------------------------
BEGIN;
INSERT INTO `QRTZ_SCHEDULER_STATE` VALUES ('RuoyiScheduler', 'localhost1624850026976', 1624850106117, 15000);
COMMIT;

-- ----------------------------
-- Table structure for QRTZ_SIMPLE_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SIMPLE_TRIGGERS`;
CREATE TABLE `QRTZ_SIMPLE_TRIGGERS` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `repeat_count` bigint(19) NOT NULL,
  `repeat_interval` bigint(19) NOT NULL,
  `times_triggered` bigint(19) NOT NULL,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='简单触发器';

-- ----------------------------
-- Records of QRTZ_SIMPLE_TRIGGERS
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for QRTZ_SIMPROP_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SIMPROP_TRIGGERS`;
CREATE TABLE `QRTZ_SIMPROP_TRIGGERS` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `str_prop_1` varchar(512) DEFAULT NULL,
  `str_prop_2` varchar(512) DEFAULT NULL,
  `str_prop_3` varchar(512) DEFAULT NULL,
  `int_prop_1` int(10) DEFAULT NULL,
  `int_prop_2` int(10) DEFAULT NULL,
  `long_prop_1` bigint(19) DEFAULT NULL,
  `long_prop_2` bigint(19) DEFAULT NULL,
  `dec_prop_1` decimal(13,4) DEFAULT NULL,
  `dec_prop_2` decimal(13,4) DEFAULT NULL,
  `bool_prop_1` varchar(1) DEFAULT NULL,
  `bool_prop_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='存储触发器';

-- ----------------------------
-- Records of QRTZ_SIMPROP_TRIGGERS
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for QRTZ_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_TRIGGERS`;
CREATE TABLE `QRTZ_TRIGGERS` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `job_name` varchar(200) NOT NULL,
  `job_group` varchar(200) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `next_fire_time` bigint(13) DEFAULT NULL,
  `prev_fire_time` bigint(13) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `trigger_state` varchar(16) NOT NULL,
  `trigger_type` varchar(8) NOT NULL,
  `start_time` bigint(13) NOT NULL,
  `end_time` bigint(13) DEFAULT NULL,
  `calendar_name` varchar(200) DEFAULT NULL,
  `misfire_instr` smallint(2) DEFAULT NULL,
  `job_data` blob,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`) USING BTREE,
  KEY `sched_name` (`sched_name`,`job_name`,`job_group`) USING BTREE,
  CONSTRAINT `QRTZ_TRIGGERS_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `QRTZ_JOB_DETAILS` (`sched_name`, `job_name`, `job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of QRTZ_TRIGGERS
-- ----------------------------
BEGIN;
INSERT INTO `QRTZ_TRIGGERS` VALUES ('RuoyiScheduler', '__TASK_CLASS_NAME__1', 'DEFAULT', '__TASK_CLASS_NAME__1', 'DEFAULT', NULL, 1624850030000, -1, 5, 'PAUSED', 'CRON', 1624850027000, 0, NULL, 2, '');
INSERT INTO `QRTZ_TRIGGERS` VALUES ('RuoyiScheduler', '__TASK_CLASS_NAME__2', 'DEFAULT', '__TASK_CLASS_NAME__2', 'DEFAULT', NULL, 1624850040000, -1, 5, 'PAUSED', 'CRON', 1624850027000, 0, NULL, 2, '');
INSERT INTO `QRTZ_TRIGGERS` VALUES ('RuoyiScheduler', '__TASK_CLASS_NAME__3', 'DEFAULT', '__TASK_CLASS_NAME__3', 'DEFAULT', NULL, 1624850040000, -1, 5, 'PAUSED', 'CRON', 1624850027000, 0, NULL, 2, '');
COMMIT;

-- ----------------------------
-- Table structure for face_client_info
-- ----------------------------
DROP TABLE IF EXISTS `face_client_info`;
CREATE TABLE `face_client_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `merchant_id` int(11) unsigned NOT NULL COMMENT '所属商户',
  `client_id` varchar(32) NOT NULL COMMENT '客户端编号',
  `client_name` varchar(40) DEFAULT NULL COMMENT '客户端名称',
  `client_secret` varchar(32) DEFAULT NULL COMMENT '客户端密钥',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态（0：无效，1：有效）',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `create_by` varchar(64) CHARACTER SET utf8 DEFAULT '' COMMENT '创建者',
  `update_by` varchar(64) CHARACTER SET utf8 DEFAULT '' COMMENT '更新者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='客户端表';

-- ----------------------------
-- Records of face_client_info
-- ----------------------------
BEGIN;
INSERT INTO `face_client_info` VALUES (1, 1, '9bf7052cab65d25a', '测试客户端', 'bc5f06d2e10d9be18edbb81d339bd2a2', 1, '测试客户端', '2021-06-28 15:43:58', '2021-06-28 15:44:09', '', '');
COMMIT;

-- ----------------------------
-- Table structure for face_face_info
-- ----------------------------
DROP TABLE IF EXISTS `face_face_info`;
CREATE TABLE `face_face_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `person_id` int(11) NOT NULL COMMENT '人员编号',
  `merchant_id` int(11) NOT NULL COMMENT '商户编号',
  `face_id` varchar(10) NOT NULL COMMENT '人脸数据编号',
  `face_feature` blob COMMENT '人脸特征',
  `face_picture` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT '人脸图片',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态（0：禁用，1：启用）',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_by` varchar(64) CHARACTER SET utf8 DEFAULT '' COMMENT '创建者',
  `update_by` varchar(64) CHARACTER SET utf8 DEFAULT '' COMMENT '更新者',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='人脸数据表';

-- ----------------------------
-- Records of face_face_info
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for face_merchant_info
-- ----------------------------
DROP TABLE IF EXISTS `face_merchant_info`;
CREATE TABLE `face_merchant_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `dept_id` int(11) unsigned NOT NULL COMMENT '机构id',
  `phone` varchar(20) DEFAULT NULL COMMENT '联系电话',
  `contact_name` varchar(30) DEFAULT NULL COMMENT '联系人名称',
  `merchant_name` varchar(100) NOT NULL COMMENT '商户名称',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态（0：无效，1：有效）',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `create_by` varchar(64) CHARACTER SET utf8 DEFAULT '' COMMENT '创建者',
  `update_by` varchar(64) CHARACTER SET utf8 DEFAULT '' COMMENT '更新者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='商户表';

-- ----------------------------
-- Records of face_merchant_info
-- ----------------------------
BEGIN;
INSERT INTO `face_merchant_info` VALUES (1, 100, '18850713988', '李东航', '测试商户', 1, '2021-06-28 15:43:13', NULL, '', '');
COMMIT;

-- ----------------------------
-- Table structure for face_person_info
-- ----------------------------
DROP TABLE IF EXISTS `face_person_info`;
CREATE TABLE `face_person_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `merchant_id` int(11) NOT NULL COMMENT '商户编号',
  `phone` varchar(22) CHARACTER SET utf8 DEFAULT NULL COMMENT '手机号',
  `name` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '姓名',
  `gender` tinyint(1) unsigned DEFAULT NULL COMMENT '性别',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态（0：禁用，1：启用）',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_by` varchar(64) CHARACTER SET utf8 DEFAULT '' COMMENT '创建者',
  `update_by` varchar(64) CHARACTER SET utf8 DEFAULT '' COMMENT '更新者',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1002 DEFAULT CHARSET=utf8mb4 COMMENT='人员表';

-- ----------------------------
-- Records of face_person_info
-- ----------------------------
BEGIN;
INSERT INTO `face_person_info` VALUES (1, 1, '15557829451', '柴犯', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (2, 1, '18185323810', '仲长酃', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (3, 1, '15687067225', '薛喈延', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (4, 1, '13264705598', '臧跌', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (5, 1, '13443239812', '况摄陈', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (6, 1, '14932999818', '边乓糟', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (7, 1, '18525622554', '綦愚攥', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (8, 1, '15557856600', '暨启', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (9, 1, '18125903149', '乌城', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (10, 1, '17586650736', '缪瓢', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (11, 1, '18059094641', '申突', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (12, 1, '19947590262', '时舍嘧', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (13, 1, '18574989491', '缪夼', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (14, 1, '15306578641', '原簇仍', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (15, 1, '18695263874', '伏薹荦', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (16, 1, '15378041989', '康僖薪', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (17, 1, '14950047368', '铁丝菹', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (18, 1, '18784064886', '鱼泛势', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (19, 1, '16624606776', '查坝', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (20, 1, '13112703043', '芮叔埒', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (21, 1, '17237257578', '东方这别', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (22, 1, '18612664055', '赵哇', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (23, 1, '18666019757', '况迭剔', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (24, 1, '13512043725', '柴逆捧', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (25, 1, '15579135186', '仪寞伶', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (26, 1, '17287638763', '太叔水荃', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (27, 1, '17684395687', '巫马废', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (28, 1, '16672027518', '沈圯', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (29, 1, '16688681990', '桓搀涝', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (30, 1, '17550891864', '越崽钉', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (31, 1, '13420146316', '督熊嗅', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (32, 1, '16645223753', '晏殊', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (33, 1, '17696674047', '濮她交', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (34, 1, '15140038856', '融隍', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (35, 1, '15717161774', '闻藩作', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (36, 1, '13381683187', '范雅关', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (37, 1, '14741676080', '公良翼', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (38, 1, '18142059974', '凌洛', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (39, 1, '19981535065', '符泳死', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (40, 1, '14997183488', '皮告', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (41, 1, '13480370730', '宣易啾', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (42, 1, '18991817223', '慕容蒋', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (43, 1, '14546479875', '贾哪摄', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (44, 1, '17676474057', '盖擅', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (45, 1, '17756934105', '单于该', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (46, 1, '18711337562', '宰岜巾', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (47, 1, '13561406717', '东乡蠃谱', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (48, 1, '17868729247', '羊迄欧', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (49, 1, '15358526195', '毋丘匆', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (50, 1, '18681841330', '喻捎', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (51, 1, '18077778168', '钮灯', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (52, 1, '18014771022', '童照赁', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (53, 1, '17791159182', '庾坌俨', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (54, 1, '17199453847', '庞葶急', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (55, 1, '17518120962', '申忒亠', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (56, 1, '15893090250', '法把', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (57, 1, '17774386863', '迟暗', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (58, 1, '18149524794', '毕植王', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (59, 1, '15140482934', '成筷邰', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (60, 1, '18151934114', '柴馘', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (61, 1, '13347272069', '通蓣邵', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (62, 1, '16659933216', '李狨', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (63, 1, '19936099942', '达厚', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (64, 1, '18033363847', '都渗', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (65, 1, '15917781802', '喻烷盖', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (66, 1, '13480632133', '柏穆', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (67, 1, '18188072627', '郜焙豺', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (68, 1, '15251285585', '杨亭', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (69, 1, '15996270032', '阳营祥', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (70, 1, '15059623255', '束硅环', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (71, 1, '14917332912', '厍烂杜', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (72, 1, '18036148661', '邴揠', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (73, 1, '13256310016', '竹烙崽', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (74, 1, '19873349418', '缪庇冢', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (75, 1, '14550130089', '叶慈享', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (76, 1, '15554737903', '燕柠厦', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (77, 1, '13347867443', '平镊奉', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (78, 1, '18515281221', '甄沾垡', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (79, 1, '13736184982', '蓟肤啡', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (80, 1, '17146240542', '桑帅栈', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (81, 1, '18772508796', '鲜于佟江', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (82, 1, '16609462816', '郁耷驶', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (83, 1, '13099296059', '枚琉', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (84, 1, '17771126031', '贾恬圹', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (85, 1, '13794129413', '瞿喧', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (86, 1, '17370064378', '羊舌碍', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (87, 1, '15911835274', '公孙鄹', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (88, 1, '18714379954', '匡韶', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (89, 1, '13067675517', '缑试死', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (90, 1, '16630050187', '鱼徼', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (91, 1, '13697112538', '令狐千龚', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (92, 1, '17846413634', '韶曾息', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (93, 1, '13318102623', '牛挢', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (94, 1, '17853208066', '章麓券', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (95, 1, '15388673105', '荀玲', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (96, 1, '17759679864', '胡母舔', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (97, 1, '15879006745', '奚坞', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (98, 1, '14920413666', '朱凌', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (99, 1, '18103084640', '容墚', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (100, 1, '14757003053', '荆录冲', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (101, 1, '14583481346', '瞿芹诣', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (102, 1, '15528345878', '祭蒇消', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (103, 1, '18986452916', '虎型矾', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (104, 1, '13066034879', '雍门虐纵', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (105, 1, '18800019790', '吉等', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (106, 1, '17508100416', '雍频', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (107, 1, '16650824005', '臧捡', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (108, 1, '16660946436', '仰掇丿', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (109, 1, '17308353384', '木骸篆', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (110, 1, '16623606589', '亓挟莨', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (111, 1, '13672272898', '叔孙撕蹄', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (112, 1, '13002925434', '雷苕', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (113, 1, '13617897370', '莘搐', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (114, 1, '16612486836', '易噗', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (115, 1, '17503980397', '谷梁炼唏', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (116, 1, '18055715939', '段干邓', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (117, 1, '15856354241', '铁钮练', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (118, 1, '18036102249', '车正墉', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (119, 1, '13229608317', '梁丘弄', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (120, 1, '14958901210', '竹火忽', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (121, 1, '13014630931', '毋丘狩诬', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (122, 1, '14972874353', '伊欺馆', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (123, 1, '18678235562', '张廖撵', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (124, 1, '13399592279', '柴滨', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (125, 1, '15920659166', '徐居', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (126, 1, '13353993872', '红唧', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (127, 1, '17820090737', '阚捋', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (128, 1, '18841095721', '尚湛磋', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (129, 1, '14594715279', '能灾', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (130, 1, '15869780433', '繁俦礼', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (131, 1, '17742704814', '邱蛀叮', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (132, 1, '18866255232', '介簧', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (133, 1, '17823051396', '侴旗', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (134, 1, '14956384591', '车正槛', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (135, 1, '15386535155', '畅惑啤', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (136, 1, '13845771542', '戚退', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (137, 1, '13726153780', '佟椿释', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (138, 1, '18694539333', '羊辉蜗', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (139, 1, '16620170488', '杞盏嘟', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (140, 1, '13979915541', '宗码', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (141, 1, '17745581222', '樊柞铰', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (142, 1, '18942911628', '古轰沛', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (143, 1, '15133150729', '阳唾伫', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (144, 1, '15581562353', '郁柔蓣', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (145, 1, '17525306670', '姜队垧', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (146, 1, '18627237988', '公乘愤', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (147, 1, '18014675284', '巢荃', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (148, 1, '18578249325', '司马根斟', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (149, 1, '16610347039', '檀闯忒', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (150, 1, '19950764834', '太史轴隙', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (151, 1, '16648091759', '过避', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (152, 1, '18411660640', '终议秸', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (153, 1, '18900090588', '苏蕨穆', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (154, 1, '15549647960', '潘倌卯', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (155, 1, '18062885939', '伍鼐', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (156, 1, '13006850822', '郗当', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (157, 1, '17347329212', '储村栋', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (158, 1, '13989919154', '余徘', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (159, 1, '15907307114', '还哿侩', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (160, 1, '13357439886', '柏栖情', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (161, 1, '13087914034', '伍桔誓', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (162, 1, '18606924593', '裴侉呕', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (163, 1, '14752978956', '太史劭', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (164, 1, '14958811543', '宋哆挠', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (165, 1, '17675969937', '谷梁崾', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (166, 1, '14512201017', '娄旋', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (167, 1, '17323462189', '鲜约供', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (168, 1, '16660511691', '莫派呓', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (169, 1, '17509280309', '展客', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (170, 1, '17129725098', '茅徜秋', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (171, 1, '15267486030', '潘束', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (172, 1, '16671513249', '宰父凿', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (173, 1, '13092777163', '苍凼', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (174, 1, '15029279546', '成丰寻', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (175, 1, '16690457204', '微生阼', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (176, 1, '15378145489', '查嗑妻', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (177, 1, '15940724013', '姜啕葚', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (178, 1, '19909634019', '时莴衙', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (179, 1, '15957212765', '游淫伧', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (180, 1, '13324264776', '禄揿', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (181, 1, '18117396949', '宗正疏', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (182, 1, '17797493897', '穆焕', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (183, 1, '18118415426', '子车幢', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (184, 1, '13803440028', '贲荧', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (185, 1, '13614331977', '咸炽', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (186, 1, '14569360020', '沈嗬付', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (187, 1, '13410386202', '劳牡差', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (188, 1, '18234530479', '鱼页耗', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (189, 1, '13358106270', '迟伫唯', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (190, 1, '18996433932', '车正衅钉', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (191, 1, '15722135455', '胶糊', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (192, 1, '19900893895', '陆怠脓', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (193, 1, '18131294480', '张廖酉', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (194, 1, '14511988077', '穆歹', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (195, 1, '18779946201', '黄旷', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (196, 1, '18995828781', '闻人嘬邓', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (197, 1, '15815700353', '于薯湛', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (198, 1, '15248514996', '步夯汾', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (199, 1, '15583881136', '纪票', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (200, 1, '17777110027', '杨接帕', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (201, 1, '17709637552', '赵锅巫', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (202, 1, '18954047478', '计嗉拘', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (203, 1, '19948677890', '屈觅耪', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (204, 1, '18970112892', '陈搪', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (205, 1, '13411065387', '国帮迹', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (206, 1, '14727101925', '费铜', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (207, 1, '19911593228', '纵惑狭', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (208, 1, '13805285589', '劳冉', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (209, 1, '18951682023', '叶寅蘑', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (210, 1, '17224326863', '端木货偷', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (211, 1, '15322944410', '公良铬', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (212, 1, '15120162120', '沙茎譬', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (213, 1, '19974775988', '解撙', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (214, 1, '13714964151', '程虑', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (215, 1, '16603984498', '兀官狒邹', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (216, 1, '18777886295', '邰苇', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (217, 1, '13224922871', '殳微双', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (218, 1, '15273455763', '魏邢', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (219, 1, '13893793112', '党巽', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (220, 1, '13344292323', '漆雕芄玲', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (221, 1, '16601716525', '贡郧袤', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (222, 1, '13294905768', '胡母伢垂', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (223, 1, '17856930476', '任钡螺', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (224, 1, '18149731863', '毋丘韭度', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (225, 1, '18754509358', '章莽役', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (226, 1, '18055512490', '孟葺光', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (227, 1, '18571291422', '强亮', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (228, 1, '17156172010', '蒙芋痪', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (229, 1, '15903827482', '明畸茔', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (230, 1, '15521957850', '令狐仟直', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (231, 1, '15321051419', '闫矽', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (232, 1, '17892289150', '顾拯', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (233, 1, '15903517573', '鱼呓袍', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (234, 1, '18565351941', '艾狂诳', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (235, 1, '14937413999', '仲孙翰', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (236, 1, '13021546829', '北宫石诒', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (237, 1, '14713443317', '东我', 0, 1, '2021-06-28 16:27:46', '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (238, 1, '14577855687', '公孙圾', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (239, 1, '14718234617', '焦醒邰', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (240, 1, '13549195659', '铁第', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (241, 1, '13861393209', '季芄脑', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (242, 1, '15637047153', '危竖彳', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (243, 1, '18805843999', '钦岐咋', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (244, 1, '17314128945', '海碟', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (245, 1, '16648409861', '上官柠嗒', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (246, 1, '13176628138', '叔孙峡', 0, 1, '2021-06-28 16:27:50', '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (247, 1, '13359762909', '莘谓', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (248, 1, '17851407357', '罗竟', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (249, 1, '17869492983', '计盔恐', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (250, 1, '18156661588', '高陈', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (251, 1, '17588597010', '言匆草', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (252, 1, '13039524235', '饶丘', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (253, 1, '17595069287', '戎币呼', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (254, 1, '17393781811', '宇文玖', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (255, 1, '13474726176', '召住鼎', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (256, 1, '19941662687', '卞伍嚯', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (257, 1, '14787208344', '焦实浙', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (258, 1, '18620055544', '司马酵', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (259, 1, '16647825564', '詹呀', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (260, 1, '15700604471', '劳参', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (261, 1, '17863313725', '郗堑', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (262, 1, '13331897825', '佟唪耗', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (263, 1, '17773836254', '叔孙筒蔻', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (264, 1, '16675932566', '包斤', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (265, 1, '15265708495', '干沈', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (266, 1, '15840359982', '胡洞', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (267, 1, '17821365944', '温尖', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (268, 1, '17874999707', '徐�', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (269, 1, '17530704683', '卓单', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (270, 1, '18041356500', '轩辕谈', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (271, 1, '15302001807', '荀墚救', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (272, 1, '13039859044', '傅香', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (273, 1, '17833729948', '毛篮', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (274, 1, '18548448219', '揭泌', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (275, 1, '17190112752', '却钾', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (276, 1, '17319080178', '沈晴敲', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (277, 1, '15793470261', '夏难', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (278, 1, '15535510440', '查吮茸', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (279, 1, '14789274275', '宰父哔', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (280, 1, '17603073499', '屈伫窄', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (281, 1, '18942592513', '轩辕峁廖', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (282, 1, '19874074568', '韩脐遥', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (283, 1, '19974566664', '羊舌胎', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (284, 1, '17878530224', '钮寓圪', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (285, 1, '17160320767', '公西鱼', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (286, 1, '15336920760', '阮太', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (287, 1, '14596169369', '雍坳般', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (288, 1, '17306661211', '宾扮', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (289, 1, '18996739118', '安喑', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (290, 1, '17797232109', '任芭位', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (291, 1, '16647160309', '端借赢', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (292, 1, '19893703401', '孔奔糯', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (293, 1, '15534282028', '公西轮', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (294, 1, '17727484378', '郁瘦', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (295, 1, '15075454045', '雷摒畅', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (296, 1, '15117387435', '计逊', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (297, 1, '18171349353', '仓宫', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (298, 1, '15901575205', '唐葬', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (299, 1, '15976695024', '岳铸菩', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (300, 1, '13848570215', '缑就', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (301, 1, '18201713099', '卢蓝筏', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (302, 1, '19865175533', '百里唤嶂', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (303, 1, '15849065294', '辜缚', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (304, 1, '16677346902', '贺兰褥', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (305, 1, '18669799073', '韦腿', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (306, 1, '17209461160', '柴搅厢', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (307, 1, '15327728446', '牟皋', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (308, 1, '18057243034', '颛孙白鸿', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (309, 1, '17782621170', '邹薇', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (310, 1, '13704561204', '甘扣', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (311, 1, '17516653331', '法倩', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (312, 1, '16665460430', '周撅痔', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (313, 1, '13534724385', '衡悯铱', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (314, 1, '13232533648', '公良蠃', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (315, 1, '17620377161', '张廖管莽', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (316, 1, '15196742134', '韦份歇', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (317, 1, '13543585262', '钦鲍末', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (318, 1, '15308158451', '司空讴链', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (319, 1, '14535089310', '史仉', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (320, 1, '18599721030', '步湍融', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (321, 1, '13270338331', '谢谟嗷', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (322, 1, '18988352885', '干末', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (323, 1, '17391078937', '林赐', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (324, 1, '18070463983', '辛绎悔', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (325, 1, '18175941634', '慕蜀', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (326, 1, '15639834879', '廉驴滴', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (327, 1, '18942011724', '羊舌思', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (328, 1, '13507405592', '万众', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (329, 1, '14901498617', '能岍笔', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (330, 1, '15798489127', '厍部伴', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (331, 1, '18946331157', '蒲情菲', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (332, 1, '13318495976', '翁坎艳', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (333, 1, '19939847426', '楚雪', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (334, 1, '18608092021', '叔孙锚徇', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (335, 1, '17838765612', '挚监', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (336, 1, '17809339668', '荆佾贼', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (337, 1, '17515358488', '爱新', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (338, 1, '17599628130', '巩作', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (339, 1, '15907659458', '贺兰锋', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (340, 1, '19829640574', '阙叨', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (341, 1, '18037440649', '涂居迷', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (342, 1, '15173967838', '融霄', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (343, 1, '18934610861', '艾护', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (344, 1, '18387435015', '楼敢缨', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (345, 1, '14519838985', '班冷', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (346, 1, '13560732442', '还旅', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (347, 1, '13879987016', '佘卺一', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (348, 1, '18620815169', '俞辰鞋', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (349, 1, '13351173964', '仲叉经', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (350, 1, '15614150304', '权攀', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (351, 1, '13360927082', '密训', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (352, 1, '17626737551', '闵纳', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (353, 1, '18132861064', '关昏樱', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (354, 1, '18202186084', '尉迟隅', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (355, 1, '13801229480', '舒芗', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (356, 1, '17330498479', '司马杜', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (357, 1, '17518372890', '訾朵额', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (358, 1, '14582808672', '寿颓', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (359, 1, '13384051621', '闻人锑锡', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (360, 1, '18711063342', '鄢蒗啜', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (361, 1, '16699289314', '荣锚弯', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (362, 1, '13491005773', '还穷迅', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (363, 1, '18724643702', '乌掉', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (364, 1, '16638346734', '官小', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (365, 1, '16619104201', '柏郊', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (366, 1, '15296797520', '左刘蓑', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (367, 1, '15733453786', '爱嗷', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (368, 1, '15721087495', '白啃', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (369, 1, '17162893150', '蒋恤', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (370, 1, '15782840535', '汝厥澄', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (371, 1, '15665924566', '祁删', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (372, 1, '13564308441', '齐挥', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (373, 1, '18198963449', '荀摄', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (374, 1, '15751329417', '毋呷壅', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (375, 1, '13866053882', '司城蔫', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (376, 1, '17749998886', '步砚', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (377, 1, '15317485523', '苌革闷', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (378, 1, '15257375516', '官焕孙', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (379, 1, '13163384650', '宫伪呐', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (380, 1, '16682974037', '祁冯磕', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (381, 1, '15814863579', '廉�', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (382, 1, '18961655384', '卜训', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (383, 1, '17363017299', '贺络', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (384, 1, '19977523972', '彭当', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (385, 1, '13254129263', '田者漳', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (386, 1, '13134932862', '阙柬超', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (387, 1, '18544501471', '郈脐求', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (388, 1, '17662751022', '平宙', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (389, 1, '18087983847', '祝坩', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (390, 1, '17505973189', '靳埭', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (391, 1, '13450662348', '司城狈', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (392, 1, '13856449139', '钟蒴', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (393, 1, '15068392248', '孟茨', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (394, 1, '19942849282', '毛盼', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (395, 1, '18109516503', '毛欣跑', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (396, 1, '17355365458', '鲜纠', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (397, 1, '13234851144', '姬范揽', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (398, 1, '17654775589', '仉萑采', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (399, 1, '18069397445', '邹坟', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (400, 1, '13209418238', '盛饮筹', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (401, 1, '18062751410', '公孙鹏', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (402, 1, '13275771753', '澹台忧畦', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (403, 1, '14927777791', '那噶', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (404, 1, '13387885595', '宰厘', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (405, 1, '17540200600', '应搛', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (406, 1, '13756538653', '蔺壤', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (407, 1, '15663252325', '牟算里', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (408, 1, '18178773971', '却租', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (409, 1, '13308532539', '闵巍', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (410, 1, '15585217859', '童煽免', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (411, 1, '18148966693', '黎铲佚', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (412, 1, '18668776416', '仲嘻蒈', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (413, 1, '18078640559', '温条钠', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (414, 1, '18056858309', '翟淤', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (415, 1, '14942658866', '夔碉', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (416, 1, '18220101476', '巨嗒', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (417, 1, '13057324558', '穆圄', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (418, 1, '16694812339', '莫壕', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (419, 1, '16609077762', '孔噤姚', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (420, 1, '13360116111', '暴禄', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (421, 1, '17145376885', '展犭', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (422, 1, '15767336903', '于端', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (423, 1, '15525949764', '严香绍', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (424, 1, '15930636994', '鄢歇', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (425, 1, '17659451272', '从瘁', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (426, 1, '14513421853', '刘胜曳', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (427, 1, '13127665647', '鲜于始给', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (428, 1, '17200310642', '单高', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (429, 1, '17745243121', '居狯析', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (430, 1, '13533045073', '仇蓖', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (431, 1, '13606865640', '暨哿蒴', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (432, 1, '14579893602', '郑嘴示', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (433, 1, '19926962684', '华帅', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (434, 1, '18430654128', '从脓韵', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (435, 1, '13095556439', '臧客', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (436, 1, '17741453878', '微生追', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (437, 1, '18314564973', '钱榔', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (438, 1, '15612163184', '沈咀', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (439, 1, '17356469847', '沙涟', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (440, 1, '17342813527', '陆�', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (441, 1, '13403097215', '戈蔽', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (442, 1, '15146386280', '耿啤', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (443, 1, '18376235067', '双惭函', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (444, 1, '13409546463', '何阀啾', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (445, 1, '15385611096', '闫陡焙', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (446, 1, '17727532858', '钦毅乾', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (447, 1, '13128747185', '郭肘', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (448, 1, '15182952118', '缪饥', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (449, 1, '19981055407', '蒙矢邳', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (450, 1, '13790264707', '寇氢', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (451, 1, '15729846905', '能兮', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (452, 1, '15997151293', '苌络', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (453, 1, '13522876564', '冯物邹', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (454, 1, '17777011046', '查僻睫', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (455, 1, '19913134403', '贺兰瘁', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (456, 1, '13012890522', '郏颅忌', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (457, 1, '15280188082', '高搅', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (458, 1, '13251362282', '姬翼受', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (459, 1, '15700045193', '邹十谘', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (460, 1, '17572897656', '宰蔷薇', 0, 1, '2021-06-28 16:28:17', '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (461, 1, '15741399812', '刁捡', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (462, 1, '13937667720', '皋诶', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (463, 1, '18157175864', '叔孙讥', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (464, 1, '17146899496', '罗斤褪', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (465, 1, '18220183593', '詹噌', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (466, 1, '14950602575', '严堰泰', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (467, 1, '15609729408', '时豫桩', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (468, 1, '17696728807', '温晴', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (469, 1, '18256424962', '羊柄', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (470, 1, '18501797376', '高螟垮', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (471, 1, '15312062197', '秦炙', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (472, 1, '14928042250', '羊舌苹', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (473, 1, '17644035295', '叶夭', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (474, 1, '17308717721', '唐帏', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (475, 1, '18951016135', '厉凉', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (476, 1, '18996631962', '疏岁', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (477, 1, '17192403137', '令狐偏装', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (478, 1, '13807193732', '万乏', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (479, 1, '18095920077', '童荷', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (480, 1, '13455301309', '端税', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (481, 1, '17659774716', '屠酝叼', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (482, 1, '15598659029', '阎攸', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (483, 1, '15289634955', '匡虞委', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (484, 1, '13007511910', '老瘴', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (485, 1, '16657672137', '丰范菰', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (486, 1, '15346917062', '倪尧', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (487, 1, '13635279140', '匡臼', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (488, 1, '15179598843', '屈突芽', 0, 1, '2021-06-28 16:21:21', '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (489, 1, '19958200488', '逯货葭', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (490, 1, '18071817567', '桓影傈', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (491, 1, '13360135371', '蔚诋还', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (492, 1, '14980601773', '皇获奶', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (493, 1, '14934617857', '高痰', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (494, 1, '15051456219', '晋众彻', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (495, 1, '17751059710', '舒堠肖', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (496, 1, '15718380502', '骆郇', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (497, 1, '13827302869', '麦舟', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (498, 1, '15951430821', '娄瞬', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (499, 1, '17675023691', '薛矣芨', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (500, 1, '15134500881', '谭氯', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (501, 1, '17113347078', '师请郛', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (502, 1, '17746306173', '墨抡芤', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (503, 1, '13333812940', '益刻蓑', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (504, 1, '13610798592', '申屠诀', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (505, 1, '17211068515', '晁嗳某', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (506, 1, '18968146530', '常囹撕', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (507, 1, '17221993993', '厍缀敬', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (508, 1, '19946027378', '宦秒', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (509, 1, '18600435997', '虎嗷布', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (510, 1, '13625993599', '还蒌', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (511, 1, '13128352941', '甘闪', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (512, 1, '18971973262', '金诰', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (513, 1, '13053241960', '康烽', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (514, 1, '15679893875', '平孟', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (515, 1, '18912112150', '盛茔', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (516, 1, '15332672921', '宁捐', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (517, 1, '18608393774', '利涡壤', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (518, 1, '18345831809', '谯茨如', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (519, 1, '18162178238', '贺兰捎', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (520, 1, '19803333975', '蒙鄄', 0, 1, '2021-06-28 16:28:23', '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (521, 1, '15762317483', '麻尖', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (522, 1, '15547495012', '饶庙距', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (523, 1, '18215186455', '毛毕颐', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (524, 1, '16661640157', '百里驳', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (525, 1, '19803622503', '田他敦', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (526, 1, '17197779582', '冉邗', 0, 1, '2021-06-28 16:28:27', '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (527, 1, '17702640671', '过汛元', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (528, 1, '13684547147', '越鄹佐', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (529, 1, '17692477738', '范凋揪', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (530, 1, '14923846286', '汤亘瘟', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (531, 1, '18122521243', '介烧', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (532, 1, '15873472273', '乔蓁违', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (533, 1, '18798150546', '仪裸是', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (534, 1, '18776897181', '从芈噫', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (535, 1, '15859933716', '龙棠', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (536, 1, '15546624609', '费藿炭', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (537, 1, '15979973414', '杜鳞', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (538, 1, '15585254362', '韩拢拆', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (539, 1, '17528368550', '苍真', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (540, 1, '14907722186', '吴汁', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (541, 1, '17747435078', '纵先', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (542, 1, '14727038570', '顾码', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (543, 1, '13377070436', '史半', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (544, 1, '18916528910', '干嫂贼', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (545, 1, '15570563295', '康炼', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (546, 1, '16696385984', '浦枕', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (547, 1, '15226999039', '姚铭', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (548, 1, '18902186009', '阴整', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (549, 1, '13202547486', '容百埋', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (550, 1, '18651851123', '蓬霄丽', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (551, 1, '17529063871', '是菲钧', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (552, 1, '15117471350', '东乡咭僦', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (553, 1, '15859516186', '洪剡粥', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (554, 1, '13402233414', '徐涣', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (555, 1, '18958515396', '南门膘', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (556, 1, '16669383256', '子车薯', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (557, 1, '17208340072', '茹寐', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (558, 1, '13199212159', '覃荏盔', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (559, 1, '15806008481', '南窗', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (560, 1, '15116868776', '易嗪', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (561, 1, '14783880201', '第五综嗾', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (562, 1, '15517919406', '申珐', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (563, 1, '13886850846', '侴臼愁', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (564, 1, '15383436186', '胡赣炙', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (565, 1, '15248115708', '聂腹', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (566, 1, '15707306817', '游薇', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (567, 1, '14722493311', '还侃', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (568, 1, '15331951145', '喻恤', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (569, 1, '15781653416', '挚靶', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (570, 1, '15922717785', '余协歇', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (571, 1, '15315272328', '查弘卸', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (572, 1, '16681064769', '季抉', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (573, 1, '13368119024', '老懊', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (574, 1, '18975818484', '柯吧', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (575, 1, '14565288831', '尉迟潜跋', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (576, 1, '18557522999', '金宋', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (577, 1, '18189070835', '乐泄萃', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (578, 1, '18903294902', '官夯禽', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (579, 1, '15652562285', '虞皮怒', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (580, 1, '18148622174', '张还', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (581, 1, '18154694591', '畅茎偬', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (582, 1, '18235771064', '仲蒲', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (583, 1, '14933323323', '宫茹', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (584, 1, '17257153260', '仰浚', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (585, 1, '18415397985', '贝钒菏', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (586, 1, '17800490022', '吴凵葑', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (587, 1, '17790658536', '漆雕暂', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (588, 1, '16696191730', '慕容刮透', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (589, 1, '16651756895', '龙芎', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (590, 1, '18038054680', '宾女', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (591, 1, '18647320708', '法闷犹', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (592, 1, '18090505043', '终扪', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (593, 1, '19915072800', '危佑嗍', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (594, 1, '14587838526', '邬阁泊', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (595, 1, '13863242979', '家菠', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (596, 1, '15333520710', '贝膨诊', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (597, 1, '15339665264', '秋临公', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (598, 1, '18823124086', '慕容予', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (599, 1, '17126218261', '申诓匠', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (600, 1, '19977852335', '秘宋', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (601, 1, '15076188056', '相里噙', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (602, 1, '13861675248', '巫撂', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (603, 1, '16631200711', '昝窜胸', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (604, 1, '19806329565', '扈硅', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (605, 1, '15352457483', '空疤铁', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (606, 1, '17394914969', '龚青', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (607, 1, '18041987564', '终初宦', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (608, 1, '13323550012', '高氽', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (609, 1, '14966481972', '仲长洋', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (610, 1, '18867307574', '马禀蒸', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (611, 1, '14554353230', '张廖呐', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (612, 1, '18793063215', '拓跋甘', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (613, 1, '16695856605', '皋揎著', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (614, 1, '16627749345', '丁胡剿', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (615, 1, '16621884125', '荣典', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (616, 1, '14727331003', '桑荦弧', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (617, 1, '18699864344', '逄哞', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (618, 1, '18442001527', '滕重', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (619, 1, '15381966340', '茹凫堂', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (620, 1, '18318989260', '广桂', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (621, 1, '18719394613', '折犊', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (622, 1, '16671310975', '屋庐菌晚', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (623, 1, '13362227610', '郎领拐', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (624, 1, '19873949849', '吕菘踌', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (625, 1, '17551826658', '伊僳研', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (626, 1, '15027330463', '微生叭绝', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (627, 1, '13472676422', '焦茵募', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (628, 1, '17282880795', '蒯曝乃', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (629, 1, '18971923733', '宓带玻', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (630, 1, '18912167315', '权呃缩', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (631, 1, '17172305714', '顾鄞', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (632, 1, '13761906162', '柴疼阪', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (633, 1, '17603407107', '尚庐', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (634, 1, '18154849932', '寇洛铂', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (635, 1, '19960088033', '侴驹嗫', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (636, 1, '17364035705', '柯季', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (637, 1, '15263823020', '空伪', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (638, 1, '13328063668', '卓佴窜', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (639, 1, '18673046553', '颛孙烧胎', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (640, 1, '18269231060', '柴哨喜', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (641, 1, '15147410001', '弘岢', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (642, 1, '18096468677', '郦嗬店', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (643, 1, '15947732598', '太叔脔', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (644, 1, '16677513069', '宰睬宛', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (645, 1, '17131803697', '家鳃娠', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (646, 1, '19835225276', '能岙', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (647, 1, '15560579912', '越透值', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (648, 1, '18665096117', '狐摺', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (649, 1, '17166792482', '毋壤榷', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (650, 1, '18905954071', '涂咩叫', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (651, 1, '13579014951', '东乡谤贬', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (652, 1, '14919731617', '冒晨', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (653, 1, '13377229324', '杜页祁', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (654, 1, '17192432461', '仉萘', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (655, 1, '15307234631', '宗正去', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (656, 1, '14700982365', '狐搀', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (657, 1, '16608273946', '通倡', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (658, 1, '13720158896', '柯遣', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (659, 1, '13779483279', '闻硷', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (660, 1, '17594872724', '墨惫潭', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (661, 1, '17741986411', '蒙訇', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (662, 1, '18977377880', '丰士浴', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (663, 1, '14546867765', '来辣缀', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (664, 1, '17228980243', '空帚', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (665, 1, '19943886769', '赖垫', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (666, 1, '15095568795', '伯亮', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (667, 1, '17389388187', '国苯羡', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (668, 1, '18054855232', '南划饲', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (669, 1, '14953829293', '胶日抓', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (670, 1, '17388027580', '干泼', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (671, 1, '18136876260', '金徨', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (672, 1, '13365551235', '殳根', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (673, 1, '13464896384', '席弦淮', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (674, 1, '17751083034', '蒯嗔', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (675, 1, '14936689669', '闾丘芈袜', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (676, 1, '17720521170', '老呖则', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (677, 1, '15314372591', '罗渝驮', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (678, 1, '15679891952', '程赫端', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (679, 1, '18428068274', '汤嗍杏', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (680, 1, '15762676040', '高叱', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (681, 1, '13370871042', '苍妇叶', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (682, 1, '15920647638', '弓警俎', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (683, 1, '18011966196', '亢谚', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (684, 1, '18572842466', '秦坌', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (685, 1, '17757288064', '佟朝涸', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (686, 1, '15853178696', '佟阳羔', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (687, 1, '15092075994', '辛嘏', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (688, 1, '15966281859', '富吴嶷', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (689, 1, '18504846002', '诸葛令仕', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (690, 1, '18111565818', '饶阝', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (691, 1, '17641837150', '傅茅', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (692, 1, '13679213710', '花碑谍', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (693, 1, '17173992097', '国锋', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (694, 1, '13871809582', '康队', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (695, 1, '14559791749', '空片', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (696, 1, '13915288207', '冷规蛆', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (697, 1, '18195955585', '伊讼盘', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (698, 1, '13612728948', '来竖', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (699, 1, '18653033488', '秦祥呒', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (700, 1, '14931035788', '卫撮', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (701, 1, '17557804711', '牛禁', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (702, 1, '18930872298', '任实魄', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (703, 1, '15069294310', '庞览', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (704, 1, '17355465813', '东方谂', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (705, 1, '17174195414', '危啊鸣', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (706, 1, '13322635088', '楚搬窝', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (707, 1, '18826300749', '荀凯汉', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (708, 1, '19841233584', '闫阉坡', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (709, 1, '17744391942', '公通', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (710, 1, '13837130417', '池式蔬', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (711, 1, '13259920896', '綦毋钞', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (712, 1, '14559274205', '达卖汁', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (713, 1, '15288907442', '向掺', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (714, 1, '13146599182', '殳阼', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (715, 1, '16639160198', '房朋创', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (716, 1, '18050933433', '禹薪考', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (717, 1, '18153052899', '爱娩仳', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (718, 1, '15326479827', '惠陶', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (719, 1, '13958989111', '施余', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (720, 1, '18684733042', '胡母厩角', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (721, 1, '15610956407', '娄率', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (722, 1, '18992859420', '涂颓', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (723, 1, '17117605964', '慎俗', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (724, 1, '17722821168', '蒯赘挎', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (725, 1, '18724588778', '权砒', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (726, 1, '15826175231', '李逃喝', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (727, 1, '17509323024', '段施案', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (728, 1, '15344235965', '司空莘', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (729, 1, '13334276347', '挚枚', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (730, 1, '16614365433', '楼蔟', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (731, 1, '18490978840', '韶帜钟', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (732, 1, '15359288905', '康啪脾', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (733, 1, '13213654463', '百里菸阴', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (734, 1, '14799604694', '利�', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (735, 1, '14764744276', '上官演', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (736, 1, '16621291555', '练唆', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (737, 1, '18972713494', '干染铬', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (738, 1, '17613036533', '闾揖懿', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (739, 1, '14767970426', '贾罗', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (740, 1, '14913673946', '南宫儇葜', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (741, 1, '13852229107', '向萝', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (742, 1, '13491840893', '西门坞特', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (743, 1, '15341540523', '许药拊', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (744, 1, '18445424403', '温吧斡', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (745, 1, '14744711032', '仉磨', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (746, 1, '15936592911', '淳于苌', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (747, 1, '18213881249', '向唐嵴', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (748, 1, '15360257947', '富黔宾', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (749, 1, '19986853210', '欧匏', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (750, 1, '13475086721', '虞谅', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (751, 1, '17794490701', '索仔修', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (752, 1, '15342294213', '蔡莺', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (753, 1, '17657366294', '虎埕', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (754, 1, '17141152804', '干徒葱', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (755, 1, '18512314437', '轩辕兢', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (756, 1, '18179890083', '夔譬', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (757, 1, '18257616130', '庆耕', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (758, 1, '19866046467', '郈囿司', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (759, 1, '14785848391', '刁荀', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (760, 1, '16688401024', '庆穿', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (761, 1, '15349586650', '庆哳偾', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (762, 1, '18387277540', '须藤', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (763, 1, '17189956653', '毋惺', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (764, 1, '18674778359', '惠口', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (765, 1, '15216202952', '米而', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (766, 1, '17358217548', '芮痊鸟', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (767, 1, '18926744472', '还哀', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (768, 1, '15079904473', '阿嘀', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (769, 1, '16656150235', '薛呷铂', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (770, 1, '17365231487', '司城礼', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (771, 1, '13219503390', '鄂藩属', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (772, 1, '15325357269', '佘寥凹', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (773, 1, '18943484404', '童芷陟', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (774, 1, '18287371154', '虎她', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (775, 1, '18905198796', '楚票烘', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (776, 1, '18339595361', '宫府蒗', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (777, 1, '13365474196', '闾晶乙', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (778, 1, '18265360036', '法溶抠', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (779, 1, '18685573488', '麦抡仁', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (780, 1, '15056233106', '阳慌', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (781, 1, '18098221105', '迟樟', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (782, 1, '18661401291', '眭仔俺', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (783, 1, '18647911595', '能昭', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (784, 1, '17354335821', '仰努', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (785, 1, '17390275632', '朱兜裕', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (786, 1, '18585012155', '荀皖', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (787, 1, '18057283764', '殷侥噙', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (788, 1, '18139930294', '辜敝', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (789, 1, '13390696784', '索挥多', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (790, 1, '15586256997', '扶扁逞', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (791, 1, '13887835585', '阎蔫景', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (792, 1, '13428684704', '苌渗', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (793, 1, '15022871822', '邱晶中', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (794, 1, '15068016582', '吴芸适', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (795, 1, '17741482435', '彭仰', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (796, 1, '13471992190', '晏郢�', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (797, 1, '18887695275', '韶骤', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (798, 1, '13857244219', '祖包', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (799, 1, '18669905868', '桑昆', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (800, 1, '19955648784', '方藐御', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (801, 1, '13851419846', '蓝藁凡', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (802, 1, '15052618156', '于虏旁', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (803, 1, '17227746286', '隗案', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (804, 1, '15672403360', '陆驳', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (805, 1, '17673145279', '宣京', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (806, 1, '14515646213', '杜甍谑', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (807, 1, '15262803689', '甘茅', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (808, 1, '13371859181', '富何', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (809, 1, '15307256679', '车喈', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (810, 1, '17566427895', '徐倡疥', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (811, 1, '18263729588', '祁辜', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (812, 1, '18516212086', '符囟袄', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (813, 1, '14903280449', '微生巽骏', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (814, 1, '14708286180', '爱拓', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (815, 1, '14525946993', '许伲', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (816, 1, '15688526372', '鱼樟', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (817, 1, '17679012777', '钦醋板', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (818, 1, '14516605282', '骆攥', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (819, 1, '13117891303', '景垣坨', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (820, 1, '18942447070', '苍陆', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (821, 1, '18099995219', '幸旨臀', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (822, 1, '18140749452', '干鼐', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (823, 1, '17765212797', '弥掣馘', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (824, 1, '15353882693', '金劭', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (825, 1, '17232476379', '梁梨', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (826, 1, '18418913847', '花穴戳', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (827, 1, '17301179762', '施懂', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (828, 1, '18998155612', '蔺虹云', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (829, 1, '18039770196', '宿堰械', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (830, 1, '15566393316', '怀峋拔', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (831, 1, '15183332759', '迟亢狲', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (832, 1, '13345107080', '成侃', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (833, 1, '18761007964', '张版快', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (834, 1, '17267700267', '酆默', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (835, 1, '19932272862', '蓟氓', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (836, 1, '14775792175', '法池深', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (837, 1, '17142338178', '鄂赠抵', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (838, 1, '17106863772', '芮稠', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (839, 1, '16653737478', '夔条门', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (840, 1, '18932900903', '冉慌辞', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (841, 1, '17193742155', '乜盆邳', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (842, 1, '18856776655', '任菇', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (843, 1, '16637963940', '兀官械驻', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (844, 1, '15151560788', '言剪钧', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (845, 1, '13190296064', '居地僧', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (846, 1, '17295064598', '池孜', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (847, 1, '18709929196', '汝掇', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (848, 1, '19975031670', '赫连迟', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (849, 1, '15375051927', '柳烁', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (850, 1, '19915616373', '黄纷', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (851, 1, '13080164897', '许篮埠', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (852, 1, '18145238221', '扈汁艽', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (853, 1, '13820364842', '卞秋', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (854, 1, '17299593004', '仓扩', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (855, 1, '13002610159', '鄂贰村', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (856, 1, '18215665077', '督岐', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (857, 1, '14577808831', '杨唾钮', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (858, 1, '16659203055', '项叱胃', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (859, 1, '18529780611', '木溪辫', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (860, 1, '18645593178', '冉歌', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (861, 1, '19955222758', '徐钒站', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (862, 1, '15008254330', '陈辜跺', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (863, 1, '13388054223', '琴毡', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (864, 1, '18095911621', '南宫嗫骏', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (865, 1, '13856874217', '壤驷堂客', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (866, 1, '18913627484', '甘捣', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (867, 1, '15186909162', '郭叫', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (868, 1, '17639053779', '唐脾狁', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (869, 1, '17652547064', '彭利扎', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (870, 1, '15859239212', '申屠决', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (871, 1, '16633959641', '益递磺', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (872, 1, '18729284905', '靳岙', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (873, 1, '13125861846', '雍药', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (874, 1, '13475633008', '须拌荒', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (875, 1, '19933934698', '兀官演装', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (876, 1, '19871332010', '牧仅框', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (877, 1, '18653084761', '仲筏荦', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (878, 1, '14722634640', '段捭', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (879, 1, '18793786364', '蒯厍最', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (880, 1, '17336249560', '季该摺', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (881, 1, '13553792586', '诸葛恭', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (882, 1, '15367122972', '郝谷', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (883, 1, '18670553279', '席碍撮', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (884, 1, '18383133098', '晋睾', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (885, 1, '15225946444', '鲜于葬忙', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (886, 1, '19971896727', '班其块', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (887, 1, '18427234956', '公西迈红', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (888, 1, '18591156098', '司徒代', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (889, 1, '18821343492', '官�', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (890, 1, '18696992601', '莫春剩', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (891, 1, '18876884621', '都弱是', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (892, 1, '18116417331', '朱忽佬', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (893, 1, '18582782031', '慕容爽', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (894, 1, '18821066933', '林食', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (895, 1, '14904750356', '公瞎', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (896, 1, '16666810184', '归佻', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (897, 1, '18492379767', '桂牵', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (898, 1, '15823475273', '侯邾仲', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (899, 1, '13327159882', '郈优确', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (900, 1, '14976238476', '诸腺蠃', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (901, 1, '17746795654', '尚着', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (902, 1, '18945455073', '简拯', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (903, 1, '18662769047', '来斑琵', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (904, 1, '15099951038', '荀搛陲', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (905, 1, '17709191297', '邬见', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (906, 1, '18907245813', '屠嗑原', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (907, 1, '15021784738', '桑枣默', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (908, 1, '13295188659', '章籍', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (909, 1, '17284698016', '罗稼', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (910, 1, '18186939777', '洪枷', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (911, 1, '14962947787', '杨芑猎', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (912, 1, '17121267185', '桓蒂', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (913, 1, '18212132254', '呼延据冕', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (914, 1, '15185101035', '管右郝', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (915, 1, '15655152107', '滑蓁愿', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (916, 1, '16612802862', '侴气懈', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (917, 1, '14779217542', '宿孙', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (918, 1, '14933057413', '黄烟', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (919, 1, '17688720730', '简叱恩', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (920, 1, '17242449724', '席误', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (921, 1, '13870219046', '陶呻', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (922, 1, '13850714416', '佴卷', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (923, 1, '17214447788', '弥褥', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (924, 1, '18434048743', '融慑噍', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (925, 1, '17656795015', '禹瓶曹', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (926, 1, '18002056775', '逄杭铱', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (927, 1, '15300936374', '端无', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (928, 1, '13792344211', '充毕', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (929, 1, '14940983264', '穆狰', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (930, 1, '15341541782', '南门苜', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (931, 1, '13147870805', '鞠沛毁', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (932, 1, '18976398110', '俞蒈保', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (933, 1, '18008989765', '贾频', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (934, 1, '13858144809', '谯拿薤', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (935, 1, '18580814317', '桂楼戴', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (936, 1, '18101807548', '辜噻', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (937, 1, '17303928164', '班弑', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (938, 1, '14936985417', '红增', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (939, 1, '17387856447', '邰怔吆', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (940, 1, '14526877885', '咸诧', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (941, 1, '18022329242', '宾检赐', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (942, 1, '17788083642', '栾侦', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (943, 1, '18109424918', '从廾', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (944, 1, '13439940705', '殳暮屯', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (945, 1, '13299969528', '竺瘟', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (946, 1, '18694084611', '竹葭', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (947, 1, '13298656014', '蒙役', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (948, 1, '19910269460', '师莆悉', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (949, 1, '17718297194', '尉迟揣', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (950, 1, '15627380480', '黎浙嘌', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (951, 1, '19931881813', '梁俄劾', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (952, 1, '18170540382', '莘给', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (953, 1, '17146592007', '谷撬祈', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (954, 1, '17737540853', '公西掊具', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (955, 1, '13407214715', '陈篷查', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (956, 1, '16621151100', '苏拆外', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (957, 1, '16625392310', '温类', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (958, 1, '15543019876', '东门圊式', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (959, 1, '13399337477', '满缺', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (960, 1, '15624654098', '逯剽', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (961, 1, '15923472319', '薛塑', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (962, 1, '15363613653', '展鹿猿', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (963, 1, '19905829997', '苗圄', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (964, 1, '18094827028', '仰吉', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (965, 1, '14527033725', '宾过俘', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (966, 1, '13272561790', '郏谂', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (967, 1, '13254709830', '巢篮', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (968, 1, '18736744298', '虎褒颠', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (969, 1, '15600696869', '周抿革', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (970, 1, '15365996637', '公西墚蜕', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (971, 1, '14915501075', '暴酚', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (972, 1, '15588450285', '公羊泞', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (973, 1, '18049067433', '井峦宜', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (974, 1, '15291431864', '屈突蘅', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (975, 1, '17529698055', '戚皿', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (976, 1, '17563491897', '卓霹嘈', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (977, 1, '14585775419', '云衮盖', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (978, 1, '13107134649', '胡俅', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (979, 1, '13829531830', '鲍酸扰', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (980, 1, '15050895707', '徐掊', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (981, 1, '15128850306', '独孤据', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (982, 1, '13730183476', '充档剖', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (983, 1, '13351592442', '高值卟', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (984, 1, '14531857893', '冀蛔', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (985, 1, '13734090061', '劳徜骚', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (986, 1, '15706655146', '慕容酣', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (987, 1, '15316089058', '巢隅酣', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (988, 1, '16673955306', '上官吗仙', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (989, 1, '17232838855', '楼于', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (990, 1, '18228287573', '武铲', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (991, 1, '17137783833', '高西', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (992, 1, '15643853964', '阮嬴沸', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (993, 1, '17656246098', '毋丘伟', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (994, 1, '14573305745', '季氽揠', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (995, 1, '13861208805', '屈断', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (996, 1, '17320094204', '吕榨间', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (997, 1, '15557645041', '逄君', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (998, 1, '15876065094', '云俚', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (999, 1, '15587055874', '房腾', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (1000, 1, '18194257443', '房沪蔽', 0, 1, NULL, '2021-06-28 16:20:49', '', '');
INSERT INTO `face_person_info` VALUES (1001, 1, '13950345671', '测试', 1, 1, NULL, '2021-06-29 14:01:49', '', '');
COMMIT;

-- ----------------------------
-- Table structure for face_terminal_info
-- ----------------------------
DROP TABLE IF EXISTS `face_terminal_info`;
CREATE TABLE `face_terminal_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `merchant_id` int(11) NOT NULL DEFAULT '0' COMMENT '商户编号',
  `terminal_sn` varchar(32) CHARACTER SET utf8 NOT NULL COMMENT '终端序列号',
  `client_id` varchar(32) CHARACTER SET utf8 NOT NULL COMMENT '所属的客户端编号',
  `terminal_name` varchar(40) CHARACTER SET utf8 NOT NULL COMMENT '终端名',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态（0：无效，1：有效）',
  `remark` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `create_by` varchar(64) CHARACTER SET utf8 DEFAULT '' COMMENT '创建者',
  `update_by` varchar(64) CHARACTER SET utf8 DEFAULT '' COMMENT '更新者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='终端表';

-- ----------------------------
-- Records of face_terminal_info
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table` (
  `table_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) DEFAULT '' COMMENT '表描述',
  `class_name` varchar(100) DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) DEFAULT NULL COMMENT '生成功能作者',
  `options` varchar(1000) DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='代码生成业务表';

-- ----------------------------
-- Records of gen_table
-- ----------------------------
BEGIN;
INSERT INTO `gen_table` VALUES (1, 'face_person_info', '人员表', 'PersonInfo', 'crud', 'com.centerm.face.person', 'person', 'info', '人员', 'lidonghang', '{\"treeName\":\"\",\"treeParentCode\":\"\",\"treeCode\":\"\"}', 'admin', '2021-06-25 15:28:03', '', '2021-06-25 15:28:30', '');
INSERT INTO `gen_table` VALUES (2, 'face_face_info', '人脸数据表', 'Info', 'crud', 'com.centerm.face.person', 'face', 'info', '人脸数据', 'lidonghang', '{\"treeName\":\"\",\"treeParentCode\":\"\",\"treeCode\":\"\"}', 'admin', '2021-06-28 11:14:23', '', '2021-06-28 11:14:51', '');
COMMIT;

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column` (
  `column_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) DEFAULT '=' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) DEFAULT '' COMMENT '字典类型',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='代码生成业务表字段';

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
BEGIN;
INSERT INTO `gen_table_column` VALUES (1, '1', 'id', '自增主键', 'int(11)', 'Long', 'id', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2021-06-25 15:28:03', NULL, '2021-06-25 15:28:30');
INSERT INTO `gen_table_column` VALUES (2, '1', 'merchant_id', '商户编号', 'int(11)', 'Long', 'merchantId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 2, 'admin', '2021-06-25 15:28:03', NULL, '2021-06-25 15:28:30');
INSERT INTO `gen_table_column` VALUES (3, '1', 'phone', '手机号', 'varchar(11)', 'String', 'phone', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2021-06-25 15:28:03', NULL, '2021-06-25 15:28:30');
INSERT INTO `gen_table_column` VALUES (4, '1', 'name', '姓名', 'varchar(30)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 4, 'admin', '2021-06-25 15:28:03', NULL, '2021-06-25 15:28:30');
INSERT INTO `gen_table_column` VALUES (5, '1', 'gender', '性别', 'tinyint(1) unsigned', 'Integer', 'gender', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2021-06-25 15:28:03', NULL, '2021-06-25 15:28:30');
INSERT INTO `gen_table_column` VALUES (6, '1', 'face_id', '人脸数据编号', 'varchar(10)', 'String', 'faceId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2021-06-25 15:28:03', NULL, '2021-06-25 15:28:30');
INSERT INTO `gen_table_column` VALUES (7, '1', 'status', '状态（0：禁用，1：启用）', 'tinyint(1)', 'Integer', 'status', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'radio', '', 7, 'admin', '2021-06-25 15:28:03', NULL, '2021-06-25 15:28:30');
INSERT INTO `gen_table_column` VALUES (8, '1', 'face_feature', '人脸特征', 'blob', 'Long', 'faceFeature', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2021-06-25 15:28:03', NULL, '2021-06-25 15:28:30');
INSERT INTO `gen_table_column` VALUES (9, '1', 'face_picture', '人脸图片', 'varchar(200)', 'String', 'facePicture', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2021-06-25 15:28:03', NULL, '2021-06-25 15:28:30');
INSERT INTO `gen_table_column` VALUES (10, '1', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 10, 'admin', '2021-06-25 15:28:03', NULL, '2021-06-25 15:28:30');
INSERT INTO `gen_table_column` VALUES (11, '1', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 11, 'admin', '2021-06-25 15:28:03', NULL, '2021-06-25 15:28:30');
INSERT INTO `gen_table_column` VALUES (12, '2', 'id', '自增主键', 'int(11)', 'Long', 'id', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2021-06-28 11:14:23', NULL, '2021-06-28 11:14:51');
INSERT INTO `gen_table_column` VALUES (13, '2', 'person_id', '人员编号', 'int(11)', 'Long', 'personId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2021-06-28 11:14:23', NULL, '2021-06-28 11:14:51');
INSERT INTO `gen_table_column` VALUES (14, '2', 'merchant_id', '商户编号', 'int(11)', 'Long', 'merchantId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2021-06-28 11:14:23', NULL, '2021-06-28 11:14:51');
INSERT INTO `gen_table_column` VALUES (15, '2', 'face_id', '人脸数据编号', 'varchar(10)', 'String', 'faceId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2021-06-28 11:14:23', NULL, '2021-06-28 11:14:51');
INSERT INTO `gen_table_column` VALUES (16, '2', 'face_feature', '人脸特征', 'blob', 'Long', 'faceFeature', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2021-06-28 11:14:23', NULL, '2021-06-28 11:14:51');
INSERT INTO `gen_table_column` VALUES (17, '2', 'face_picture', '人脸图片', 'varchar(200)', 'String', 'facePicture', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2021-06-28 11:14:23', NULL, '2021-06-28 11:14:51');
INSERT INTO `gen_table_column` VALUES (18, '2', 'status', '状态（0：禁用，1：启用）', 'tinyint(1)', 'Integer', 'status', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'radio', '', 7, 'admin', '2021-06-28 11:14:23', NULL, '2021-06-28 11:14:51');
INSERT INTO `gen_table_column` VALUES (19, '2', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 8, 'admin', '2021-06-28 11:14:23', NULL, '2021-06-28 11:14:51');
INSERT INTO `gen_table_column` VALUES (20, '2', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2021-06-28 11:14:23', NULL, '2021-06-28 11:14:51');
COMMIT;

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `config_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) NOT NULL COMMENT '参数名称',
  `config_key` varchar(100) NOT NULL COMMENT '参数键名',
  `config_value` varchar(500) DEFAULT NULL COMMENT '参数键值',
  `config_type` char(1) DEFAULT NULL COMMENT '系统内置（y是 n否）',
  `value_type` tinyint(3) DEFAULT '0' COMMENT '参数值类型(0:文本，1:密码，2:单选)',
  `status` tinyint(3) unsigned DEFAULT NULL COMMENT '状态：0启动;1:停用',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新者',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='参数配置表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
BEGIN;
INSERT INTO `sys_config` VALUES (7, '新增用户初始密码', 'sys.user.initPassword', '111111', 'Y', 0, NULL, 'admin', NULL, NULL, NULL, NULL);
INSERT INTO `sys_config` VALUES (8, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 0, NULL, '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', NULL, NULL);
INSERT INTO `sys_config` VALUES (10, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 0, NULL, '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', NULL, NULL);
INSERT INTO `sys_config` VALUES (12, '人脸引擎库路径', 'face.sdk.path', '/data/face/sdk/', 'Y', 0, 0, 'admin', '', NULL, '2021-06-28 17:07:39', NULL);
INSERT INTO `sys_config` VALUES (13, '人脸引擎APPID', 'face.app.id', 'Enbd6BQbMHtNnYNmtnUZaWYbqkYpw7FVxekNUSaih8mC', 'Y', 0, 0, 'admin', '', NULL, '2021-06-28 17:07:39', NULL);
INSERT INTO `sys_config` VALUES (14, '人脸引擎SDKKEY', 'face.sdk.key', '8yrrTGCFipSqE9n59GXT8Pru6gwAe4H1E67xjuWKh6zm', 'Y', 0, 0, 'admin', '', NULL, '2021-06-28 17:07:39', NULL);
INSERT INTO `sys_config` VALUES (15, '人脸识别通过值', 'face.pass.value', '85', 'Y', 0, 0, 'admin', '', '', '2021-06-28 17:07:39', NULL);
INSERT INTO `sys_config` VALUES (16, '人脸图片路径存放地址', 'files.member.face.path', '/data/face/files/', 'Y', 0, 0, 'admin', '', NULL, '2021-06-28 17:07:39', NULL);
INSERT INTO `sys_config` VALUES (17, '人脸识别线程池大小', 'face.thread.pool.size', '5', 'Y', 0, 0, 'admin', '', NULL, '2021-06-28 17:07:30', NULL);
INSERT INTO `sys_config` VALUES (18, '人脸识别激活码', 'face.active.key', '82D1-117N-K13B-KHP6', 'Y', 0, NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for sys_config_list
-- ----------------------------
DROP TABLE IF EXISTS `sys_config_list`;
CREATE TABLE `sys_config_list` (
  `config_id` int(10) unsigned DEFAULT NULL COMMENT '参数ID',
  `config_value` varchar(200) DEFAULT NULL COMMENT '参数值'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='配置文件列表';

-- ----------------------------
-- Records of sys_config_list
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `code` varchar(32) DEFAULT NULL COMMENT '机构号',
  `parent_id` int(11) DEFAULT '0' COMMENT '父部门id',
  `ancestors` varchar(50) DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `leader` varchar(20) DEFAULT '' COMMENT '负责人',
  `phone` varchar(11) DEFAULT '' COMMENT '联系电话',
  `email` varchar(50) DEFAULT '' COMMENT '邮箱',
  `status` tinyint(1) DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '删除标志（0否；1：是）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='部门表';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
BEGIN;
INSERT INTO `sys_dept` VALUES (100, 'yzf', 0, '0', '升腾云支付', 0, 'admin', '13888888888', 'yzf@centerm.com', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES (101, 'fjzh', 100, '0,100', '福建总行', 1, 'admin', '13888888888', 'fjzh@centerm.com', 0, 0, 'admin', '2019-11-08 14:55:21', 'admin', '2019-11-08 14:55:30');
INSERT INTO `sys_dept` VALUES (102, 'zjzh', 100, '0,100', '浙江总行', 2, 'admin', '13888888888', 'zjzh@centerm.com', 0, 0, 'admin', '2019-11-08 14:57:44', 'admin', '2019-11-08 14:58:06');
INSERT INTO `sys_dept` VALUES (103, 'research', 101, '0,100,101', '研发部门', 1, 'admin', '13888888888', 'research@centerm.com', 0, 0, 'admin', '2019-11-08 15:03:41', 'admin', '2019-11-08 15:03:48');
INSERT INTO `sys_dept` VALUES (104, 'market', 101, '0,100,101', '市场部门', 2, 'admin', '13888888888', 'market@centerm.com', 0, 0, 'admin', '2019-11-08 15:05:09', 'admin', '2019-11-08 15:05:15');
INSERT INTO `sys_dept` VALUES (105, 'test', 101, '0,100,101', '测试部门', 3, 'admin', '13888888888', 'test@centerm.com', 0, 0, 'admin', '2019-11-08 15:06:29', 'admin', '2019-11-08 15:06:35');
INSERT INTO `sys_dept` VALUES (106, 'finance', 102, '0,100,102', '财务部门', 1, 'admin', '13888888888', 'finance@centerm.com', 0, 0, 'admin', '2019-11-08 15:09:57', 'admin', '2019-11-08 15:10:03');
INSERT INTO `sys_dept` VALUES (107, 'maintenance', 102, '0,100,102', '运维部门', 2, 'admin', '13888888888', 'maintenance@centerm.com', 0, 0, 'admin', '2019-11-08 15:11:19', 'admin', '2019-11-08 15:11:26');
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data` (
  `dict_code` int(10) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(10) DEFAULT NULL COMMENT '字典排序',
  `dict_label` varchar(100) DEFAULT NULL COMMENT '字典标签',
  `dict_value` varchar(32) DEFAULT NULL COMMENT '字典键值',
  `dict_type` varchar(32) DEFAULT NULL COMMENT '字典类型',
  `css_class` varchar(500) DEFAULT NULL COMMENT '样式属性',
  `list_class` varchar(500) DEFAULT NULL COMMENT '回显样式',
  `is_default` tinyint(3) unsigned DEFAULT NULL COMMENT '是否默认（y是 n否）',
  `status` tinyint(3) DEFAULT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建者',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新者',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10102 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='字典数据表';

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
BEGIN;
INSERT INTO `sys_dict_data` VALUES (102, 2, '女', '1', 'sys_user_sex', '', '', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '性别女');
INSERT INTO `sys_dict_data` VALUES (103, 3, '未知', '2', 'sys_user_sex', '', '', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '性别未知');
INSERT INTO `sys_dict_data` VALUES (104, 1, '显示', '0', 'sys_show_hide', '', 'primary', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '显示菜单');
INSERT INTO `sys_dict_data` VALUES (105, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (106, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (107, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES (108, 1, '正常', '0', 'sys_job_status', '', 'primary', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (109, 2, '暂停', '1', 'sys_job_status', '', 'danger', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES (1010, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '系统默认是');
INSERT INTO `sys_dict_data` VALUES (1011, 2, '否', 'N', 'sys_yes_no', '', 'danger', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '系统默认否');
INSERT INTO `sys_dict_data` VALUES (1012, 1, '通知', '1', 'sys_notice_type', '', 'warning', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '通知');
INSERT INTO `sys_dict_data` VALUES (1013, 2, '公告', '2', 'sys_notice_type', '', 'success', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '公告');
INSERT INTO `sys_dict_data` VALUES (1014, 1, '正常', '0', 'sys_notice_status', '', 'primary', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (1015, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '关闭状态');
INSERT INTO `sys_dict_data` VALUES (1016, 1, '新增', '1', 'sys_oper_type', '', 'info', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '新增操作');
INSERT INTO `sys_dict_data` VALUES (1017, 2, '修改', '2', 'sys_oper_type', '', 'info', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '修改操作');
INSERT INTO `sys_dict_data` VALUES (1018, 3, '删除', '3', 'sys_oper_type', '', 'danger', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '删除操作');
INSERT INTO `sys_dict_data` VALUES (1019, 4, '授权', '4', 'sys_oper_type', '', 'primary', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '授权操作');
INSERT INTO `sys_dict_data` VALUES (1020, 5, '导出', '5', 'sys_oper_type', '', 'warning', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '导出操作');
INSERT INTO `sys_dict_data` VALUES (1021, 6, '导入', '6', 'sys_oper_type', '', 'warning', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '导入操作');
INSERT INTO `sys_dict_data` VALUES (1022, 7, '强退', '7', 'sys_oper_type', '', 'danger', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '强退操作');
INSERT INTO `sys_dict_data` VALUES (1023, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '生成操作');
INSERT INTO `sys_dict_data` VALUES (1024, 8, '清空数据', '9', 'sys_oper_type', '', 'danger', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '清空操作');
INSERT INTO `sys_dict_data` VALUES (1025, 1, '成功', '0', 'sys_common_status', '', 'primary', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES (1026, 2, '失败', '1', 'sys_common_status', '', 'danger', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES (1027, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 0, 0, 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '默认分组');
INSERT INTO `sys_dict_data` VALUES (1028, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 0, 0, 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统分组');
INSERT INTO `sys_dict_data` VALUES (10101, 1, '男', '0', 'sys_user_sex', '', '', 0, 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '性别男');
COMMIT;

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type` (
  `dict_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) DEFAULT NULL COMMENT '字典名称',
  `dict_type` varchar(100) NOT NULL COMMENT '字典类型',
  `status` tinyint(3) unsigned DEFAULT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建者',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新者',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='字典类型表';

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
BEGIN;
INSERT INTO `sys_dict_type` VALUES (19, '用户性别', 'sys_user_sex', 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (20, '菜单状态', 'sys_show_hide', 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (21, '系统开关', 'sys_normal_disable', 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (22, '任务状态', 'sys_job_status', 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (23, '系统是否', 'sys_yes_no', 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (24, '通知类型', 'sys_notice_type', 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (25, '通知状态', 'sys_notice_status', 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (26, '操作类型', 'sys_oper_type', 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (27, '系统状态', 'sys_common_status', 0, 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '登录状态列表');
COMMIT;

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `method_name` varchar(50) DEFAULT NULL,
  `method_params` varchar(100) DEFAULT NULL,
  `cron_expression` varchar(255) DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`,`job_name`,`job_group`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='定时任务调度表';

-- ----------------------------
-- Records of sys_job
-- ----------------------------
BEGIN;
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', NULL, NULL, '0/10 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', NULL, NULL, '0/15 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', NULL, NULL, '0/20 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
COMMIT;

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log` (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) DEFAULT NULL COMMENT '日志信息',
  `status` char(1) DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) DEFAULT '' COMMENT '异常信息',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='定时任务调度日志表';

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor` (
  `info_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '访问id',
  `login_name` varchar(50) DEFAULT NULL COMMENT '登录账号',
  `ipaddr` varchar(50) DEFAULT NULL COMMENT '登录ip地址',
  `login_location` varchar(255) DEFAULT NULL COMMENT '登录地点',
  `browser` varchar(50) DEFAULT NULL COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT NULL COMMENT '操作系统',
  `status` tinyint(3) unsigned DEFAULT NULL COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) DEFAULT NULL COMMENT '提示消息',
  `operator_type` tinyint(3) unsigned DEFAULT NULL COMMENT '操作类别',
  `login_time` datetime DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统访问记录';

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
BEGIN;
INSERT INTO `sys_logininfor` VALUES (1, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', 0, '登录成功', NULL, '2019-11-20 19:16:59');
COMMIT;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父菜单ID',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `url` varchar(200) DEFAULT '#' COMMENT '请求地址',
  `target` varchar(20) DEFAULT '' COMMENT '打开方式（menuItem页签 menuBlank新窗口）',
  `menu_type` char(1) DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `perms` varchar(100) DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1092 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='菜单权限表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 5, '#', '', 'M', '0', '', 'fa fa-gear', 'admin', '2018-03-16 11:33:00', 'admin', '2019-11-08 17:37:00', '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 6, '#', '', 'M', '0', '', 'fa fa-video-camera', 'admin', '2018-03-16 11:33:00', 'admin', '2019-11-08 17:37:13', '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 7, '#', '', 'M', '0', '', 'fa fa-bars', 'admin', '2018-03-16 11:33:00', 'admin', '2019-11-08 17:37:21', '系统工具目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, '/system/user', '', 'C', '0', 'system:user:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, '/system/role', '', 'C', '0', 'system:role:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, '/system/menu', '', 'C', '0', 'system:menu:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, '/system/dept', '', 'C', '0', 'system:dept:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, '/system/post', '', 'C', '0', 'system:post:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, '/system/dict', '', 'C', '0', 'system:dict:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, '/system/config', '', 'C', '0', 'system:config:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, '/system/notice', '', 'C', '0', 'system:notice:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, '#', '', 'M', '0', '', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, '/monitor/online', '', 'C', '0', 'monitor:online:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, '/monitor/job', '', 'C', '0', 'monitor:job:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, '/monitor/data', '', 'C', '0', 'monitor:data:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 3, '/monitor/server', '', 'C', '0', 'monitor:server:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '表单构建', 3, 1, '/tool/build', '', 'C', '0', 'tool:build:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '表单构建菜单');
INSERT INTO `sys_menu` VALUES (114, '代码生成', 3, 2, '/tool/gen', '', 'C', '0', 'tool:gen:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '代码生成菜单');
INSERT INTO `sys_menu` VALUES (115, '系统接口', 3, 3, '/tool/swagger', '', 'C', '0', 'tool:swagger:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, '/monitor/operlog', '', 'C', '0', 'monitor:operlog:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, '/monitor/logininfor', '', 'C', '0', 'monitor:logininfor:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 100, 1, '#', '', 'F', '0', 'system:user:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 2, '#', '', 'F', '0', 'system:user:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 3, '#', '', 'F', '0', 'system:user:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 4, '#', '', 'F', '0', 'system:user:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1004, '用户导出', 100, 5, '#', '', 'F', '0', 'system:user:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1005, '用户导入', 100, 6, '#', '', 'F', '0', 'system:user:import', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1006, '重置密码', 100, 7, '#', '', 'F', '0', 'system:user:resetPwd', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1007, '角色查询', 101, 1, '#', '', 'F', '0', 'system:role:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1008, '角色新增', 101, 2, '#', '', 'F', '0', 'system:role:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1009, '角色修改', 101, 3, '#', '', 'F', '0', 'system:role:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1010, '角色删除', 101, 4, '#', '', 'F', '0', 'system:role:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1011, '角色导出', 101, 5, '#', '', 'F', '0', 'system:role:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1012, '菜单查询', 102, 1, '#', '', 'F', '0', 'system:menu:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1013, '菜单新增', 102, 2, '#', '', 'F', '0', 'system:menu:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1014, '菜单修改', 102, 3, '#', '', 'F', '0', 'system:menu:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1015, '菜单删除', 102, 4, '#', '', 'F', '0', 'system:menu:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1016, '部门查询', 103, 1, '#', '', 'F', '0', 'system:dept:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1017, '部门新增', 103, 2, '#', '', 'F', '0', 'system:dept:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1018, '部门修改', 103, 3, '#', '', 'F', '0', 'system:dept:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1019, '部门删除', 103, 4, '#', '', 'F', '0', 'system:dept:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1020, '岗位查询', 104, 1, '#', '', 'F', '0', 'system:post:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1021, '岗位新增', 104, 2, '#', '', 'F', '0', 'system:post:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1022, '岗位修改', 104, 3, '#', '', 'F', '0', 'system:post:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1023, '岗位删除', 104, 4, '#', '', 'F', '0', 'system:post:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1024, '岗位导出', 104, 5, '#', '', 'F', '0', 'system:post:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1025, '字典查询', 105, 1, '#', '', 'F', '0', 'system:dict:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1026, '字典新增', 105, 2, '#', '', 'F', '0', 'system:dict:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1027, '字典修改', 105, 3, '#', '', 'F', '0', 'system:dict:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1028, '字典删除', 105, 4, '#', '', 'F', '0', 'system:dict:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1029, '字典导出', 105, 5, '#', '', 'F', '0', 'system:dict:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1030, '参数查询', 106, 1, '#', '', 'F', '0', 'system:config:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1031, '参数新增', 106, 2, '#', '', 'F', '0', 'system:config:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1032, '参数修改', 106, 3, '#', '', 'F', '0', 'system:config:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1033, '参数删除', 106, 4, '#', '', 'F', '0', 'system:config:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1034, '参数导出', 106, 5, '#', '', 'F', '0', 'system:config:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1035, '公告查询', 107, 1, '#', '', 'F', '0', 'system:notice:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1036, '公告新增', 107, 2, '#', '', 'F', '0', 'system:notice:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1037, '公告修改', 107, 3, '#', '', 'F', '0', 'system:notice:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1038, '公告删除', 107, 4, '#', '', 'F', '0', 'system:notice:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1039, '操作查询', 500, 1, '#', '', 'F', '0', 'monitor:operlog:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1040, '操作删除', 500, 2, '#', '', 'F', '0', 'monitor:operlog:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1041, '详细信息', 500, 3, '#', '', 'F', '0', 'monitor:operlog:detail', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1042, '日志导出', 500, 4, '#', '', 'F', '0', 'monitor:operlog:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1043, '登录查询', 501, 1, '#', '', 'F', '0', 'monitor:logininfor:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1044, '登录删除', 501, 2, '#', '', 'F', '0', 'monitor:logininfor:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1045, '日志导出', 501, 3, '#', '', 'F', '0', 'monitor:logininfor:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1046, '账户解锁', 501, 4, '#', '', 'F', '0', 'monitor:logininfor:unlock', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1047, '在线查询', 109, 1, '#', '', 'F', '0', 'monitor:online:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1048, '批量强退', 109, 2, '#', '', 'F', '0', 'monitor:online:batchForceLogout', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1049, '单条强退', 109, 3, '#', '', 'F', '0', 'monitor:online:forceLogout', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1050, '任务查询', 110, 1, '#', '', 'F', '0', 'monitor:job:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1051, '任务新增', 110, 2, '#', '', 'F', '0', 'monitor:job:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1052, '任务修改', 110, 3, '#', '', 'F', '0', 'monitor:job:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1053, '任务删除', 110, 4, '#', '', 'F', '0', 'monitor:job:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1054, '状态修改', 110, 5, '#', '', 'F', '0', 'monitor:job:changeStatus', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1055, '任务详细', 110, 6, '#', '', 'F', '0', 'monitor:job:detail', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1056, '任务导出', 110, 7, '#', '', 'F', '0', 'monitor:job:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1057, '生成查询', 114, 1, '#', '', 'F', '0', 'tool:gen:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1058, '生成修改', 114, 2, '#', '', 'F', '0', 'tool:gen:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1059, '生成删除', 114, 3, '#', '', 'F', '0', 'tool:gen:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1060, '预览代码', 114, 4, '#', '', 'F', '0', 'tool:gen:preview', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1061, '生成代码', 114, 5, '#', '', 'F', '0', 'tool:gen:code', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES (1062, '人员', 3, 1, '/person/info', '', 'C', '0', 'person:info:view', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '人员菜单');
INSERT INTO `sys_menu` VALUES (1063, '人员查询', 1062, 1, '#', '', 'F', '0', 'person:info:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1064, '人员新增', 1062, 2, '#', '', 'F', '0', 'person:info:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1065, '人员修改', 1062, 3, '#', '', 'F', '0', 'person:info:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1066, '人员删除', 1062, 4, '#', '', 'F', '0', 'person:info:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1067, '人员导出', 1062, 5, '#', '', 'F', '0', 'person:info:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1068, '商户', 3, 1, '/client/info', '', 'C', '0', 'merchant:info:view', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '商户菜单');
INSERT INTO `sys_menu` VALUES (1069, '商户查询', 1068, 1, '#', '', 'F', '0', 'merchant:info:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1070, '商户新增', 1068, 2, '#', '', 'F', '0', 'merchant:info:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1071, '商户修改', 1068, 3, '#', '', 'F', '0', 'merchant:info:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1072, '商户删除', 1068, 4, '#', '', 'F', '0', 'merchant:info:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1073, '商户导出', 1068, 5, '#', '', 'F', '0', 'merchant:info:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1074, '客户端', 3, 1, '/client/info', '', 'C', '0', 'client:info:view', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '客户端菜单');
INSERT INTO `sys_menu` VALUES (1075, '客户端查询', 1074, 1, '#', '', 'F', '0', 'client:info:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1076, '客户端新增', 1074, 2, '#', '', 'F', '0', 'client:info:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1077, '客户端修改', 1074, 3, '#', '', 'F', '0', 'client:info:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1078, '客户端删除', 1074, 4, '#', '', 'F', '0', 'client:info:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1079, '客户端导出', 1074, 5, '#', '', 'F', '0', 'client:info:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1080, '终端', 3, 1, '/client/info', '', 'C', '0', 'client:info:view', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '终端菜单');
INSERT INTO `sys_menu` VALUES (1081, '终端查询', 1080, 1, '#', '', 'F', '0', 'terminal:info:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1082, '终端新增', 1080, 2, '#', '', 'F', '0', 'terminal:info:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1083, '终端修改', 1080, 3, '#', '', 'F', '0', 'terminal:info:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1084, '终端删除', 1080, 4, '#', '', 'F', '0', 'terminal:info:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1085, '终端导出', 1080, 5, '#', '', 'F', '0', 'terminal:info:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1086, '人脸数据', 3, 1, '/face/info', '', 'C', '0', 'face:info:view', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '人脸数据菜单');
INSERT INTO `sys_menu` VALUES (1087, '人脸数据查询', 1086, 1, '#', '', 'F', '0', 'face:info:list', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1088, '人脸数据新增', 1086, 2, '#', '', 'F', '0', 'face:info:add', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1089, '人脸数据修改', 1086, 3, '#', '', 'F', '0', 'face:info:edit', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1090, '人脸数据删除', 1086, 4, '#', '', 'F', '0', 'face:info:remove', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
INSERT INTO `sys_menu` VALUES (1091, '人脸数据导出', 1086, 5, '#', '', 'F', '0', 'face:info:export', '#', 'admin', '2018-03-01 00:00:00', 'ry', '2018-03-01 00:00:00', '');
COMMIT;

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice` (
  `notice_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '公告id',
  `notice_title` varchar(50) NOT NULL COMMENT '公告标题',
  `notice_type` char(2) NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` varchar(500) NOT NULL COMMENT '公告内容',
  `status` tinyint(3) unsigned DEFAULT NULL COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='通知公告表';

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log` (
  `oper_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) DEFAULT '' COMMENT '模块标题',
  `action` char(2) DEFAULT '0' COMMENT '业务功能',
  `method` varchar(100) DEFAULT '' COMMENT '方法名称',
  `access` char(1) DEFAULT '0' COMMENT '访问类型（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(30) DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(255) DEFAULT '' COMMENT '请求参数',
  `status` char(1) DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='操作日志记录';

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
BEGIN;
INSERT INTO `sys_oper_log` VALUES (100, '代码生成', '6', 'org.miser.generator.controller.GenController.importTableSave()', NULL, 'admin', '升腾云支付', '/face-admin/tool/gen/importTable', '127.0.0.1', '内网IP', '参数1: \"face_person_info\" ', '0', NULL, '2021-06-25 15:28:03');
INSERT INTO `sys_oper_log` VALUES (101, '代码生成', '2', 'org.miser.generator.controller.GenController.editSave()', NULL, 'admin', '升腾云支付', '/face-admin/tool/gen/edit', '127.0.0.1', '内网IP', '参数1: {\"remark\":\"\",\"params\":{\"treeName\":\"\",\"treeParentCode\":\"\",\"treeCode\":\"\"},\"tableId\":1,\"tableName\":\"face_person_info\",\"tableComment\":\"人员表\",\"className\":\"PersonInfo\",\"tplCategory\":\"crud\",\"packageName\":\"com.centerm.face.person\",\"moduleName\":\"person\",\"busin', '0', NULL, '2021-06-25 15:28:30');
INSERT INTO `sys_oper_log` VALUES (102, '代码生成', '8', 'org.miser.generator.controller.GenController.genCode()', NULL, 'admin', '升腾云支付', '/face-admin/tool/gen/genCode/face_person_info', '127.0.0.1', '内网IP', '参数2: \"face_person_info\" ', '0', NULL, '2021-06-25 15:28:32');
INSERT INTO `sys_oper_log` VALUES (103, '代码生成', '8', 'org.miser.generator.controller.GenController.genCode()', NULL, 'admin', '升腾云支付', '/face-admin/tool/gen/genCode/face_person_info', '127.0.0.1', '内网IP', '参数2: \"face_person_info\" ', '0', NULL, '2021-06-25 15:31:04');
INSERT INTO `sys_oper_log` VALUES (104, '代码生成', '6', 'org.miser.generator.controller.GenController.importTableSave()', NULL, 'admin', '升腾云支付', '/face-admin/tool/gen/importTable', '127.0.0.1', '内网IP', '参数1: \"face_face_info\" ', '0', NULL, '2021-06-28 11:14:23');
INSERT INTO `sys_oper_log` VALUES (105, '代码生成', '2', 'org.miser.generator.controller.GenController.editSave()', NULL, 'admin', '升腾云支付', '/face-admin/tool/gen/edit', '127.0.0.1', '内网IP', '参数1: {\"remark\":\"\",\"params\":{\"treeName\":\"\",\"treeParentCode\":\"\",\"treeCode\":\"\"},\"tableId\":2,\"tableName\":\"face_face_info\",\"tableComment\":\"人脸数据表\",\"className\":\"Info\",\"tplCategory\":\"crud\",\"packageName\":\"com.centerm.face.person\",\"moduleName\":\"face\",\"businessName\"', '0', NULL, '2021-06-28 11:14:51');
INSERT INTO `sys_oper_log` VALUES (106, '代码生成', '8', 'org.miser.generator.controller.GenController.genCode()', NULL, 'admin', '升腾云支付', '/face-admin/tool/gen/genCode/face_face_info', '127.0.0.1', '内网IP', '参数2: \"face_face_info\" ', '0', NULL, '2021-06-28 11:14:56');
COMMIT;

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='岗位信息表';

-- ----------------------------
-- Records of sys_post
-- ----------------------------
BEGIN;
INSERT INTO `sys_post` VALUES (1, 'GM', '总经理', 1, '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', 'General Manager');
COMMIT;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `role_name` varchar(30) NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) NOT NULL COMMENT '角色编码',
  `role_sort` int(10) DEFAULT NULL COMMENT '显示顺序',
  `status` tinyint(3) unsigned NOT NULL COMMENT '角色状态（0正常 1停用）',
  `data_scope` char(1) DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限）',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `is_deleted` tinyint(4) unsigned DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色信息表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_role` VALUES (1, '管理员', 'admin', 1, 0, '1', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '管理员', 0);
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, 0, '2', 'admin', '2018-03-16 11:33:00', 'admin', '2018-03-16 11:33:00', '普通角色', 0);
COMMIT;

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` int(10) unsigned NOT NULL COMMENT '角色id',
  `menu_id` int(10) unsigned NOT NULL COMMENT '菜单id',
  PRIMARY KEY (`role_id`,`menu_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色和菜单关联表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 113);
INSERT INTO `sys_role_menu` VALUES (2, 114);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1000);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1002);
INSERT INTO `sys_role_menu` VALUES (2, 1003);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1006);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1009);
INSERT INTO `sys_role_menu` VALUES (2, 1010);
INSERT INTO `sys_role_menu` VALUES (2, 1011);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1015);
INSERT INTO `sys_role_menu` VALUES (2, 1016);
INSERT INTO `sys_role_menu` VALUES (2, 1017);
INSERT INTO `sys_role_menu` VALUES (2, 1018);
INSERT INTO `sys_role_menu` VALUES (2, 1019);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1023);
INSERT INTO `sys_role_menu` VALUES (2, 1024);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1027);
INSERT INTO `sys_role_menu` VALUES (2, 1028);
INSERT INTO `sys_role_menu` VALUES (2, 1029);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1032);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1036);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1038);
INSERT INTO `sys_role_menu` VALUES (2, 1039);
INSERT INTO `sys_role_menu` VALUES (2, 1040);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 1055);
COMMIT;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `dept_id` bigint(11) DEFAULT NULL COMMENT '机构id',
  `role_key` varchar(45) DEFAULT NULL COMMENT '角色编码',
  `login_name` varchar(30) NOT NULL COMMENT '登录账号',
  `user_name` varchar(30) NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) DEFAULT NULL COMMENT '用户类型（00系统用户）',
  `email` varchar(50) DEFAULT NULL COMMENT '用户邮箱',
  `phone` varchar(11) DEFAULT NULL COMMENT '手机号码',
  `sex` char(1) DEFAULT NULL COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) DEFAULT NULL COMMENT '头像路径',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `salt` varchar(20) DEFAULT NULL COMMENT '盐加密',
  `login_ip` varchar(50) DEFAULT NULL COMMENT '最后登陆ip',
  `login_date` datetime DEFAULT NULL COMMENT '最后登陆时间',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `is_deleted` tinyint(1) unsigned DEFAULT '0' COMMENT '是否删除  0:否  1:是',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户信息表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_user` VALUES (1, 100, 'admin', 'admin', '超级管理员', '0', NULL, NULL, NULL, NULL, '2eb5c9ae7c144c8aebb96ed87d79d361', '111111', '127.0.0.1', '2019-11-20 19:16:59', NULL, NULL, NULL, 0, 0, NULL, '2019-11-20 19:16:59');
COMMIT;

-- ----------------------------
-- Table structure for sys_user_online
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_online`;
CREATE TABLE `sys_user_online` (
  `sessionid` varchar(50) NOT NULL COMMENT '用户会话id',
  `login_name` varchar(50) DEFAULT NULL COMMENT '登录账号',
  `dept_name` varchar(50) DEFAULT NULL COMMENT '部门名称',
  `ipaddr` varchar(50) DEFAULT NULL COMMENT '登录ip地址',
  `login_location` varchar(255) DEFAULT NULL COMMENT '登录地点',
  `browser` varchar(50) DEFAULT NULL COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT NULL COMMENT '操作系统',
  `status` varchar(10) DEFAULT NULL COMMENT '在线状态on_line在线off_line离线',
  `start_timestamp` datetime DEFAULT NULL COMMENT 'session创建时间',
  `last_access_time` datetime DEFAULT NULL COMMENT 'session最后访问时间',
  `expire_time` int(10) DEFAULT NULL COMMENT '超时时间，单位为分钟',
  PRIMARY KEY (`sessionid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='在线用户记录';

-- ----------------------------
-- Records of sys_user_online
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post` (
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `post_id` int(11) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`,`post_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户与岗位关联表';

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_post` VALUES (1, 1);
COMMIT;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户和角色关联表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_role` VALUES (1, 1);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
