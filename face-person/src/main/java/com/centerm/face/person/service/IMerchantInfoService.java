package com.centerm.face.person.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.centerm.face.person.domain.MerchantInfo;
import java.util.List;

/**
 * 商户Service接口
 *
 * @author lidonghang
 * @date 2021-06-25
 */
public interface IMerchantInfoService extends IService<MerchantInfo>{

    /**
     * 查询商户
     *
     * @param id 商户ID
     * @return 商户
     */
    MerchantInfo selectMerchantInfoById(Long id);

    /**
     * 查询商户列表
     *
     * @param merchantInfo 商户
     * @return 商户集合
     */
    List<MerchantInfo> selectMerchantInfoList(MerchantInfo merchantInfo);


}
