package com.centerm.face.person.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;

import com.centerm.face.person.domain.MerchantInfo;
import com.centerm.face.person.mapper.MerchantInfoMapper;
import com.centerm.face.person.service.IMerchantInfoService;
import org.springframework.stereotype.Service;
/**
 * 商户Service业务层处理
 * 
 * @author lidonghang
 * @date 2021-06-25
 */
@Service
public class MerchantInfoServiceImpl extends ServiceImpl<MerchantInfoMapper, MerchantInfo>  implements IMerchantInfoService {
 

    /**
     * 查询商户
     * 
     * @param id 商户ID
     * @return 商户
     */
    @Override
    public MerchantInfo selectMerchantInfoById(Long id){
        return baseMapper.selectMerchantInfoById(id);
    }

    /**
     * 查询商户列表
     * 
     * @param merchantInfo 商户
     * @return 商户
     */
    @Override
    public List<MerchantInfo> selectMerchantInfoList(MerchantInfo merchantInfo){
        return baseMapper.selectMerchantInfoList(merchantInfo);
    }
}
