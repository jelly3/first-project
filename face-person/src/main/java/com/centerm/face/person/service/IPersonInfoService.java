package com.centerm.face.person.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.centerm.face.person.domain.PersonInfo;
import com.centerm.face.person.dto.PersonFaceInfoVO;
import com.centerm.face.person.dto.PersonRegisterDto;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 人员Service接口
 * 
 * @author lidonghang
 * @date 2021-06-25
 */
public interface IPersonInfoService extends IService<PersonInfo>{

    /**
     * 查询人员
     * 
     * @param id 人员ID
     * @return 人员
     */
    PersonInfo selectPersonInfoById(Long id);

    /**
     * 根据面部编号查询人员信息
     *
     * @param faceId 脸部编号
     * @return 人员信息
     */
    PersonInfo selectPersonInfoByFaceId(String faceId);

    /**
     * 查询人员列表
     * 
     * @param personInfo 人员
     * @return 人员集合
     */
    List<PersonInfo> selectPersonInfoList(PersonInfo personInfo);

    /**
     * 注册人员
     * @param personRegisterDto 人脸注册信息实体类
     * @author lidonghang
     * @date 2021/6/28 10:35
     * @return 人员信息
     * @throws Exception
     **/
    PersonInfo registerPerson(PersonRegisterDto personRegisterDto) throws Exception;

    /**
     * 修改人员信息
     * @param personRegisterDto 人脸注册信息实体类
     * @author lidonghang
     * @date 2021/6/28 10:35
     * @return 人员信息
     * @throws Exception
     **/
    PersonInfo updatePerson(PersonRegisterDto personRegisterDto) throws Exception;

    /**
     * 根据上传图片检测人员信息
     * @param merchantId 商户编号
     * @param multipartFile 人脸图片文件
     * @author lidonghang
     * @date 2021/6/29 10:54
     * @return 人员信息
     **/
    List<PersonFaceInfoVO> recognizePersonByPic(Long merchantId, MultipartFile multipartFile) throws Exception;
}
