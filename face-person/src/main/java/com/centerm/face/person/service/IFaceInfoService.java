package com.centerm.face.person.service;

import com.arcsoft.face.FaceFeature;
import com.baomidou.mybatisplus.extension.service.IService;
import com.centerm.face.dto.FaceCompareResultDTO;
import com.centerm.face.person.domain.FaceInfo;
import com.centerm.face.person.domain.PersonInfo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 人脸数据Service接口
 * 
 * @author lidonghang
 * @date 2021-06-28
 */
public interface IFaceInfoService extends IService<FaceInfo>{
    /**
     * 查询人脸数据
     * 
     * @param id 人脸数据ID
     * @return 人脸数据
     */
    FaceInfo selectInfoById(Long id);

    /**
     * 查询人脸数据列表
     * 
     * @param info 人脸数据
     * @return 人脸数据集合
     */
    List<FaceInfo> selectInfoList(FaceInfo info);

    /**
     * 注册人脸信息
     * @param fileList 人脸信息文件
     * @param personInfo 人员信息
     * @author lidonghang
     * @date 2021/6/28 11:25
     **/
    Boolean registerFace(MultipartFile[] fileList, PersonInfo personInfo) throws Exception;

    /**
     * 根据传入人脸信息，获取对应的人脸数据
     * @param faceFeature 人脸特征
     * @param merchantId 商户号
     * @author lidonghang
     * @date 2021/6/28 13:59
     * @return 人脸数据
     **/
    List<FaceCompareResultDTO> getFace(FaceFeature faceFeature, Long merchantId) throws Exception;

    /**
     * 根据传入人脸信息，获取对应的人脸数据
     * @param merchantId 商户编号
     * @param multipartFile 人脸图片文件
     * @author lidonghang
     * @date 2021/6/28 13:59
     * @return 人脸数据
     **/
    List<FaceCompareResultDTO> getFace(MultipartFile multipartFile,Long merchantId) throws Exception;
}
