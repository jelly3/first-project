package com.centerm.face.person.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.miser.common.enums.SexEnum;
import org.miser.common.validation.EnumCheck;
import org.miser.common.validation.GroupInsert;
import org.miser.common.validation.GroupUpdate;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 人员注册实体类
 * @author lidonghang
 * @date 2021/6/28
 */
@ApiModel(value = "人脸注册/修改实体类")
@Data
public class PersonRegisterDto {

    @ApiModelProperty(value = "商户编号",hidden = true)
    @NotNull(groups = {GroupUpdate.class})
    private Long id;

    @ApiModelProperty(value = "商户编号",hidden = true)
    private Long merchantId;

    @ApiModelProperty(value = "电话号码")
    private String phone;

    @ApiModelProperty(value = "姓名")
    @NotEmpty(groups = {GroupInsert.class})
    private String name;

    @ApiModelProperty(value = "性别（0:男，1：女，2：未知）")
    @EnumCheck(enumClass = SexEnum.class,groups = {GroupInsert.class})
    @NotNull(groups = {GroupInsert.class})
    private String gender;

    @ApiModelProperty(value = "图片文件",hidden = true)
    private MultipartFile[] multipartFile;
}
