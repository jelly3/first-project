package com.centerm.face.person.mapper;

import com.centerm.face.person.domain.PersonInfo;
import java.util.List;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 人员Mapper接口
 * 
 * @author lidonghang
 * @date 2021-06-25
 */
@Repository
public interface PersonInfoMapper  extends BaseMapper<PersonInfo>{
    /**
     * 查询人员
     * 
     * @param id 人员ID
     * @return 人员
     */
    PersonInfo selectPersonInfoById(Long id);

    /**
     * 查询人员列表
     * 
     * @param personInfo 人员
     * @return 人员集合
     */
    List<PersonInfo> selectPersonInfoList(PersonInfo personInfo);


    /**
     * 根据面部Id查询人员信息
     * @param faceId 面部ID
     * @return 人员信息
     * @author lidonghang
     * @date 2021/6/29 11:18
     **/
    PersonInfo selectPersonInfoByFaceId(String faceId);
}
