package com.centerm.face.person.mapper;

import com.centerm.face.person.domain.FaceInfo;
import java.util.List;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 人脸数据Mapper接口
 * 
 * @author lidonghang
 * @date 2021-06-28
 */
@Repository
public interface FaceInfoMapper extends BaseMapper<FaceInfo>{
    /**
     * 查询人脸数据
     * 
     * @param id 人脸数据ID
     * @return 人脸数据
     */
    public FaceInfo selectInfoById(Long id);

    /**
     * 查询人脸数据列表
     * 
     * @param info 人脸数据
     * @return 人脸数据集合
     */
    public List<FaceInfo> selectInfoList(FaceInfo info);


}
