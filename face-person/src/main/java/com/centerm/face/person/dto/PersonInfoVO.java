package com.centerm.face.person.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.miser.common.annotation.Excel;

/**
 * 人员信息
 * @author lidonghang
 * @date 2021/6/29
 */
@ApiModel(value = "人员信息")
@Data
public class PersonInfoVO {

    @ApiModelProperty(value = "人员编号")
    private Long id;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "性别")
    private Integer gender;
}
