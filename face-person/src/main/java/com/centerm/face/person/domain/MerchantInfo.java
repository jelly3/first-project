package com.centerm.face.person.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import org.miser.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import java.util.Date;
import org.miser.common.core.domain.BaseEntity;

/**
 * 商户对象 face_merchant_info
 *
 * @author lidonghang
 * @date 2021-06-25
 */
@ApiModel(value = "商户对象")
@Data
@ToString
@TableName("face_merchant_info")
@Accessors(chain = true)
public class MerchantInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;


    /** 自增主键 */
    @ApiModelProperty(value = "${comment}")
    private Long id;

    /** 机构id */
    @Excel(name = "机构id")
    @ApiModelProperty(value = "机构id")
    private Long deptId;

    /** 联系电话 */
    @Excel(name = "联系电话")
    @ApiModelProperty(value = "联系电话")
    private String phone;

    /** 联系人名称 */
    @Excel(name = "联系人名称")
    @ApiModelProperty(value = "联系人名称")
    private String contactName;

    /** 商户名称 */
    @Excel(name = "商户名称")
    @ApiModelProperty(value = "商户名称")
    private String merchantName;

    /** 状态（0：无效，1：有效） */
    @Excel(name = "状态", readConverterExp = "0=：无效，1：有效")
    @ApiModelProperty(value = "状态")
    private Integer status;




}
