package com.centerm.face.person.mapper;

import java.util.List;
import com.centerm.face.person.domain.MerchantInfo;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 商户Mapper接口
 *
 * @author lidonghang
 * @date 2021-06-25
 */
@Repository
public interface MerchantInfoMapper  extends BaseMapper<MerchantInfo>{
    /**
     * 查询商户
     *
     * @param id 商户ID
     * @return 商户
     */
    MerchantInfo selectMerchantInfoById(Long id);

    /**
     * 查询商户列表
     *
     * @param merchantInfo 商户
     * @return 商户集合
     */
    List<MerchantInfo> selectMerchantInfoList(MerchantInfo merchantInfo);


}
