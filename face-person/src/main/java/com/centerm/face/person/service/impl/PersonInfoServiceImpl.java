package com.centerm.face.person.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.Comparator;
import java.util.List;

import com.centerm.face.dto.FaceCompareResultDTO;
import com.centerm.face.person.domain.MerchantInfo;
import com.centerm.face.person.dto.PersonFaceInfoVO;
import com.centerm.face.person.dto.PersonRegisterDto;
import com.centerm.face.person.service.IFaceInfoService;
import com.centerm.face.person.service.IMerchantInfoService;
import com.google.common.collect.Lists;
import org.miser.common.constant.Constants;
import org.miser.common.utils.data.ApiAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.centerm.face.person.mapper.PersonInfoMapper;
import com.centerm.face.person.domain.PersonInfo;
import com.centerm.face.person.service.IPersonInfoService;
import org.springframework.web.multipart.MultipartFile;

/**
 * 人员Service业务层处理
 * 
 * @author lidonghang
 * @date 2021-06-25
 */
@Service
public class PersonInfoServiceImpl extends ServiceImpl<PersonInfoMapper, PersonInfo>  implements IPersonInfoService {
 
    @Autowired
    private IFaceInfoService faceInfoService;

    @Autowired
    private IMerchantInfoService merchantInfoService;

    @Autowired
    private IPersonInfoService personInfoService;


    /**
     * 查询人员
     * 
     * @param id 人员ID
     * @return 人员
     */
    @Override
    public PersonInfo selectPersonInfoById(Long id){
        return baseMapper.selectPersonInfoById(id);
    }

    @Override
    public PersonInfo selectPersonInfoByFaceId(String faceId) {
        return baseMapper.selectPersonInfoByFaceId(faceId);
    }

    /**
     * 查询人员列表
     * 
     * @param personInfo 人员
     * @return 人员
     */
    @Override
    public List<PersonInfo> selectPersonInfoList(PersonInfo personInfo){
        return baseMapper.selectPersonInfoList(personInfo);
    }

    @Override
    public PersonInfo registerPerson(PersonRegisterDto personRegisterDto) throws Exception {
        PersonInfo personInfo = new PersonInfo();
        personInfo.setMerchantId(personRegisterDto.getMerchantId());
        personInfo.setGender(Integer.valueOf(personRegisterDto.getGender()));
        personInfo.setName(personRegisterDto.getName());
        personInfo.setPhone(personRegisterDto.getPhone());
        personInfo.setStatus(Constants.YES);
        save(personInfo);
        if(personRegisterDto.getMultipartFile() != null){
            faceInfoService.registerFace(personRegisterDto.getMultipartFile(),personInfo);
        }
        return personInfo;
    }

    @Override
    public PersonInfo updatePerson(PersonRegisterDto personRegisterDto) throws Exception {
        PersonInfo personInfo = getById(personRegisterDto.getId());
        ApiAssert.isFalse("person.not.exists",personInfo == null);
        personInfo.setMerchantId(personRegisterDto.getMerchantId());
        personInfo.setGender(Integer.valueOf(personRegisterDto.getGender()));
        personInfo.setName(personRegisterDto.getName());
        personInfo.setPhone(personRegisterDto.getPhone());
        updateById(personInfo);
        if(personRegisterDto.getMultipartFile() != null){
            faceInfoService.registerFace(personRegisterDto.getMultipartFile(),personInfo);
        }
        return personInfo;
    }

    @Override
    public List<PersonFaceInfoVO> recognizePersonByPic(Long merchantId, MultipartFile multipartFile) throws Exception {
        List<FaceCompareResultDTO> resultList = faceInfoService.getFace(multipartFile, merchantId);
        List<PersonFaceInfoVO> returnList = Lists.newArrayListWithExpectedSize(resultList.size());
        //MerchantInfo merchantInfo = merchantInfoService.getById(merchantId);
        for(FaceCompareResultDTO faceCompareResultDTO : resultList){
            //PersonInfo personInfo = personInfoService.selectPersonInfoByFaceId(faceCompareResultDTO.getFaceId());
            PersonFaceInfoVO personInfoVO = new PersonFaceInfoVO();
            /*personInfoVO.setMerchantId(faceCompareResultDTO.getMerchantId());
            personInfoVO.setMerchantName(merchantInfo.getMerchantName());*/
            personInfoVO.setName("");
            personInfoVO.setFaceId(faceCompareResultDTO.getFaceId());
            personInfoVO.setSimilarValue(faceCompareResultDTO.getSimilarValue());
            returnList.add(personInfoVO);
        }
        // 按相似度排序
        returnList.sort(Comparator.comparingInt(PersonFaceInfoVO::getSimilarValue).reversed());
        return returnList;
    }
}
