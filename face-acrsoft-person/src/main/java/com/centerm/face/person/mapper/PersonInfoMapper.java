package com.centerm.face.person.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.centerm.face.person.domain.PersonInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PersonInfoMapper extends BaseMapper<PersonInfo> {

    /**
     * 根据面部Id查询人员信息
     * @param faceId
     * @return
     */
    PersonInfo selectPersonInfoByFaceId(String faceId);
}
