package com.centerm.face.person.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.centerm.face.person.domain.PersonInfo;
import com.centerm.face.person.dto.PersonRegisterDto;

import java.util.List;

public interface IPersonInfoService extends IService<PersonInfo> {

    /**
     * 新增人员信息
     * @param personRegisterDto  人员信息
     * @return
     */
    PersonInfo addPersonInfo(PersonRegisterDto personRegisterDto);

    /**
     * 根据面部编号查询人员信息
     *
     * @param faceId 脸部编号
     * @return 人员信息
     */
    PersonInfo selectPersonInfoByFaceId(String faceId);

    /**
     * 新增人员信息
     * @param personRegisterDtoList  人员信息
     * @return
     */
    Boolean addBatchPerson(List<PersonRegisterDto> personRegisterDtoList);
}
