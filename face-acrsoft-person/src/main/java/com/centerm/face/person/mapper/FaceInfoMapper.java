package com.centerm.face.person.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.centerm.face.person.domain.FaceInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FaceInfoMapper extends BaseMapper<FaceInfo> {
}
