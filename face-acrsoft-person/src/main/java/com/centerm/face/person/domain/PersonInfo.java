package com.centerm.face.person.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.miser.common.annotation.Excel;
import org.miser.common.core.domain.BaseEntity;

@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "人员对象")
@Data
@ToString
@TableName("face_person_info")
@Accessors(chain = true)
public class PersonInfo extends BaseEntity {

    private static final long serialVersionUID = -4354070796558150287L;

    /** 自增主键 */
    @ApiModelProperty(value = "自增主键")
    private Long id;

    /** 商户编号 */
    @ApiModelProperty(value = "商户编号")
    private Long merchantId;

    /** 手机号 */
    @Excel(name = "手机号")
    @ApiModelProperty(value = "手机号")
    private String phone;

    /** 姓名 */
    @Excel(name = "姓名")
    @ApiModelProperty(value = "姓名")
    private String name;

    /** 性别 */
    @Excel(name = "性别")
    @ApiModelProperty(value = "性别")
    private Integer gender;

    /** 状态（0：禁用，1：启用） */
    @Excel(name = "状态", readConverterExp = "0=：禁用，1：启用")
    @ApiModelProperty(value = "状态")
    private Integer status;
}
