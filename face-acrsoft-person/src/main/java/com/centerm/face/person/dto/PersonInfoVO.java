package com.centerm.face.person.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "人员信息")
@Data
public class PersonInfoVO {

    @ApiModelProperty(value = "人员编号")
    private Long id;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "状态")
    private Integer status;

    @ApiModelProperty(value = "性别")
    private Integer gender;
}
