package com.centerm.face.person.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import cn.hutool.core.exceptions.ExceptionUtil;
import com.arcsoft.face.FaceFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.centerm.face.config.CompareFaceTask;
import com.centerm.face.constant.FaceConstant;
import com.centerm.face.dto.FaceCompareDTO;
import com.centerm.face.dto.FaceCompareResultDTO;
import com.centerm.face.person.domain.FaceInfo;
import com.centerm.face.person.domain.PersonInfo;
import com.centerm.face.person.dto.PersonFaceInfoVO;
import com.centerm.face.person.mapper.FaceInfoMapper;
import com.centerm.face.person.service.IFaceInfoService;
import com.centerm.face.person.service.IPersonInfoService;
import com.centerm.face.util.FaceUtil;
import com.centerm.face.util.ThreadUtil;
import com.centerm.face.utils.FileUtils;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.miser.common.constant.Constants;
import org.miser.common.utils.data.ApiAssert;
import org.miser.common.utils.data.CommonUtils;
import org.miser.common.utils.data.StringUtils;
import org.miser.system.service.ISysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

@Service
@Slf4j
public class FaceInfoServiceImpl extends ServiceImpl<FaceInfoMapper, FaceInfo> implements IFaceInfoService {

    @Autowired
    private ISysConfigService sysConfigService;

    @Autowired
    private IPersonInfoService personInfoService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public Boolean addFaceInfo(MultipartFile[] fileList, PersonInfo personInfo) throws IOException {
        //首先提取人脸信息
        String dirPath = sysConfigService.selectConfigByKey(FaceConstant.FILES_MEMBER_FACE_PATH) + personInfo.getId() + File.separator;
        File dir = new File(dirPath);
        if(!dir.exists()){
            dir.mkdir();
        }
        FileUtils.emptyDir(dirPath);
        List<FaceInfo> resultAdd = Lists.newArrayListWithExpectedSize(fileList.length);
        remove(new QueryWrapper<FaceInfo>().lambda().eq(FaceInfo::getPersonId,personInfo.getId()));
        String fileName;
        for(MultipartFile multipartFile : fileList){
            fileName = StringUtils.isEmpty(multipartFile.getOriginalFilename())?multipartFile.getName():multipartFile.getOriginalFilename();
            FaceInfo faceInfo = new FaceInfo();
            faceInfo.setMerchantId(personInfo.getMerchantId());
            faceInfo.setPersonId(personInfo.getId());
            faceInfo.setFaceFeature(getFaceFeature(multipartFile));
            faceInfo.setFaceId(CommonUtils.getStringRandom(10));
            String filename = faceInfo.getFaceId();
            if (CommonUtils.isNotEmpty(fileName)) {
                filename += fileName.substring(fileName.lastIndexOf("."));
            }
            try {
                FileUtils.createFileByByte(multipartFile.getBytes(),dirPath,filename);
            } catch (IOException e) {
                log.warn("人脸图片保存失败：{}", ExceptionUtil.stacktraceToString(e));
                return false;
            }
            faceInfo.setFacePicture(filename);
            faceInfo.setStatus(Constants.YES);
            resultAdd.add(faceInfo);
        }
        return saveBatch(resultAdd);
    }

    @Override
    public List<PersonFaceInfoVO> recognizePersonByPic(Long merchantId,MultipartFile multipartFile) throws Exception{
        List<FaceCompareResultDTO> resultList = this.getFace(merchantId,multipartFile);
        List<PersonFaceInfoVO> returnList = Lists.newArrayListWithExpectedSize(resultList.size());
        for(FaceCompareResultDTO faceCompareResultDTO : resultList) {
            //PersonInfo personInfo = personInfoService.selectPersonInfoByFaceId(faceCompareResultDTO.getFaceId());
            PersonFaceInfoVO personInfoVO = new PersonFaceInfoVO();
            personInfoVO.setName("");
            personInfoVO.setFaceId(faceCompareResultDTO.getFaceId());
            personInfoVO.setSimilarValue(faceCompareResultDTO.getSimilarValue());
            returnList.add(personInfoVO);
        }
        // 按相似度排序
        returnList.sort(Comparator.comparingInt(PersonFaceInfoVO::getSimilarValue).reversed());
        return returnList;
    }

    @Override
    public List<FaceCompareResultDTO> getFace(Long merchantId,MultipartFile multipartFile) throws Exception{
        // 提取人脸信息
        List<FaceFeature> featureList = Lists.newArrayListWithExpectedSize(1);
        // 特征值提取耗时
        TimeInterval timer = DateUtil.timer();
        featureList.add(new FaceFeature(getFaceFeature(multipartFile)));
        log.debug("特征值提取完成！！总耗时【{}】ms" , timer.interval());
        return compareMerchantFace(merchantId,featureList);
    }

    /**
     * 提取人脸特征
     * @param multipartFile
     * @return
     * @throws Exception
     */
    private byte[] getFaceFeature(MultipartFile multipartFile) throws IOException {
        String fileName = StringUtils.isEmpty(multipartFile.getOriginalFilename())?multipartFile.getName():multipartFile.getOriginalFilename();
        log.debug("当前文件名："+fileName);
        ApiAssert.isTrue("file.type.error",fileName.endsWith("jpg")
                || fileName.endsWith("jpeg")
                || fileName.endsWith("bmp")
                || fileName.endsWith("png"),"jpg/jpeg/bmp/png");
        return FaceUtil.extractFaceFeature(multipartFile).getFeatureData();
    }

    /**
     * 比对人脸信息
     * @param merchantId
     * @param featureList
     * @return
     * @throws Exception
     */
    private List<FaceCompareResultDTO> compareMerchantFace(Long merchantId,List<FaceFeature> featureList) throws Exception {
        List<FaceCompareResultDTO> result = Lists.newLinkedList();
        Integer passValue = Integer.valueOf(sysConfigService.selectConfigByKey(FaceConstant.FACE_PASS_VALUE));
        ExecutorService executorService = ThreadUtil.getFaceService();
        // 将人脸信息按1000个一组分组
        List<List<FaceCompareDTO>> parts = Lists.partition(getMerchantFaceList(merchantId), 1000);
        // 人脸比对计耗时计算启动
        TimeInterval timer = DateUtil.timer();
        CompletionService<List<FaceCompareResultDTO>> completionService = new ExecutorCompletionService(executorService);
        for (List<FaceCompareDTO> part : parts) {
            completionService.submit(new CompareFaceTask(part, featureList, passValue));
        }
        // 按分组收集信息
        for(List<FaceCompareDTO> ignored : parts){
            result.addAll(completionService.take().get());
        }
        // 检测到人脸信息，但是不在人脸库
        ApiAssert.isFalse("no.register.face", result.isEmpty());
        log.debug("比对完成！！总耗时【{}】ms" , timer.interval());
        // 根据脸部ID去重
        return result.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(FaceCompareResultDTO::getFaceId))), ArrayList::new));
    }

    /**
     * 获取指定商户所有有录入并且开通人脸支付的人脸信息
     * @return
     */
    private List<FaceCompareDTO> getMerchantFaceList(Long merchantId){
        // 获取所有有录入并且开通人脸支付的人脸信息,转换成人脸比对实体类
        List<FaceInfo> merchantFaceList = list(new QueryWrapper<FaceInfo>().lambda()
                .eq(FaceInfo::getStatus,Constants.YES)
                .eq(FaceInfo::getMerchantId,merchantId));
        List<FaceCompareDTO> faceInfoDtoList = Lists.newArrayListWithExpectedSize(merchantFaceList.size());
        for(FaceInfo faceInfo : merchantFaceList){
            FaceCompareDTO faceCompareDTO= new FaceCompareDTO();
            faceCompareDTO.setFaceId(faceInfo.getFaceId());
            faceCompareDTO.setFaceFeature(faceInfo.getFaceFeature());
            faceInfoDtoList.add(faceCompareDTO);
        }
        return faceInfoDtoList;
    }
}
