package com.centerm.face.person.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.centerm.face.dto.FaceCompareResultDTO;
import com.centerm.face.person.domain.FaceInfo;
import com.centerm.face.person.domain.PersonInfo;
import com.centerm.face.person.dto.PersonFaceInfoVO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface IFaceInfoService extends IService<FaceInfo> {

    /**
     * 新增人脸信息
     * @param fileList
     * @param personInfo
     * @return
     * @throws Exception
     */
    Boolean addFaceInfo(MultipartFile[] fileList, PersonInfo personInfo) throws IOException;

    /**
     * 人脸识别
     * @param multipartFile
     * @return
     */
    List<PersonFaceInfoVO> recognizePersonByPic(Long merchantId,MultipartFile multipartFile) throws Exception;

    /**
     * 根据传入人脸信息，获取对应的人脸数据
     * @param multipartFile
     * @return
     * @throws Exception
     */
    List<FaceCompareResultDTO> getFace(Long merchantId,MultipartFile multipartFile) throws Exception;
}
