package com.centerm.face.person.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.centerm.face.person.domain.PersonInfo;
import com.centerm.face.person.dto.PersonRegisterDto;
import com.centerm.face.person.mapper.PersonInfoMapper;
import com.centerm.face.person.service.IPersonInfoService;
import com.google.common.collect.Lists;
import org.miser.common.constant.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class PersonInfoServiceImpl extends ServiceImpl<PersonInfoMapper, PersonInfo> implements IPersonInfoService {

    @Autowired
    private PersonInfoMapper personInfoMapper;

    @Override
    public PersonInfo addPersonInfo(PersonRegisterDto personRegisterDto) {
        PersonInfo personInfo = new PersonInfo();
        personInfo.setMerchantId(personRegisterDto.getMerchantId());
        personInfo.setGender(Integer.valueOf(personRegisterDto.getGender()));
        personInfo.setName(personRegisterDto.getName());
        personInfo.setPhone(personRegisterDto.getPhone());
        personInfo.setStatus(Constants.YES);
        save(personInfo);
        return personInfo;
    }

    @Override
    public PersonInfo selectPersonInfoByFaceId(String faceId) {
        return personInfoMapper.selectPersonInfoByFaceId(faceId);
    }

    @Override
    public Boolean addBatchPerson(List<PersonRegisterDto> personRegisterDtoList) {
        List<PersonInfo> personInfoList = Lists.newArrayListWithCapacity(personRegisterDtoList.size());
        AtomicLong i = new AtomicLong(36210L);
        personRegisterDtoList.forEach(personRegisterDto -> {
            PersonInfo personInfo = new PersonInfo();
            personInfo.setId(i.incrementAndGet());
            personInfo.setMerchantId(12L);
            personInfo.setGender(0);
            personInfo.setName(personInfo.getId()+"");
            personInfo.setPhone("");
            personInfo.setStatus(Constants.YES);
            personInfoList.add(personInfo);
        });
        saveBatch(personInfoList);
        return true;
    }
}
