package com.centerm.face.person.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.miser.common.annotation.Excel;
import org.miser.common.core.domain.BaseEntity;

@ApiModel(value = "人脸数据对象")
@Data
@ToString
@TableName("face_face_info")
@Accessors(chain = true)
public class FaceInfo extends BaseEntity
{

    private static final long serialVersionUID = -7822926914542720581L;

    /** 自增主键 */
    @ApiModelProperty(value = "${comment}")
    private Long id;

    /** 人员编号 */
    @Excel(name = "人员编号")
    @ApiModelProperty(value = "人员编号")
    private Long personId;

    /** 商户编号 */
    @Excel(name = "商户编号")
    @ApiModelProperty(value = "商户编号")
    private Long merchantId;

    /** 人脸数据编号 */
    @Excel(name = "人脸数据编号")
    @ApiModelProperty(value = "人脸数据编号")
    private String faceId;

    /** 人脸特征 */
    @Excel(name = "人脸特征")
    @ApiModelProperty(value = "人脸特征")
    private byte[] faceFeature;

    /** 人脸图片 */
    @Excel(name = "人脸图片")
    @ApiModelProperty(value = "人脸图片")
    private String facePicture;

    /** 状态（0：禁用，1：启用） */
    @Excel(name = "状态", readConverterExp = "0=：禁用，1：启用")
    @ApiModelProperty(value = "状态")
    private Integer status;


}
