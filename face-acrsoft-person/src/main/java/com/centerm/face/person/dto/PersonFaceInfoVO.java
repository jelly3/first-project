package com.centerm.face.person.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "人员信息")
@Data
public class PersonFaceInfoVO {

    @ApiModelProperty(value = "人脸编号")
    private String faceId;

    @ApiModelProperty(value = "人员姓名")
    private String name;

    @ApiModelProperty(value = "相似度")
    private Integer similarValue;
}
