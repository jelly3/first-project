package com.centerm.face.gateway.controller.auth;

import com.centerm.face.client.domain.ClientInfo;
import com.centerm.face.client.service.IClientInfoService;
import com.centerm.face.client.service.ITerminalInfoService;
import org.miser.common.utils.data.ApiAssert;
import org.miser.common.utils.data.StringUtils;
import org.miser.framework.shiro.oauth2.domain.OAuthClient;
import org.miser.framework.shiro.oauth2.domain.OauthToken;
import org.miser.framework.shiro.service.Oauth2Service;
import org.miser.common.core.controller.BaseController;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.constraints.NotEmpty;

/**
 * 登录验证
 *
 * @author Barry
 */
@Controller
public class OAuthController extends BaseController {

    @Autowired
    private Oauth2Service oauth2Service;

    @Autowired
    private IClientInfoService clientInfoService;

    @Autowired
    private ITerminalInfoService terminalInfoService;

    /**
     * 请求登陆token
     * @author lidonghang
     * @date 2020/7/21 09:47
     **/
    @PostMapping("/oauth/token")
    @ResponseBody
    public OauthToken token(@RequestParam @NotEmpty String clientId) {

        ClientInfo clientInfo = clientInfoService.getByClientId(clientId);
        ApiAssert.isTrue("client.id.not.exists",clientInfo != null);
        OAuthClient oAuthClient = new OAuthClient();
        BeanUtils.copyProperties(clientInfo,oAuthClient);
//        //判断是否是终端接入
//        if (isTerminalAccess(clientInfo, terminalId)) {
//            oAuthClient.setExtraField(terminalId);
//        }
        return oauth2Service.getToken(oAuthClient);
    }

    /**
     * 刷新token
     * @author lidonghang
     * @date 2020/7/21 09:48
     **/
    @PostMapping("/oauth/refresh")
    @ResponseBody
    public OauthToken refresh(@RequestParam @NotEmpty String clientId, @RequestParam @NotEmpty String refreshToken) {

        ClientInfo clientInfo = clientInfoService.getByClientId(clientId);
        ApiAssert.isTrue("client.id.not.exists",clientInfo != null);
        OAuthClient oAuthClient = new OAuthClient();
        BeanUtils.copyProperties(clientInfo,oAuthClient);
//        //判断是否是终端接入
//        if (isTerminalAccess(clientInfo, terminalId)) {
//            oAuthClient.setExtraField(terminalId);
//        }
        String token = oauth2Service.refreshToken(oAuthClient,refreshToken);
        ApiAssert.isTrue("refresh.token.error", StringUtils.isNotEmpty(token));
        return oauth2Service.getToken(oAuthClient);
    }

//    /**
//     * 判断是否是终端接入及终端的有效性
//     * @author wuyubin
//     * @param clientInfo 客户端信息
//     * @param terminalId 请求中携带的终端号(可能为null)
//     * @return 终端接入:true,无终端接入:false
//     * @date 2021-02-03
//    */
//    private boolean isTerminalAccess(ClientInfo clientInfo, String terminalId){
//        //终端接入时判断终端的状态是否可用
//        if (ClientAccessTypeEnum.Terminal.getCode().equals(clientInfo.getType())){
//            if (StringUtils.isEmpty(terminalId)) {
//                ApiAssert.failure("terminal.id.empty");
//            }
//            TerminalInfo terminalInfo = terminalInfoService.getOne(new QueryWrapper<TerminalInfo>().lambda()
//                    .eq(TerminalInfo::getClientId, clientInfo.getClientId())
//                    .eq(TerminalInfo::getTerminalId, terminalId)
//                    .eq(TerminalInfo::getStatus, TerminalStatusEnum.Valid.getCode()));
//            ApiAssert.isTrue("terminal.id.not.valid",terminalInfo != null);
//            return true;
//        }
//        return false;
//    }
}
