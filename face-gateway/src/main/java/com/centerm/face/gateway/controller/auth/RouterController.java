package com.centerm.face.gateway.controller.auth;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.UnauthorizedException;
import org.apache.shiro.subject.Subject;
import org.miser.common.annotation.LogIgnore;
import org.miser.common.annotation.PassUrl;
import org.miser.common.constant.ErrorConstants;
import org.miser.common.core.domain.ResponseResult;
import org.miser.common.exception.BusinessException;
import org.miser.common.utils.data.StringUtils;
import org.miser.common.core.controller.BaseController;
import org.miser.common.utils.http.HttpUtils;
import org.springframework.stereotype.Controller;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * API路由
 *
 * @author Barry
 */
@Controller
@Slf4j
public class RouterController extends BaseController {

    @ApiOperation(value = "请求重定向")
    @RequestMapping(path = "/api/**")
    @LogIgnore
    @PassUrl
    public String router(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (!passUrl(request)) {

            if(request.getAttribute("code") != null){
                String code = request.getAttribute("code").toString();
                Object args = request.getAttribute("args");
                if(StringUtils.isNotEmpty(code) && args != null){
                    throw new BusinessException(code,args.toString().split(","));
                }
                if(StringUtils.isNotEmpty(code)){
                    throw new BusinessException(code);
                }
            }
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isAuthenticated()) {
                throw new BusinessException("unauthorized");
            }
        }
        String url = request.getServletPath().replaceFirst("/api", "");
        return "forward:" + url;
    }

    @RequestMapping(path="/open/**")
    public String open(HttpServletRequest request, HttpServletResponse response)  {
        Subject subject = SecurityUtils.getSubject();
        if (subject.isAuthenticated()) {
            String url = request.getRequestURI().replaceFirst("/open", "");
            return HttpUtils.forwardPost(url, request, response);
        } else {
            throw new UnauthorizedException();
        }
    }

    private boolean passUrl(HttpServletRequest request) {
        String requestPath = request.getServletPath();
        log.debug("请求url:{}", requestPath);
        List<String> passUrlList = new ArrayList<>();
        passUrlList.add("/api/login");
        passUrlList.add("/api/401");
        passUrlList.add("/401");
        passUrlList.add("/api/doc.html");
        passUrlList.add("/api/swagger-resources");
        passUrlList.add("/login");
        passUrlList.add("/api/v2/api-docs");
        passUrlList.add("/api/demo");
        if (passUrlList.contains(requestPath)) {
            return true;
        }
        //直接放行
        String[] patchUrl = new String[]{"/api/webjars/**/**/**","/api/oauth/**","/api/test/**", "/api/person/**"};
        PathMatcher matcher = new AntPathMatcher();
        for (String passUrl : patchUrl) {
            if (matcher.match(passUrl, requestPath)) {
                return true;
            }
        }
        return false;
    }

    @ApiOperation(value = "401页面")
    @RequestMapping(path = "/api/401")
    @ResponseBody
    public ResponseResult unauthorizedApi() {
        return ResponseResult.fail(ErrorConstants.UNAUTHORIZED);
    }

    @ApiOperation(value = "login")
    @RequestMapping(path = "/api/login")
    @ResponseBody
    public ResponseResult apiLogin() {
        return ResponseResult.fail(ErrorConstants.UNAUTHORIZED);
    }

    @ApiOperation(value = "401页面")
    @RequestMapping(path = "/401")
    @ResponseBody
    public ResponseResult unauthorized() {
        return ResponseResult.fail(ErrorConstants.UNAUTHORIZED);
    }


    @ApiOperation(value = "login")
    @RequestMapping(path = "/login")
    @ResponseBody
    public ResponseResult login() {
        return ResponseResult.fail(ErrorConstants.UNAUTHORIZED);
    }


    @ApiOperation(value = "登录业务异常")
    @RequestMapping({"/api/loginError/","/api/loginError/**"})
    @PassUrl
    @ResponseBody
    public ResponseResult loginError() {
        return getBusinessResponseResult();
    }

    private ResponseResult getBusinessResponseResult() {
        HttpServletRequest request = getRequest();
        String code = (String) request.getAttribute("code");
        Object args = request.getAttribute("args");
        if(StringUtils.isNotEmpty(code) && args != null){
            return ResponseResult.fail(code,args.toString().split(","));
        }
        if(StringUtils.isNotEmpty(code)){
            return ResponseResult.fail(code);
        }
        return ResponseResult.fail(ErrorConstants.UNAUTHORIZED);
    }

}
