package com.centerm.face.gateway.controller.demo;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.centerm.face.person.domain.PersonInfo;
import com.centerm.face.person.service.IFaceInfoService;
import com.centerm.face.person.service.IPersonInfoService;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.miser.common.annotation.PassUrl;
import org.miser.common.core.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * 业务相关对外接口
 * @author lidonghang
 * @date 2020/7/6
 */
@Api(tags = "示例接口")
@RestController
@RequestMapping("/demo")
@Slf4j
public class DemoController extends BaseController {

    @Autowired
    private IPersonInfoService personInfoService;

    @Autowired
    private IFaceInfoService faceInfoService;

    @ApiOperation(value = "示例接口")
    @PostMapping
    public String detect() {
        return "测试成功";
    }

    /*static LoadingCache<String,String> loadingCache = CacheBuilder.newBuilder()
            .expireAfterWrite(15,TimeUnit.SECONDS)
            .build(new CacheLoader<String, String>() {
                @Override
                public String load(String s) throws Exception {
                    String result = "11";
                    System.out.println("1111111111");
                    getLoadingCache().put("1",result);
                    return result;
                }
            });//在构建时指定自动加载器


    public static LoadingCache getLoadingCache() {
        return loadingCache;
    }

    @ApiOperation(value = "示例接口")
    @PostMapping
    public String detect1() throws ExecutionException {
        return loadingCache.get("1");
    }*/

    @ApiOperation(value = "批量新增人脸数据")
    @PostMapping("/batchAdd/{merchantId}")
    @PassUrl
    public boolean batchAdd(@PathVariable Long merchantId) {
        String path = "C:\\Users\\L\\Desktop\\person\\person\\";
        List<PersonInfo> employeeList = personInfoService.list(new LambdaQueryWrapper<PersonInfo>()
                .eq(PersonInfo::getMerchantId, merchantId)
                .eq(PersonInfo::getStatus, 1));
        int target = 13001;
        File file;
        for (PersonInfo personInfo : employeeList) {
            boolean flag = false;
            while (!flag) {
                try {
                    file = new File(path + target + ".jpg");
                    InputStream inputStream = new FileInputStream(file);
                    MultipartFile[] multipartFiles = new MultipartFile[1];
                    MultipartFile multipartFile = new MockMultipartFile(file.getName(), inputStream);
                    multipartFiles[0] = multipartFile;
                    faceInfoService.registerFace(multipartFiles, personInfo);
                } catch (Exception e) {
                    log.error("batchAdd被抛弃的图片,信息：{}",(path + target));
                    target--;
                    continue;
                }
                flag = true;
                target--;
            }
        }
        return true;
    }

    @ApiOperation(value = "人脸识别耗时计算")
    @PostMapping("/reconizeTime/{merchantId}")
    @PassUrl
    public Boolean reconizeTime(@PathVariable Long merchantId) {
        String path = "/data/face/facepic/person/";
        //String path = "C:\\Users\\L\\Desktop\\person\\person\\";
        List<PersonInfo> employeeList = personInfoService.list(new LambdaQueryWrapper<PersonInfo>()
                .eq(PersonInfo::getStatus, 1)
                .eq(PersonInfo::getMerchantId, merchantId));
        int target = 0;
        File file;
        for (int i=0;i<employeeList.size();i++) {
            if(i == 100) {
                break;
            }
            boolean flag = false;
            while (!flag) {
                file = new File(path + target + ".jpg");
                if(!file.exists()) {
                    target++;
                    continue;
                }
                InputStream inputStream = null;
                try {
                    inputStream = new FileInputStream(file);
                    MultipartFile multipartFile = new MockMultipartFile(file.getName(), inputStream);
                    personInfoService.recognizePersonByPic(merchantId,multipartFile);
                } catch (Exception e) {
                    target++;
                    log.warn(e.getMessage());
                    continue;
                }
                flag = true;
                target++;
            }
        }
        return true;
    }
}
