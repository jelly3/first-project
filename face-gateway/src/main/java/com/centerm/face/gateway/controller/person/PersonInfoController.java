package com.centerm.face.gateway.controller.person;

import com.centerm.face.client.service.IClientInfoService;
import com.centerm.face.person.domain.PersonInfo;
import com.centerm.face.person.dto.PersonFaceInfoVO;
import com.centerm.face.person.service.IFaceInfoService;
import com.centerm.face.person.service.IPersonInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.miser.common.annotation.PassUrl;
import org.miser.common.core.controller.BaseController;
import org.miser.common.utils.data.ApiAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 人员管理
 *
 * @author lidonghang
 * @date 2021/6/28
 */
@RestController
@Api(tags = "人员管理")
@RequestMapping("/api/person")
@Slf4j
public class PersonInfoController extends BaseController {

    @Autowired
    private IPersonInfoService personInfoService;

    @Autowired
    private IClientInfoService clientInfoService;

    @Autowired
    private IFaceInfoService faceInfoService;

    @ApiOperation(value = "识别人脸")
    @PostMapping("/recognize")
    @PassUrl
    public List<PersonFaceInfoVO> recognize(@RequestParam("file") @NotNull MultipartFile file,@RequestParam("merchantId") Long merchantId) throws Exception {
       /* OAuthClient oAuthClient = CommonUtil.getOauthUser();
        String clientId = oAuthClient.getClientId();
        ClientInfo clientInfo = clientInfoService.getByClientId(clientId);*/
        return personInfoService.recognizePersonByPic(merchantId, file);
    }

    /*@ApiOperation(value = "注册人员")
    @PostMapping("/register")
    @PassUrl
    public PersonInfoVO register(@Validated(GroupInsert.class) @RequestBody PersonRegisterDto personRegisterDto) throws Exception {

        PersonInfoVO personInfoVO = new PersonInfoVO();
        OAuthClient oAuthClient = CommonUtil.getOauthUser();
        String clientId = oAuthClient.getClientId();
        ClientInfo clientInfo = clientInfoService.getByClientId(clientId);
        personRegisterDto.setMerchantId(clientInfo.getId());
        PersonInfo personInfo = personInfoService.registerPerson(personRegisterDto);
        BeanUtils.copyProperties(personInfo, personInfoVO);
        return personInfoVO;
    }*/

    @ApiOperation(value = "注册/修改人脸信息")
    @PostMapping("/face/register")
    @PassUrl
    public Boolean faceRegister(@RequestParam("file") @NotNull @NotBlank MultipartFile[] files,
                                @RequestParam("personId") @NotNull Long personId) throws Exception {

        /*OAuthClient oAuthClient = CommonUtil.getOauthUser();
        String clientId = oAuthClient.getClientId();
        ClientInfo clientInfo = clientInfoService.getByClientId(clientId);*/
        PersonInfo personInfo = personInfoService.getById(personId);
        ApiAssert.isFalse("person.not.exists", personInfo == null);
        //ApiAssert.isFalse("person.not.permitted", !clientInfo.getMerchantId().equals(personInfo.getMerchantId()));
        return faceInfoService.registerFace(files, personInfo);
    }
}
