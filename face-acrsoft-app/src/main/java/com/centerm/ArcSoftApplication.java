package com.centerm;


import com.github.pagehelper.autoconfigure.PageHelperAutoConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(scanBasePackages={"org.miser","com.centerm"}, exclude = {DataSourceAutoConfiguration.class, PageHelperAutoConfiguration.class})
@MapperScan({"com.centerm.**.mapper","org.miser.**.mapper"})
public class ArcSoftApplication {
    public static void main(String[] args) {
        SpringApplication.run(ArcSoftApplication.class);
        System.out.println("______ ___  _____  _____       _____   ___ _____ _____ _    _  _____   __\n" +
                "|  ___/ _ \\/  __ \\|  ___|     |  __ \\ / _ \\_   _|  ___| |  | |/ _ \\ \\ / /\n" +
                "| |_ / /_\\ \\ /  \\/| |__ ______| |  \\// /_\\ \\| | | |__ | |  | / /_\\ \\ V / \n" +
                "|  _||  _  | |    |  __|______| | __ |  _  || | |  __|| |/\\| |  _  |\\ /  \n" +
                "| |  | | | | \\__/\\| |___      | |_\\ \\| | | || | | |___\\  /\\  / | | || |  \n" +
                "\\_|  \\_| |_/\\____/\\____/       \\____/\\_| |_/\\_/ \\____/ \\/  \\/\\_| |_/\\_/  \n");
    }
}