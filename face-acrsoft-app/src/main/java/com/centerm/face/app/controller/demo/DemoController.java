package com.centerm.face.app.controller.demo;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.centerm.face.person.domain.PersonInfo;
import com.centerm.face.person.dto.PersonRegisterDto;
import com.centerm.face.person.service.IFaceInfoService;
import com.centerm.face.person.service.IPersonInfoService;
import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.miser.common.annotation.PassUrl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;

@RestController
@Api(tags = "测试接口")
@RequestMapping("/api/demo")
@Slf4j
public class DemoController {

    @Autowired
    private IPersonInfoService personInfoService;

    @Autowired
    private IFaceInfoService faceInfoService;

    @ApiOperation(value = "批量新增人员信息")
    @PostMapping("/addBatchPerson")
    @PassUrl
    public Boolean addBatchPerson() {
        List<PersonRegisterDto> personRegisterDtoList = Lists.newArrayListWithCapacity(10);
        for (int i = 0; i < 10; i++) {
            PersonRegisterDto personRegisterDto = new PersonRegisterDto();
            personRegisterDtoList.add(personRegisterDto);
        }
        return personInfoService.addBatchPerson(personRegisterDtoList);
    }

    @ApiOperation(value = "批量新增人员图片信息")
    @PostMapping("/addBatchFace/{merchantId}")
    @PassUrl
    public Boolean addBatchFace(@PathVariable Long merchantId) {
        String path = "C:\\Users\\L\\Desktop\\person\\person\\";
        List<PersonInfo> employeeList = personInfoService.list(new LambdaQueryWrapper<PersonInfo>()
                .eq(PersonInfo::getStatus, 1)
                .eq(PersonInfo::getMerchantId, merchantId));
        int target = 13001;
        File file;
        for (PersonInfo personInfo : employeeList) {
            boolean flag = false;
            while (!flag) {
                file = new File(path + target + ".jpg");
                InputStream inputStream = null;
                try {
                    inputStream = new FileInputStream(file);
                    MultipartFile[] multipartFiles = new MultipartFile[1];
                    MultipartFile multipartFile = new MockMultipartFile(file.getName(), inputStream);
                    multipartFiles[0] = multipartFile;
                    faceInfoService.addFaceInfo(multipartFiles, personInfo);
                } catch (Exception e) {
                    log.error("addBatchFace被抛弃的图片,信息：{}", (path + target));
                    target--;
                    continue;
                }
                flag = true;
                target--;
            }
        }
        return true;
    }

    @ApiOperation(value = "人脸识别耗时计算")
    @PostMapping("/reconizeTime/{merchantId}")
    @PassUrl
    public Boolean reconizeTime(@PathVariable Long merchantId) {
        //String path = "C:\\Users\\L\\Desktop\\person\\person\\";
        String path = "/data/face/facepic/person/";
        List<PersonInfo> employeeList = personInfoService.list(new LambdaQueryWrapper<PersonInfo>()
                .eq(PersonInfo::getStatus, 1)
                .eq(PersonInfo::getMerchantId, merchantId));
        int target = 0;
        File file;
        for (int i=0;i<employeeList.size();i++) {
            if(i == 100) {
                break;
            }
            boolean flag = false;
            while (!flag) {
                file = new File(path + target + ".jpg");
                if(!file.exists()) {
                    target++;
                    continue;
                }
                InputStream inputStream = null;
                try {
                    inputStream = new FileInputStream(file);
                    MultipartFile multipartFile = new MockMultipartFile(file.getName(), inputStream);
                    faceInfoService.recognizePersonByPic(merchantId,multipartFile);
                } catch (Exception e) {
                    target++;
                    log.warn(e.getMessage());
                    continue;
                }
                flag = true;
                target++;
            }
        }
        return true;
    }
}
