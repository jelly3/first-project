package com.centerm.face.app.controller.person;

import cn.hutool.core.exceptions.ExceptionUtil;
import com.centerm.face.person.domain.PersonInfo;
import com.centerm.face.person.dto.PersonFaceInfoVO;
import com.centerm.face.person.dto.PersonInfoVO;
import com.centerm.face.person.dto.PersonRegisterDto;
import com.centerm.face.person.service.IFaceInfoService;
import com.centerm.face.person.service.IPersonInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.miser.common.annotation.PassUrl;
import org.miser.common.utils.data.ApiAssert;
import org.miser.common.validation.GroupInsert;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

@RestController
@Api(tags = "人员管理")
@RequestMapping("/api/person")
@Slf4j
public class PersonInfoController {

    @Autowired
    private IPersonInfoService personInfoService;

    @Autowired
    private IFaceInfoService faceInfoService;

    @ApiOperation(value = "新增人员信息")
    @PostMapping("/addPersonInfo")
    @PassUrl
    public PersonInfoVO addPersonInfo(@Validated(GroupInsert.class) @RequestBody PersonRegisterDto personRegisterDto) {
        PersonInfoVO personInfoVO = new PersonInfoVO();
        personRegisterDto.setMerchantId(1L);
        PersonInfo personInfo = personInfoService.addPersonInfo(personRegisterDto);
        BeanUtils.copyProperties(personInfo, personInfoVO);
        return personInfoVO;
    }

    @ApiOperation(value = "新增/修改人脸信息")
    @PostMapping("/addFaceInfo")
    @PassUrl
    public Boolean addFaceInfo(@RequestParam(value = "file",required = true) MultipartFile[] files
            ,@RequestParam(value = "personId",required = true) Long personId) {
        PersonInfo personInfo = personInfoService.getById(personId);
        ApiAssert.isFalse("person.not.exists", personInfo == null);
        try {
            return faceInfoService.addFaceInfo(files, personInfo);
        } catch (IOException e) {
            log.error("新增人脸失败,{}", ExceptionUtil.stacktraceToString(e));
        }
        return false;
    }

    @ApiOperation(value = "识别人脸")
    @PostMapping("/recognize")
    @PassUrl
    public List<PersonFaceInfoVO> recognize(@RequestParam("file") @NotNull MultipartFile file,@RequestParam("merchantId") Long merchantId) throws Exception{
        return faceInfoService.recognizePersonByPic(merchantId,file);
    }
}
