#!/bin/bash

profileActive=test
if [ -n "$1" ]; then
    profileActive=$1
fi

mvn clean install -Dmaven.test.skip=true -P$profileActive