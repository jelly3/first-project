package com.centerm.face.constant;

/**
 * 公用常量类
 * @author lidonghang
 * @date 2021/6/29
 */
public class CommonConstants {

    /**
     * 人脸信息缓存key
     */
    public static final String MERCHANT_FACE_INFO = "merchant_face_info";

}
