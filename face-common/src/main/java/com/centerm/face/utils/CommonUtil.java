package com.centerm.face.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.miser.common.exception.BusinessException;
import org.miser.common.utils.data.DateUtils;
import org.miser.common.utils.data.StringUtils;
import org.miser.framework.shiro.oauth2.domain.OAuthClient;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.miser.framework.util.ShiroUtils.getSubjct;

/**
 * 支付通用工具类
 * @author lidonghang
 * @date 2020/7/24
 */
@Slf4j
public class CommonUtil {

    private static final String SHA1_ALGORITHM = "SHA1";
    private static final String CIPHER_ALGORITHM = "AES/ECB/PKCS5Padding";
    private static final String KEY_ALGORITHM = "AES";
    private static final Pattern MESS_PATTERN = Pattern.compile("\\s*|t*|r*|n*");

    /**
     * 解密
     * @author lidonghang
     * @date 2019-07-26 15:53
     **/
    public static String decrypt(String dataStr, String decryptKey) throws Exception{

        log.debug("业务数据密文为【{}】",dataStr);
        SecretKeySpec secretKeySpec = new SecretKeySpec(decryptKey.getBytes(StandardCharsets.US_ASCII), KEY_ALGORITHM);
        Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
        cipher.init(2, secretKeySpec);
        byte[] encrypted1 = CommonUtil.hex2byte(dataStr);
        byte[] original = cipher.doFinal(encrypted1);

        return new String(original, StandardCharsets.UTF_8);
    }

    /**
     * 用openKey加密data字段
     * @author lidonghang
     * @date 2019-07-26 14:46
     **/
    public static String encrypt(String source, String encryptKey) throws Exception{

        log.debug("业务数据明文为【{}】",source);
        SecretKeySpec secretKeySpec = new SecretKeySpec(encryptKey.getBytes(StandardCharsets.US_ASCII), KEY_ALGORITHM);
        Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
        cipher.init(1, secretKeySpec);
        byte[] encrypted = cipher.doFinal(source.getBytes(StandardCharsets.UTF_8));
        return CommonUtil.byte2hex(encrypted);
    }



    /**
     * 按ASC码排序
     * @author lidonghang
     * @date 2019-07-26 14:54
     **/
    public static String sort(Map<String, Object> paramMap) {

        StringBuilder sort = new StringBuilder();
        Map<String,Object> signMap = Maps.newHashMapWithExpectedSize(10);
        if (paramMap != null) {
            String key;
            for (String s : paramMap.keySet()) {
                key = s;
                String value = ((paramMap.get(key) != null) && (!("".equals(paramMap.get(key).toString())))) ? paramMap.get(key).toString() : "";
                signMap.put(key, value);
            }
            ArrayList<Object> list = new ArrayList<>(signMap.keySet());
            TreeSet<Object> signSet = new TreeSet<>(list);

            for (Object o : signSet) {
                key = (String) o;
                sort.append(key).append("=").append(signMap.get(key).toString()).append("&");
            }
            if (!"".equals(sort.toString())) {
                sort = new StringBuilder(sort.substring(0, sort.length() - 1));
            }
        }
        return sort.toString();
    }

    /**
     * SHA1加密
     * @author lidonghang
     * @date 2019-07-26 15:03
     **/
    public static String encryptBySha1(String str) throws Exception{

        MessageDigest digest = MessageDigest.getInstance(SHA1_ALGORITHM);
        digest.update(str.getBytes());
        byte[] messageDigest = digest.digest();
        StringBuilder hexString = new StringBuilder();
        for (byte message : messageDigest) {
            String shaHex = Integer.toHexString(message & 0xFF);

            if (shaHex.length() < 2) {
                hexString.append(0);
            }

            hexString.append(shaHex);
        }

        return hexString.toString().toLowerCase();
    }

    /**
     * 签名
     * @author lidonghang
     * @date 2019-07-26 14:52
     **/
    public static String sign(Map<String, Object> postMap) {

        String sortStr;
        String sign = null;

        try {
            //step1.A~z排序(加上open_key)
            sortStr = CommonUtil.sort(postMap);
            log.debug("【平台签名】排序后的待签名字符串为【{}】",sortStr);
            //step2.SHA1加密（小写）
            sign = CommonUtil.encryptBySha1(sortStr);
            log.debug("【平台签名】sha1签名字符串为【{}】",sign);

        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }

        return sign;
    }


    /**
     * 验签
     * @author lidonghang
     * @date 2019-07-26 15:53
     **/
    public static Boolean verifySign(String responseString, OAuthClient oAuthClient) {

        JSONObject respObject = JSON.parseObject(responseString);
        String respSign = respObject.get("sign").toString();

        // 删除sign节点
        respObject.remove("sign");
        respObject.put("clientId", oAuthClient.getClientId());
        if(oAuthClient.getExtraField() != null){
            respObject.put("terminalId", oAuthClient.getExtraField());
        }

        log.debug("==========开始验签==========");
        // 按A~z排序，串联成字符串进行sha1加密(小写),得到签名
        String verifySign = sign(respObject.getInnerMap());

        log.debug("上送签名为:{}",respSign);
        log.debug("实际签名为:{}",verifySign);
        return respSign.equals(verifySign);
    }

    /**
     * 二进制数组转十六进制字符串
     * @author lidonghang
     * @date 2019-07-26 14:41
     **/
    public static String byte2hex(byte[] result) {

        StringBuilder sb = new StringBuilder(result.length * 2);

        for(Byte b : result){
            int height = ((b >> 4) & 0x0f);
            int low = b & 0x0f;
            sb.append(height > 9 ? (char) ((height - 10) + 'a') : (char) (height + '0'));
            sb.append(low > 9 ? (char) ((low - 10) + 'a') : (char) (low + '0'));
        }

        return sb.toString();
    }

    /**
     * 十六进制字符串转二进制数组
     * @author lidonghang
     * @date 2019-07-26 14:41
     **/
    public static byte[] hex2byte(String strHex) {

        if (strHex == null) {
            return null;
        }

        int l = strHex.length();
        if (l % 2 == 1) {
            return null;
        }

        byte[] b = new byte[l / 2];

        for (int i = 0; i != l / 2; ++i) {
            b[i] = (byte) Integer.parseInt(strHex.substring(i * 2, i * 2 + 2), 16);
        }
        return b;
    }

    /**
     * 元转分
     * @author lidonghang
     * @date 2019-07-31 10:55
     **/
    public static long changeytof(double price) {
        DecimalFormat df = new DecimalFormat("#.00");
        price = Double.parseDouble(df.format(price));
        return (long)(price * 100);
    }

    /**
     * 分转元
     * @author lidonghang
     * @date 2019-08-06 09:42
     **/
    public static String changeftoy(String price) {
        return new BigDecimal(price).divide(new BigDecimal(100)).toString();
    }

    /**
     * 分转元
     * @author lidonghang
     * @date 2019-08-06 09:42
     **/
    public static String changeftoy(String price, Integer scale) {
        return new BigDecimal(price).divide(new BigDecimal(100)).setScale(scale).toString();
    }

    /**
     * 获取订单过期时间（XX秒之后）
     * @author lidonghang
     * @date 2020/7/27 14:45
     **/
    public static String getExpireTime(int seconds){

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.SECOND, seconds);

        return DateUtils.parseDateToStr(DateUtils.YYYYMMDDHHMMSS,calendar.getTime());

    }

    /**
     * 获取当前用户
     * @author lidonghang
     * @date 2020/7/28 09:19
     **/
    public static OAuthClient getOauthUser(){

        Object obj = getSubjct().getPrincipal();
        if(obj instanceof OAuthClient){
            return (OAuthClient) obj;
        }
        throw new BusinessException("unauthorized");
    }

    /**
     * 调用对应的实体类set方法
     * @author lidonghang
     * @date 2020/8/4 13:53
     **/
    public static void callSetMethod(String fieldKey, Object fieldValue, Object object) {


        String firstLetter = fieldKey.substring(0, 1).toUpperCase();
        String setterMethodName = "set" + firstLetter + fieldKey.substring(1);
        for(Method method : object.getClass().getDeclaredMethods()){
            try {
                if(method.getName().equals(setterMethodName)){
                    method.invoke(object, fieldValue);
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    /**
     * 调用对应的实体类get方法
     * @author lidonghang
     * @date 2020/8/4 13:53
     **/
    public static Object callGetMethod(String fieldKey, Object object) {

        String firstLetter = fieldKey.substring(0, 1).toUpperCase();
        String getterMethodName = "get" + firstLetter + fieldKey.substring(1);
        Method getMethod;
        Object value = null;
        try {
            getMethod = object.getClass().getMethod(getterMethodName);
            value = getMethod.invoke(object);
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }

        return value;
    }

    /**
     * 获取美化后的node名称
     * @author lidonghang
     * @date 2020/10/23 16:07
     **/
    public static String getNodeName(String oriNodeName){
        return StringUtils.convertToCamelCase(oriNodeName.toLowerCase());
    }


    /**
     * 判断字符串是否是乱码
     *
     * @param strName 字符串
     * @return 是否是乱码
     */
    public static boolean isMessyCode(String strName) {
        Matcher m = MESS_PATTERN.matcher(strName);
        String after = m.replaceAll("");
        String temp = after.replaceAll("\\p{P}", "");
        char[] ch = temp.trim().toCharArray();
        float chLength = ch.length;
        float count = 0;
        for (int i = 0; i < ch.length; i++) {
            char c = ch[i];
            if (!Character.isLetterOrDigit(c)) {
                if (!isChinese(c)) {
                    count = count + 1;
                }
            }
        }
        float result = count / chLength;
        if (result > 0.4) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 判断字符是否是中文
     *
     * @param c 字符
     * @return 是否是中文
     */
    public static boolean isChinese(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS

                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {
            return true;
        }
        return false;
    }

}
