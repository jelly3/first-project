package com.centerm.face.utils;

import java.io.File;
import java.util.LinkedList;

/**
 * 拓展文件工具类
 * @author lidonghang
 * @date 2021/6/28
 */
public class FileUtils extends org.miser.file.util.FileUtils {

    /**
     * 清空目标文件夹
     * @author lidonghang
     * @date 2020/10/19 16:08
     **/
    public static void emptyDir(String filePath){

        File file = new File(filePath);
        LinkedList<File> list = new LinkedList<>();
        list.push(file);

        while (!list.isEmpty()) {
            File f = list.pop();
            if(!f.isDirectory()){
                f.delete();
            }
            File[] files = f.listFiles();
            if (files != null) {
                for (File fil : files) {
                    list.push(fil);
                }
            }
        }

    }
}
