package com.centerm.face.factory;

import com.arcsoft.face.FaceEngine;
import com.arcsoft.face.FaceFeature;
import com.arcsoft.face.FaceSimilar;
import com.arcsoft.face.enums.CompareModel;
import com.centerm.face.dto.FaceCompareDTO;
import com.centerm.face.dto.FaceCompareResultDTO;
import com.centerm.face.util.FaceEngineUtils;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.pool2.impl.GenericObjectPool;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * 人脸比对TASK
 * @author lidonghang
 * @date 2020/10/13
 */
@Slf4j
public class CompareFaceTask implements Callable<List<FaceCompareResultDTO>> {

    /**
     * 人脸信息列表
     **/
    private List<FaceCompareDTO> faceInfoList;

    /**
     * 待比对人脸特征信息
     **/
    private List<FaceFeature> faceFeatureList;

    /**
     * 人脸比对通过值
     **/
    private Integer passValue;

    public CompareFaceTask(List<FaceCompareDTO> faceInfoList, List<FaceFeature> faceFeatureList, Integer passValue) {
        this.faceInfoList = faceInfoList;
        this.faceFeatureList = faceFeatureList;
        this.passValue = passValue;
    }

    @Override
    public List<FaceCompareResultDTO> call() throws Exception {

        GenericObjectPool<FaceEngine> faceEngineObjectPool = FaceEngineUtils.getFaceEngineObjectPool();
        FaceEngine faceEngine = null;

        //识别到的人脸列表
        List<FaceCompareResultDTO> result = Lists.newLinkedList();

        try {
            faceEngine = faceEngineObjectPool.borrowObject();

            for(int i = 0;i<faceFeatureList.size();i++){

                FaceFeature faceFeature = faceFeatureList.get(i);

                for (FaceCompareDTO faceInfoDTO : faceInfoList) {

                    FaceFeature sourceFaceFeature = new FaceFeature();
                    sourceFaceFeature.setFeatureData(faceInfoDTO.getFaceFeature());
                    FaceSimilar faceSimilar = new FaceSimilar();
                    faceEngine.compareFaceFeature(faceFeature, sourceFaceFeature, CompareModel.ID_PHOTO,faceSimilar);
                    // 获取相似值X100
                    Integer similarValue = multiHundred(faceSimilar.getScore());
                    if(similarValue >= passValue) {
                        // 相似值大于设定值，加入到识别到人脸的列表
                        FaceCompareResultDTO faceCompareResultDTO = new FaceCompareResultDTO();
                        //faceCompareResultDTO.setMerchantId(faceInfoDTO.getMerchantId());
                        faceCompareResultDTO.setFaceId(faceInfoDTO.getFaceId());
                        faceCompareResultDTO.setSimilarValue(similarValue);
                        result.add(faceCompareResultDTO);
                    }
                }
            }

        } catch (Exception e) {
            log.error("人脸比对任务异常！{}", e.getMessage());
            throw e;
        } finally {

            if (faceEngine != null) {
                faceEngineObjectPool.returnObject(faceEngine);
            }
        }

        return result;
    }

    /**
     * 将人脸对比值乘以100
     * @author lidonghang
     * @date 2019/11/21 14:06
     **/
    private Integer multiHundred(Float value) {
        BigDecimal target = new BigDecimal(value);
        BigDecimal hundred = new BigDecimal("100");
        return target.multiply(hundred).intValue();

    }
}
