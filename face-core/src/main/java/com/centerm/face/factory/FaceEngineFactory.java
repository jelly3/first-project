package com.centerm.face.factory;

import com.arcsoft.face.EngineConfiguration;
import com.arcsoft.face.FaceEngine;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;

/**
 * 引擎工厂
 * @author lidonghang
 * @date 2020/10/13
 */
@Slf4j
public class FaceEngineFactory extends BasePooledObjectFactory<FaceEngine> {

    private String appId;
    private String sdkKey;
    private String activeKey;
    private String sdkPath;
    private EngineConfiguration engineConfiguration;



    public FaceEngineFactory(String sdkPath, String appId, String sdkKey, String activeKey, EngineConfiguration engineConfiguration) {
        this.sdkPath = sdkPath;
        this.appId = appId;
        this.sdkKey = sdkKey;
        this.activeKey = activeKey;
        this.engineConfiguration = engineConfiguration;
    }


    @Override
    public FaceEngine create() throws Exception{

        //初始化引擎
        FaceEngine faceEngine = new FaceEngine(sdkPath);
        //激活引擎
        int activeCode = faceEngine.activeOnline(appId, sdkKey, activeKey);
        log.debug("激活人脸识别引擎完成..返回码为"+activeCode);
        int initCode = faceEngine.init(engineConfiguration);
        log.debug("引擎初始化完成..返回码为"+initCode);
        return faceEngine;
    }

    @Override
    public PooledObject<FaceEngine> wrap(FaceEngine faceEngine) {
        return new DefaultPooledObject<>(faceEngine);
    }


    @Override
    public void destroyObject(PooledObject<FaceEngine> p) throws Exception {
        FaceEngine faceEngine = p.getObject();
        int code = faceEngine.unInit();
        log.debug("引擎销毁完成..返回码为"+code);
        super.destroyObject(p);
    }
}
