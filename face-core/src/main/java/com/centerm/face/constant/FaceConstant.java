package com.centerm.face.constant;

/**
 * 员工相关常量
 *
 * @author lidonghang
 * @date 2020/10/13 14:43
 **/
public class FaceConstant {

    /**
     * 人脸引擎线程池大小
     */
    public static final String FACE_THREAD_POOL_SIZE ="face.thread.pool.size";

    /**
     * 人脸引擎路径
     */
    public static final String FACE_SDK_PATH ="face.sdk.path";

    /**
     * 人脸引擎appid
     */
    public static final String FACE_APP_ID ="face.app.id";

    /**
     * 人脸引擎sdkkey
     */
    public static final String FACE_SDK_KEY = "face.sdk.key";

    /**
     * 人脸sdk激活密钥
     */
    public static final String FACE_ACTIVE_KEY ="face.active.key";

    /**
     * 人脸图片路径
     */
    public static final String FILES_MEMBER_FACE_PATH = "files.member.face.path";

    /**
     * 人脸比对通过值
     */
    public static final String FACE_PASS_VALUE = "face.pass.value";

    /**
     * 人脸支付-0：失败
     **/
    public static final Integer FACE_PAY_FAIL = 0;

    /**
     * 人脸支付-1：成功
     **/
    public static final Integer FACE_PAY_SUCCESS = 1;

    /**
     * 人脸支付-2：多张人脸
     **/
    public static final Integer FACE_PAY_MULTI_FACE = 2;

    /**
     * 二维码存储路径
     */
    public static final String QRCODE_PATH = "files.qrcode";
    
    /**  
     * 二维码访问路径
     */
    public static final String QRCODE_IMG_URL = "qrcode.address";
}
