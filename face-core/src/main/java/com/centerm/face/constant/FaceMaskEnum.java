package com.centerm.face.constant;

/**
 * 人脸口罩状态常量
 * @author lidonghang
 * @date 2021/6/25
 */
public enum FaceMaskEnum {

    // 不确定
    UNCERTAIN(-1),
    // 无口罩
    NO(0),
    // 有口罩
    YES(1);

    private Integer code;

    FaceMaskEnum(Integer code){
        this.code = code;
    }

    public Integer getCode(){
        return code;
    }
}
