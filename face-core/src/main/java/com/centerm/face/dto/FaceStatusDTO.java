package com.centerm.face.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 员工人脸信息
 * @author lidonghang
 * @date 2019-11-07 10:02
 */
@ApiModel(value = "人脸状态实体类")
@Data
@Accessors(chain = true)
public class FaceStatusDTO {

    @ApiModelProperty(value = "是否开通人脸（0：否，1：是）")
    private Boolean isOpen;

    @ApiModelProperty(value = "是否上传人脸（0.否，1：是）")
    private Boolean isUpload;
}
