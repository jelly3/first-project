package com.centerm.face.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;

/**
 * 人脸信息输入实体类
 * @author lidonghang
 * @date 2019-11-07 10:02
 */
@ApiModel(value = "人脸信息输入实体类")
@Data
@Accessors(chain = true)
public class FaceInputDTO {

    @ApiModelProperty(value = "工号")
    @NotEmpty
    private String workNumber;

    @ApiModelProperty(value = "电话号码")
    @NotEmpty
    private String phone;
}
