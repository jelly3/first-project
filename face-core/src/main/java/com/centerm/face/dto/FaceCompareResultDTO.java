package com.centerm.face.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 员工人脸比对结果
 * @author lidonghang
 * @date 2019-11-07 10:02
 */
@ApiModel(value = "人脸比对结果实体类")
@Data
@Accessors(chain = true)
public class FaceCompareResultDTO {

   /* @ApiModelProperty(value = "商户编号")
    private Long merchantId;*/

    @ApiModelProperty(value = "人脸ID")
    private String faceId;

    @ApiModelProperty(value = "相似度")
    private Integer similarValue;

}
