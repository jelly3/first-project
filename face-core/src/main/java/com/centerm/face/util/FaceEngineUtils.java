package com.centerm.face.util;

import com.arcsoft.face.EngineConfiguration;
import com.arcsoft.face.FaceEngine;
import com.arcsoft.face.FunctionConfiguration;
import com.arcsoft.face.enums.DetectMode;
import com.arcsoft.face.enums.DetectOrient;
import com.centerm.face.constant.FaceConstant;
import com.centerm.face.factory.FaceEngineFactory;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.miser.system.service.ISysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.*;

/**
 * 人脸识别工具类
 * @author lidonghang
 * @date 2020/10/13
 */
@ConditionalOnExpression("'${app.name}'.equals('face-gateway')")
@Component
@Slf4j
public class FaceEngineUtils {

    @Autowired
    private ISysConfigService sysConfigService;

    private static ExecutorService executorService;

    private static GenericObjectPool faceEngineObjectPool;

    public static FaceEngineUtils faceEngineUtils;

    /**
     * 初始化人脸识别工具类
     **/
    @PostConstruct
    public void init(){

        faceEngineUtils = this;
        //引擎对象池大小
        int threadPoolSize = Integer.parseInt(sysConfigService.selectConfigByKey(FaceConstant.FACE_THREAD_POOL_SIZE));
        //sdk key
        String sdkKey = sysConfigService.selectConfigByKey(FaceConstant.FACE_SDK_KEY);
        //引擎库路径
        String sdkPath = sysConfigService.selectConfigByKey(FaceConstant.FACE_SDK_PATH);
        //引擎库路径
        String activeKey = sysConfigService.selectConfigByKey(FaceConstant.FACE_ACTIVE_KEY);
        //app id
        String appId = sysConfigService.selectConfigByKey(FaceConstant.FACE_APP_ID);

        ThreadFactory namedThreadFactory = new ThreadFactoryBuilder().setNameFormat("thread-face-runner-%d").build();
        executorService = new ThreadPoolExecutor(threadPoolSize,threadPoolSize,0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(),namedThreadFactory);

        //对象池配置
        GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
        poolConfig.setMaxIdle(threadPoolSize);
        poolConfig.setMaxTotal(threadPoolSize);
        poolConfig.setMinIdle(threadPoolSize);
        //先进先出
        poolConfig.setLifo(false);

        //引擎配置
        EngineConfiguration engineConfiguration = new EngineConfiguration();
        //设置检测模式（动态模式和静态模式）目前设置为静态模式
        engineConfiguration.setDetectMode(DetectMode.ASF_DETECT_MODE_IMAGE);
        //设置检测角度优先级（目前设置为0度，即正脸）
        engineConfiguration.setDetectFaceOrientPriority(DetectOrient.ASF_OP_0_ONLY);
        //功能配置
        FunctionConfiguration functionConfiguration = new FunctionConfiguration();
        functionConfiguration.setSupportFaceDetect(true);
        functionConfiguration.setSupportFaceRecognition(true);
        engineConfiguration.setFunctionConfiguration(functionConfiguration);

        //引擎对象池
        faceEngineObjectPool = new GenericObjectPool<>(new FaceEngineFactory(sdkPath, appId, sdkKey, activeKey, engineConfiguration), poolConfig);
        log.info("人脸识别引擎加载成功！");
    }

    public static GenericObjectPool<FaceEngine> getFaceEngineObjectPool() {
        return faceEngineObjectPool;
    }

    public static ExecutorService getExecutorService() {
        return executorService;
    }
}
