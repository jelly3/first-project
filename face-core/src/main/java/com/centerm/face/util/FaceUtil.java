package com.centerm.face.util;

import com.arcsoft.face.FaceEngine;
import com.arcsoft.face.FaceFeature;
import com.arcsoft.face.FaceInfo;
import com.arcsoft.face.enums.ExtractType;
import com.arcsoft.face.toolkit.ImageFactory;
import com.arcsoft.face.toolkit.ImageInfo;
import com.arcsoft.face.toolkit.ImageInfoEx;
import com.centerm.face.constant.FaceMaskEnum;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.miser.common.utils.data.ApiAssert;
import org.miser.file.util.FileTypeUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * 人脸工具类
 * @author lidonghang
 * @date 2020/10/13
 */
@Component
@Slf4j
public class FaceUtil {

    public static List<FaceInfo> detectFaces(MultipartFile file) throws Exception {

        GenericObjectPool<FaceEngine> faceEngineObjectPool = FaceEngineUtils.getFaceEngineObjectPool();
        FaceEngine faceEngine = null;
        try {

            ImageInfo imageInfo = ImageFactory.getRGBData(file.getInputStream());
            ImageInfoEx imageInfoEx = new ImageInfoEx();
            imageInfoEx.setHeight(imageInfo.getHeight());
            imageInfoEx.setWidth(imageInfo.getWidth());
            imageInfoEx.setImageFormat(imageInfo.getImageFormat());
            imageInfoEx.setImageDataPlanes(new byte[][]{imageInfo.getImageData()});
            imageInfoEx.setImageStrides(new int[]{imageInfo.getWidth() * 3});
            //从对象池取出引擎对象
            faceEngine = faceEngineObjectPool.borrowObject();

            //结果列表
            List<FaceInfo> faceInfoList = new ArrayList<>();

            //人脸检测
            faceEngine.detectFaces(imageInfoEx, faceInfoList);

            return faceInfoList;
        } catch (Exception e) {
            log.error("检测人脸异常！{}", e.getMessage());
            throw e;
        } finally {
            if (faceEngine != null) {
                //释放引擎对象
                faceEngineObjectPool.returnObject(faceEngine);
            }
        }

    }

    public static List<FaceFeature> extractFace(MultipartFile file,ExtractType extractType) throws Exception {

        FileTypeUtils.validImage(file.getInputStream());
        List<FaceFeature> result = Lists.newLinkedList();
        GenericObjectPool<FaceEngine> faceEngineObjectPool = FaceEngineUtils.getFaceEngineObjectPool();
        FaceEngine faceEngine = null;
        try {

            ImageInfo imageInfo = ImageFactory.getRGBData(file.getInputStream());

            ApiAssert.isFalse("face.image.error", imageInfo==null);
            //从对象池取出引擎对象
            faceEngine = faceEngineObjectPool.borrowObject();

            //结果列表
            List<FaceInfo> faceInfoList = new ArrayList<>();

            //人脸检测
            faceEngine.detectFaces(imageInfo.getImageData(), imageInfo.getWidth(), imageInfo.getHeight(), imageInfo.getImageFormat(), faceInfoList);

            log.info("提取到【{}】个人脸信息！",faceInfoList.size());

            ApiAssert.isFalse("no.face.data", faceInfoList.isEmpty());

            if (!faceInfoList.isEmpty()) {

                for(FaceInfo faceInfo : faceInfoList){
                    FaceFeature faceFeature = new FaceFeature();
                    //提取人脸特征
                    faceEngine.extractFaceFeature(imageInfo.getImageData(),
                            imageInfo.getWidth(), imageInfo.getHeight(), imageInfo.getImageFormat(),
                            faceInfo, extractType,FaceMaskEnum.NO.getCode(), faceFeature);
                    result.add(faceFeature);
                }

                return result;
            }

        } catch (Exception e) {
            log.error("检测人脸异常！{}", e.getMessage());
            throw e;
        } finally {
            if (faceEngine != null) {
                //释放引擎对象
                faceEngineObjectPool.returnObject(faceEngine);
            }

        }

        return result;
    }

}
