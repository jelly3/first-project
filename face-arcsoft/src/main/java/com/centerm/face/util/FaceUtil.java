package com.centerm.face.util;

import cn.hutool.core.collection.CollectionUtil;
import com.arcsoft.face.FaceEngine;
import com.arcsoft.face.FaceFeature;
import com.arcsoft.face.FaceInfo;
import com.arcsoft.face.FaceSimilar;
import com.arcsoft.face.enums.CompareModel;
import com.arcsoft.face.enums.ErrorInfo;
import com.arcsoft.face.toolkit.ImageFactory;
import com.arcsoft.face.toolkit.ImageInfo;
import com.google.common.collect.Lists;
import org.miser.common.utils.data.ApiAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Component
public class FaceUtil {

    @Autowired
    private FaceEngine faceEngine;

    private static FaceEngine faceEngineStatic;

    @PostConstruct
    public void init() {
        faceEngineStatic = faceEngine;
    }

    private FaceUtil() {

    }

    /**
     * 特征值提取
     * @param multipartFile
     * @return
     */
    public static FaceFeature extractFaceFeature(MultipartFile multipartFile) throws IOException {
        ApiAssert.notNull("file.content.error",multipartFile.getInputStream());
        ImageInfo imageInfo = ImageFactory.getRGBData(multipartFile.getInputStream());
        List<FaceInfo> faceInfoList = detectFace(imageInfo);
        ApiAssert.isFalse("no.face.data", CollectionUtil.isEmpty(faceInfoList));
        ApiAssert.isFalse("multi.face.detected",  faceInfoList.size() > 1);
        FaceFeature faceFeature = extractFaceFeature(faceInfoList.get(0), imageInfo);
        ApiAssert.notNull("face.feature.error",  faceFeature);
        return faceFeature;
    }

    public static FaceSimilar compareFaceFeature(FaceFeature target, FaceFeature source, CompareModel compareModel) {
        FaceSimilar faceSimilar = new FaceSimilar();
        int code = faceEngineStatic.compareFaceFeature(target, source, compareModel, faceSimilar);
        checkEngineResult(code, ErrorInfo.MOK.getValue(), "人脸特征对比失败");
        return faceSimilar;
    }

    /**
     * 人脸检测
     * @return
     */
    private static List<FaceInfo> detectFace(ImageInfo imageInfo) {
       if (imageInfo == null) {
           return null;
       }
       List<FaceInfo> faceInfoList = Lists.newArrayList();
       int code = faceEngineStatic.detectFaces(imageInfo.getImageData(), imageInfo.getWidth(), imageInfo.getHeight(), imageInfo.getImageFormat(), faceInfoList);
       checkEngineResult(code, ErrorInfo.MOK.getValue(), "人脸检测失败");
       return faceInfoList;
    }

    /**
     * 特征提取
     * @param faceInfo
     * @param imageInfo
     * @return
     */
    private static FaceFeature extractFaceFeature(FaceInfo faceInfo, ImageInfo imageInfo) {
       if (null == faceInfo || null == imageInfo) {
           return null;
       }
       FaceFeature faceFeature = new FaceFeature();
       int code = faceEngineStatic.extractFaceFeature(imageInfo.getImageData(), imageInfo.getWidth(), imageInfo.getHeight(), imageInfo.getImageFormat(), faceInfo, faceFeature);
       checkEngineResult(code, ErrorInfo.MOK.getValue(), "人脸特征提取失败");
       return faceFeature;
    }

    /**
     * 错误检测
     * @param errorCode
     * @param sourceCode
     * @param errMsg
     */
    private static void checkEngineResult(int errorCode, int sourceCode, String errMsg) {
        if (errorCode != sourceCode) {
            throw new RuntimeException(errMsg);
        }
    }
}
