package com.centerm.face.util;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import java.util.concurrent.*;

public class ThreadUtil {

    private ThreadUtil() {

    }

    // 获取当前机器的核数
    public static final int cpuNum = Runtime.getRuntime().availableProcessors();

    // 人脸识别线程
    private static final ExecutorService faceService  =
            new ThreadPoolExecutor(cpuNum, 2*cpuNum,
                    0L, TimeUnit.MILLISECONDS,
                    new LinkedBlockingQueue<>(),
                    new ThreadFactoryBuilder().
                            setNameFormat("thread-face-runner").build());


    public static ExecutorService getFaceService() {
        return faceService;
    }
}
