package com.centerm.face.util;

import com.arcsoft.face.toolkit.ImageInfo;
import com.arcsoft.face.toolkit.ImageInfoEx;
import lombok.Data;

import java.io.File;
import java.io.InputStream;

import static com.arcsoft.face.toolkit.ImageFactory.getRGBData;

public class ArcfaceUtils {

    private ArcfaceUtils() {

    }

    /**
     * 处理 File 的图片流
     * @param img
     * @return
     */
    public static ImageInfoMeta packImageInfoEx(File img){
        ImageInfo imageInfo = getRGBData(img);
        return packImageInfoMeta(imageInfo);
    }
    /**
     * 处理 byte[] 的图片流
     * @param img
     * @return
     */
    public static ImageInfoMeta packImageInfoMeta(byte[] img){
        ImageInfo imageInfo = getRGBData(img);
        return packImageInfoMeta(imageInfo);
    }

    /**
     * 处理 InpuStream 的图片流
     * @param img
     * @return
     */
    public static ImageInfoMeta packImageInfoMeta(InputStream img){
        ImageInfo imageInfo = getRGBData(img);
        return packImageInfoMeta(imageInfo);
    }


    /**
     * 打包生成 ImageInfoMeta
     * @param imageInfo
     * @return
     */
    private static ImageInfoMeta packImageInfoMeta(ImageInfo imageInfo){
        ImageInfoMeta imageInfoMeta = new ImageInfoMeta(imageInfo);
        return imageInfoMeta;
    }

    /**
     * 对imageInfo 和 imageInfoEx 的打包对象
     */
    @Data
    public static class ImageInfoMeta{
        private ImageInfo imageInfo;
        private ImageInfoEx imageInfoEx;

        public ImageInfoMeta(ImageInfo imageInfo) {
            this.imageInfo = imageInfo;
            imageInfoEx = new ImageInfoEx();
            imageInfoEx.setHeight(imageInfo.getHeight());
            imageInfoEx.setWidth(imageInfo.getWidth());
            imageInfoEx.setImageFormat(imageInfo.getImageFormat());
            imageInfoEx.setImageDataPlanes(new byte[][]{imageInfo.getImageData()});
            imageInfoEx.setImageStrides(new int[]{imageInfo.getWidth() * 3});
        }
    }
}
