package com.centerm.face.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@ApiModel(value = "人脸比对实体类")
@Data
@Accessors(chain = true)
public class FaceCompareDTO {

    @ApiModelProperty(value = "人脸编号")
    private String faceId;

    @ApiModelProperty(value = "人脸特征")
    @JsonIgnore
    private byte[] faceFeature;
}
