package com.centerm.face.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@ApiModel(value = "人脸比对结果实体类")
@Data
@Accessors(chain = true)
public class FaceCompareResultDTO {

    @ApiModelProperty(value = "人脸ID")
    private String faceId;

    @ApiModelProperty(value = "相似度")
    private Integer similarValue;
}
