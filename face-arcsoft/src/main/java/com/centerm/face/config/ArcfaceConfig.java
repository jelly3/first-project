package com.centerm.face.config;

import com.arcsoft.face.EngineConfiguration;
import com.arcsoft.face.FaceEngine;
import com.arcsoft.face.FunctionConfiguration;
import com.arcsoft.face.enums.DetectMode;
import com.arcsoft.face.enums.DetectOrient;
import com.arcsoft.face.enums.ErrorInfo;
import com.centerm.face.constant.FaceConstant;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.miser.system.service.ISysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Setter
@Configuration
@Slf4j
public class ArcfaceConfig {

    @Autowired
    private ISysConfigService sysConfigService;

    @Bean
    public FaceEngine faceEngine(){
        // sdkKey
        String sdkKey = sysConfigService.selectConfigByKey(FaceConstant.FACE_SDK_KEY);
        // 引擎库路径
        String sdkPath = sysConfigService.selectConfigByKey(FaceConstant.FACE_SDK_PATH);
        // appId
        String appId = sysConfigService.selectConfigByKey(FaceConstant.FACE_APP_ID);
        FaceEngine faceEngine = new FaceEngine(sdkPath);
        log.info("faceEngine,{}",faceEngine);
        int errorCode = faceEngine.activeOnline(appId, sdkKey);
        log.info("errorCode000,{}",errorCode);
        if (errorCode != ErrorInfo.MOK.getValue() && errorCode != ErrorInfo.MERR_ASF_ALREADY_ACTIVATED.getValue()) {
            throw new RuntimeException("引擎注册失败");
        }
        EngineConfiguration engineConfiguration = getFaceEngineConfiguration();
        // 初始化引擎
        errorCode = faceEngine.init(engineConfiguration);
        log.info("errorCode111,{}",errorCode);
        if (errorCode != ErrorInfo.MOK.getValue()) {
            throw new RuntimeException("初始化引擎失败");
        }
        log.info("引擎激活成功！");
        return faceEngine;
    }

    /**
     * 初始化引擎配置
     * @return
     */
    private EngineConfiguration getFaceEngineConfiguration() {
        EngineConfiguration engineConfiguration = new EngineConfiguration();
        engineConfiguration.setDetectMode(DetectMode.ASF_DETECT_MODE_IMAGE);
        // 配置人脸角度 全角度 ASF_OP_ALL_OUT 不够准确且检测速度慢
        engineConfiguration.setDetectFaceOrientPriority(DetectOrient.ASF_OP_0_ONLY);
        // 设置识别的最小人脸比
        engineConfiguration.setDetectFaceMaxNum(10);
        engineConfiguration.setDetectFaceScaleVal(16);
        // 功能配置
        initFuncConfiguration(engineConfiguration);
        return engineConfiguration;
    }

    /**
     * 功能配置
     * @param engineConfiguration
     */
    private void initFuncConfiguration(EngineConfiguration engineConfiguration){
        FunctionConfiguration functionConfiguration = new FunctionConfiguration();
        // 是否支持人脸检测
        functionConfiguration.setSupportFaceDetect(true);
        // 是否支持人脸识别
        functionConfiguration.setSupportFaceRecognition(true);
        engineConfiguration.setFunctionConfiguration(functionConfiguration);
    }
}
