package com.centerm.face.constant;

public class FaceConstant {

    /**
     * 人脸引擎路径
     */
    public static final String FACE_SDK_PATH ="face.arcsoft.sdk.path";

    /**
     * 人脸引擎appid
     */
    public static final String FACE_APP_ID ="face.app.id";

    /**
     * 人脸引擎sdkkey
     */
    public static final String FACE_SDK_KEY = "face.sdk.key";

    /**
     * 人脸图片路径
     */
    public static final String FILES_MEMBER_FACE_PATH = "files.member.face.path";

    /**
     * 人脸比对通过值
     */
    public static final String FACE_PASS_VALUE = "face.pass.value";
}
