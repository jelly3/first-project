package com.centerm;

import com.github.pagehelper.autoconfigure.PageHelperAutoConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 *
 * @author Barry
 */
@SpringBootApplication(scanBasePackages={"org.miser","com.centerm"},exclude = {DataSourceAutoConfiguration.class, PageHelperAutoConfiguration.class})
@MapperScan(basePackages = {"org.miser.**.mapper","com.centerm.**.mapper"})
public class AdminApplication {
    public static void main(String[] args) {
//         System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(AdminApplication.class, args);
        System.out.println("______ ___  _____  _____        ___ _________  ________ _   _ \n" +
                "|  ___/ _ \\/  __ \\|  ___|      / _ \\|  _  \\  \\/  |_   _| \\ | |\n" +
                "| |_ / /_\\ \\ /  \\/| |__ ______/ /_\\ \\ | | | .  . | | | |  \\| |\n" +
                "|  _||  _  | |    |  __|______|  _  | | | | |\\/| | | | | . ` |\n" +
                "| |  | | | | \\__/\\| |___      | | | | |/ /| |  | |_| |_| |\\  |\n" +
                "\\_|  \\_| |_/\\____/\\____/      \\_| |_/___/ \\_|  |_/\\___/\\_| \\_/\n");
    }
}