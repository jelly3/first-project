package com.centerm.web.controller.monitor;


import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.miser.common.annotation.Log;
import org.miser.common.enums.BusinessType;
import org.miser.common.enums.OnlineStatus;
import org.miser.common.exception.BusinessException;
import org.miser.common.core.page.TableDataInfo;
import org.miser.framework.shiro.session.OnlineSession;
import org.miser.framework.shiro.session.OnlineSessionDAO;
import org.miser.framework.util.ShiroUtils;
import org.miser.common.core.controller.BaseController;
import org.miser.system.domain.SysUserOnline;
import org.miser.system.service.impl.SysUserOnlineServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 在线用户监控
 *
 * @author nff
 */
@Controller
@RequestMapping("/monitor/online")
public class UserOnlineController extends BaseController {
    private String prefix = "monitor/online";

    @Autowired
    private SysUserOnlineServiceImpl userOnlineService;

    @Autowired
    private OnlineSessionDAO onlineSessionDAO;

    @RequiresPermissions("monitor:online:view")
    @GetMapping()
    public String online() {
        return prefix + "/online";
    }

    @RequiresPermissions("monitor:online:list")
    @GetMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysUserOnline userOnline) {
        startPage();
        List<SysUserOnline> list = userOnlineService.selectUserOnlineList(userOnline);
        return getDataTable(list);
    }

    @RequiresPermissions("monitor:online:batchForceLogout")
    @Log(title = "在线用户", businessType = BusinessType.FORCE)
    @PostMapping("/batchForceLogout")
    @ResponseBody
    public void batchForceLogout(@RequestParam("ids[]") String[] ids) {
        for (String sessionId : ids) {
            forceLogout(sessionId);
        }
    }

    @RequiresPermissions("monitor:online:forceLogout")
    @Log(title = "在线用户", businessType = BusinessType.FORCE)
    @PostMapping("/forceLogout")
    @ResponseBody
    public void forceLogout(String sessionId) {
        SysUserOnline online = userOnlineService.selectOnlineById(sessionId);
        if (online == null) {
            throw new BusinessException("user.has.offline");
        }
        OnlineSession onlineSession = (OnlineSession) onlineSessionDAO.readSession(online.getSessionId());
        if (onlineSession == null) {
            throw new BusinessException("user.has.offline");
        }
        if (sessionId.equals(ShiroUtils.getSessionId())) {
            throw new BusinessException("user.self.not.force");
        }
        onlineSession.setStatus(OnlineStatus.off_line);
        online.setStatus(OnlineStatus.off_line);
        userOnlineService.saveOnline(online);
    }
}
