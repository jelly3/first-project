package com.centerm.web.controller.client;

import com.centerm.face.client.domain.ClientInfo;
import com.centerm.face.client.service.IClientInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.miser.common.annotation.Log;
import org.miser.common.core.controller.BaseController;
import org.miser.common.core.page.TableDataInfo;
import org.miser.common.enums.BusinessType;
import org.miser.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 客户端Controller
 * 
 * @author lidonghang
 * @date 2021-06-25
 */
@Api(tags = "客户端模块")
@Controller
@RequestMapping("/client/info")
public class ClientInfoController extends BaseController {
    private String prefix = "client/info";

    @Autowired
    private IClientInfoService clientInfoService;

    @RequiresPermissions("client:info:view")
    @GetMapping()
    public String info()
    {
        return prefix + "/info";
    }


    @ApiOperation(value = " 查询客户端列表")
    @RequiresPermissions("client:info:list")
    @GetMapping("/list")
    @ResponseBody
    public TableDataInfo<ClientInfo> list(ClientInfo clientInfo) {
        startPage();
        List<ClientInfo> list = clientInfoService.selectClientInfoList(clientInfo);
        return getDataTable(list);
    }


    @ApiOperation(value = "导出客户端列表")
    @RequiresPermissions("client:info:export")
    @PostMapping("/export")
    @ResponseBody
    public String export(ClientInfo clientInfo) {
        List<ClientInfo> list = clientInfoService.selectClientInfoList(clientInfo);
        ExcelUtil<ClientInfo> util = new ExcelUtil<ClientInfo>(ClientInfo.class);
        return util.exportExcel(list, "info");
    }


    @ApiOperation(value = "新增客户端页面")
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }


    @ApiOperation(value = "新增保存客户端")
    @RequiresPermissions("client:info:add")
    @Log(title = "客户端", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public Boolean addSave(ClientInfo clientInfo) {
        return rowResult(clientInfoService.save(clientInfo));
    }


    @ApiOperation(value = "修改客户端页面")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        ClientInfo clientInfo = clientInfoService.selectClientInfoById(id);
        mmap.put("clientInfo", clientInfo);
        return prefix + "/edit";
    }


    @ApiOperation(value = "修改保存客户端")
    @RequiresPermissions("client:info:edit")
    @Log(title = "客户端", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public Boolean editSave(ClientInfo clientInfo)
    {
        return rowResult(clientInfoService.updateById(clientInfo));
    }


    @ApiOperation(value = "删除客户端")
    @RequiresPermissions("client:info:remove")
    @Log(title = "客户端", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public Boolean remove(String ids) {
        return clientInfoService.removeByIds(Arrays.asList(ids.split(",")));
    }
}
