package com.centerm.web.controller.person;

import com.centerm.face.person.domain.PersonInfo;
import com.centerm.face.person.service.IPersonInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.miser.common.core.page.TableDataInfo;
import org.miser.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.miser.common.annotation.Log;
import org.miser.common.enums.BusinessType;
import org.miser.common.core.controller.BaseController;
import java.util.Arrays;
import java.util.List;

/**
 * 人员Controller
 * 
 * @author lidonghang
 * @date 2021-06-25
 */
@Api(tags = "人员模块")
@RestController
@RequestMapping("/person/info")
public class PersonInfoController extends BaseController {
    private String prefix = "person/info";

    @Autowired
    private IPersonInfoService personInfoService;

    @RequiresPermissions("person:info:view")
    @GetMapping()
    public String info()
    {
        return prefix + "/info";
    }


    @ApiOperation(value = " 查询人员列表")
    @RequiresPermissions("person:info:list")
    @GetMapping("/list")
    @ResponseBody
    public TableDataInfo<PersonInfo> list(PersonInfo personInfo) {
        startPage();
        List<PersonInfo> list = personInfoService.selectPersonInfoList(personInfo);
        return getDataTable(list);
    }


    @ApiOperation(value = "导出人员列表")
    @RequiresPermissions("person:info:export")
    @PostMapping("/export")
    @ResponseBody
    public String export(PersonInfo personInfo) {
        List<PersonInfo> list = personInfoService.selectPersonInfoList(personInfo);
        ExcelUtil<PersonInfo> util = new ExcelUtil<PersonInfo>(PersonInfo.class);
        return util.exportExcel(list, "info");
    }


    @ApiOperation(value = "新增人员页面")
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }


    @ApiOperation(value = "新增保存人员")
    @RequiresPermissions("person:info:add")
    @Log(title = "人员", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public Boolean addSave(PersonInfo personInfo) {
        return rowResult(personInfoService.save(personInfo));
    }


    @ApiOperation(value = "修改人员页面")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        PersonInfo personInfo = personInfoService.selectPersonInfoById(id);
        mmap.put("personInfo", personInfo);
        return prefix + "/edit";
    }


    @ApiOperation(value = "修改保存人员")
    @RequiresPermissions("person:info:edit")
    @Log(title = "人员", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public Boolean editSave(PersonInfo personInfo)
    {
        return rowResult(personInfoService.updateById(personInfo));
    }


    @ApiOperation(value = "删除人员")
    @RequiresPermissions("person:info:remove")
    @Log(title = "人员", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public Boolean remove(String ids) {
        return personInfoService.removeByIds(Arrays.asList(ids.split(",")));
    }
}
