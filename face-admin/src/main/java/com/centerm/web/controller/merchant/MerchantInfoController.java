package com.centerm.web.controller.merchant;

import com.centerm.face.person.domain.MerchantInfo;
import com.centerm.face.person.service.IMerchantInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.miser.common.annotation.Log;
import org.miser.common.core.controller.BaseController;
import org.miser.common.core.page.TableDataInfo;
import org.miser.common.enums.BusinessType;
import org.miser.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 商户Controller
 * 
 * @author lidonghang
 * @date 2021-06-25
 */
@Api(tags = "商户模块")
@Controller
@RequestMapping("/merchant/info")
public class MerchantInfoController extends BaseController {
    private String prefix = "merchant/info";

    @Autowired
    private IMerchantInfoService merchantInfoService;

    @RequiresPermissions("merchant:info:view")
    @GetMapping()
    public String info()
    {
        return prefix + "/info";
    }


    @ApiOperation(value = " 查询商户列表")
    @RequiresPermissions("merchant:info:list")
    @GetMapping("/list")
    @ResponseBody
    public TableDataInfo<MerchantInfo> list(MerchantInfo merchantInfo) {
        startPage();
        List<MerchantInfo> list = merchantInfoService.selectMerchantInfoList(merchantInfo);
        return getDataTable(list);
    }


    @ApiOperation(value = "导出商户列表")
    @RequiresPermissions("merchant:info:export")
    @PostMapping("/export")
    @ResponseBody
    public String export(MerchantInfo merchantInfo) {
        List<MerchantInfo> list = merchantInfoService.selectMerchantInfoList(merchantInfo);
        ExcelUtil<MerchantInfo> util = new ExcelUtil<MerchantInfo>(MerchantInfo.class);
        return util.exportExcel(list, "info");
    }


    @ApiOperation(value = "新增商户页面")
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }


    @ApiOperation(value = "新增保存商户")
    @RequiresPermissions("merchant:info:add")
    @Log(title = "商户", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public Boolean addSave(MerchantInfo merchantInfo) {
        return rowResult(merchantInfoService.save(merchantInfo));
    }


    @ApiOperation(value = "修改商户页面")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        MerchantInfo merchantInfo = merchantInfoService.selectMerchantInfoById(id);
        mmap.put("merchantInfo", merchantInfo);
        return prefix + "/edit";
    }


    @ApiOperation(value = "修改保存商户")
    @RequiresPermissions("merchant:info:edit")
    @Log(title = "商户", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public Boolean editSave(MerchantInfo merchantInfo)
    {
        return rowResult(merchantInfoService.updateById(merchantInfo));
    }


    @ApiOperation(value = "删除商户")
    @RequiresPermissions("merchant:info:remove")
    @Log(title = "商户", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public Boolean remove(String ids) {
        return merchantInfoService.removeByIds(Arrays.asList(ids.split(",")));
    }
}
