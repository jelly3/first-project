package com.centerm.web.controller.system;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.miser.common.annotation.Log;
import org.miser.common.enums.BusinessType;
import org.miser.common.core.page.TableDataInfo;
import org.miser.system.domain.ConfigList;
import org.miser.system.domain.SysConfig;
import org.miser.system.service.IConfigListService;
import org.miser.system.service.ISysConfigService;
import org.miser.common.utils.poi.ExcelUtil;
import org.miser.framework.util.ShiroUtils;
import org.miser.common.core.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 参数配置 信息操作处理
 *
 * @author nff
 */
@Controller
@RequestMapping("/system/config")
public class ConfigController extends BaseController {
    private String prefix = "system/config";
    private static final Integer TYPE_RADIO = 2;

    @Autowired
    private ISysConfigService configService;

    @Autowired
    private IConfigListService configListService;

    @RequiresPermissions("system:config:view")
    @GetMapping()
    public String config() {
        return prefix + "/config";
    }

    /**
     * 查询参数配置列表
     */
    @RequiresPermissions("system:config:list")
    @GetMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysConfig config) {
        startPage();
        List<SysConfig> list = configService.selectConfigList(config);
        return getDataTable(list);
    }

    @Log(title = "参数管理", businessType = BusinessType.EXPORT)
    @RequiresPermissions("system:config:export")
    @PostMapping("/export")
    @ResponseBody
    public String export(SysConfig config) {
        List<SysConfig> list = configService.selectConfigList(config);
        ExcelUtil<SysConfig> util = new ExcelUtil<SysConfig>(SysConfig.class);
        return util.exportExcel(list, "config");
    }

    /**
     * 新增参数配置
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存参数配置
     */
    @RequiresPermissions("system:config:add")
    @Log(title = "参数管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public Integer addSave(SysConfig config) {
        config.setCreateBy(ShiroUtils.getLoginName());
        return configService.insertConfig(config);
    }

    /**
     * 修改参数配置
     */
    @GetMapping("/edit/{configId}")
    public String edit(@PathVariable("configId") Long configId, ModelMap mmap) {
        SysConfig config = configService.selectConfigById(configId);
        mmap.put("config", config);
        if (config != null && TYPE_RADIO.equals(config.getValueType())) {
            mmap.put("configList", configListService.list(new QueryWrapper<ConfigList>().lambda().eq(ConfigList::getConfigId, config.getConfigId())));
        }
        return prefix + "/edit";
    }

    /**
     * 修改保存参数配置
     */
    @RequiresPermissions("system:config:edit")
    @Log(title = "参数管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public Integer editSave(SysConfig config) {
        config.setUpdateBy(ShiroUtils.getLoginName());
        return configService.updateConfig(config);
    }

    /**
     * 删除参数配置
     */
    @RequiresPermissions("system:config:remove")
    @Log(title = "参数管理", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public Integer remove(String ids) {
        return configService.deleteConfigByIds(ids);
    }

    /**
     * 校验参数键名
     */
    @PostMapping("/checkConfigKeyUnique")
    @ResponseBody
    public String checkConfigKeyUnique(SysConfig config) {
        return configService.checkConfigKeyUnique(config);
    }
}
