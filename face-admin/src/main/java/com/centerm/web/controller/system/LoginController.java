package com.centerm.web.controller.system;


import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.miser.common.utils.data.ApiAssert;
import org.miser.common.utils.data.CommonUtils;
import org.miser.common.utils.servlet.ServletUtils;
import org.miser.common.core.controller.BaseController;
import org.miser.framework.util.ShiroUtils;
import org.miser.system.domain.SysUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 登录验证
 *
 * @author nff
 */
@Controller
public class LoginController extends BaseController {
    private static final String ADMIN ="admin";
    @GetMapping("/login")
    public String login(HttpServletRequest request, HttpServletResponse response) {
        // 如果是Ajax请求，返回Json字符串。
        if (ServletUtils.isAjaxRequest(request)) {
            return ServletUtils.renderString(response, "{\"code\":\"1\",\"msg\":\"未登录或登录超时。请重新登录\"}");
        }
        return "login";
    }

    @PostMapping("/login")
    @ResponseBody
    public void ajaxLogin(String username, String password, Boolean rememberMe) {
        UsernamePasswordToken token = new UsernamePasswordToken(username, password, rememberMe);
        Subject subject = SecurityUtils.getSubject();
        subject.login(token);
        SysUser user = ShiroUtils.getSysUser();
        //非系统用户不能登陆
        if(!ADMIN.equals(user.getLoginName())&& CommonUtils.isEmpty(user.getRoles())){
            ApiAssert.failure("login.role.null");
        }
    }

    @GetMapping("/unauth")
    public String unauth() {
        return "/error/unauth";
    }
}
