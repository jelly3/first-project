package com.centerm.web.controller.terminal;

import com.centerm.face.client.domain.TerminalInfo;
import com.centerm.face.client.service.ITerminalInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.miser.common.annotation.Log;
import org.miser.common.core.controller.BaseController;
import org.miser.common.core.page.TableDataInfo;
import org.miser.common.enums.BusinessType;
import org.miser.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 终端Controller
 * 
 * @author lidonghang
 * @date 2021-06-25
 */
@Api(tags = "终端模块")
@Controller
@RequestMapping("/terminal/info")
public class TerminalInfoController extends BaseController {
    private String prefix = "terminal/info";

    @Autowired
    private ITerminalInfoService terminalInfoService;

    @RequiresPermissions("terminal:info:view")
    @GetMapping()
    public String info()
    {
        return prefix + "/info";
    }


    @ApiOperation(value = " 查询终端列表")
    @RequiresPermissions("terminal:info:list")
    @GetMapping("/list")
    @ResponseBody
    public TableDataInfo<TerminalInfo> list(TerminalInfo terminalInfo) {
        startPage();
        List<TerminalInfo> list = terminalInfoService.selectTerminalInfoList(terminalInfo);
        return getDataTable(list);
    }


    @ApiOperation(value = "导出终端列表")
    @RequiresPermissions("terminal:info:export")
    @PostMapping("/export")
    @ResponseBody
    public String export(TerminalInfo terminalInfo) {
        List<TerminalInfo> list = terminalInfoService.selectTerminalInfoList(terminalInfo);
        ExcelUtil<TerminalInfo> util = new ExcelUtil<TerminalInfo>(TerminalInfo.class);
        return util.exportExcel(list, "info");
    }


    @ApiOperation(value = "新增终端页面")
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }


    @ApiOperation(value = "新增保存终端")
    @RequiresPermissions("terminal:info:add")
    @Log(title = "终端", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public Boolean addSave(TerminalInfo terminalInfo) {
        return rowResult(terminalInfoService.save(terminalInfo));
    }


    @ApiOperation(value = "修改终端页面")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        TerminalInfo terminalInfo = terminalInfoService.selectTerminalInfoById(id);
        mmap.put("terminalInfo", terminalInfo);
        return prefix + "/edit";
    }


    @ApiOperation(value = "修改保存终端")
    @RequiresPermissions("terminal:info:edit")
    @Log(title = "终端", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public Boolean editSave(TerminalInfo terminalInfo)
    {
        return rowResult(terminalInfoService.updateById(terminalInfo));
    }


    @ApiOperation(value = "删除终端")
    @RequiresPermissions("terminal:info:remove")
    @Log(title = "终端", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public Boolean remove(String ids) {
        return terminalInfoService.removeByIds(Arrays.asList(ids.split(",")));
    }
}
