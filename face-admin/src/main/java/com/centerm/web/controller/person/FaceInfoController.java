package com.centerm.web.controller.person;

import com.centerm.face.person.domain.FaceInfo;
import com.centerm.face.person.service.IFaceInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.miser.common.annotation.Log;
import org.miser.common.core.controller.BaseController;
import org.miser.common.core.page.TableDataInfo;
import org.miser.common.enums.BusinessType;
import org.miser.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 人脸数据Controller
 * 
 * @author lidonghang
 * @date 2021-06-28
 */
@Api(tags = "人脸数据模块")
@Controller
@RequestMapping("/person/face")
public class FaceInfoController extends BaseController {
    private String prefix = "person/face";

    @Autowired
    private IFaceInfoService infoService;

    @RequiresPermissions("face:info:view")
    @GetMapping()
    public String info()
    {
        return prefix + "/info";
    }


    @ApiOperation(value = " 查询人脸数据列表")
    @RequiresPermissions("face:info:list")
    @GetMapping("/list")
    @ResponseBody
    public TableDataInfo<FaceInfo> list(FaceInfo info) {
        startPage();
        List<FaceInfo> list = infoService.selectInfoList(info);
        return getDataTable(list);
    }


    @ApiOperation(value = "导出人脸数据列表")
    @RequiresPermissions("face:info:export")
    @PostMapping("/export")
    @ResponseBody
    public String export(FaceInfo info) {
        List<FaceInfo> list = infoService.selectInfoList(info);
        ExcelUtil<FaceInfo> util = new ExcelUtil<FaceInfo>(FaceInfo.class);
        return util.exportExcel(list, "info");
    }


    @ApiOperation(value = "新增人脸数据页面")
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }


    @ApiOperation(value = "新增保存人脸数据")
    @RequiresPermissions("face:info:add")
    @Log(title = "人脸数据", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public Boolean addSave(FaceInfo info) {
        return rowResult(infoService.save(info));
    }


    @ApiOperation(value = "修改人脸数据页面")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        FaceInfo info = infoService.selectInfoById(id);
        mmap.put("info", info);
        return prefix + "/edit";
    }


    @ApiOperation(value = "修改保存人脸数据")
    @RequiresPermissions("face:info:edit")
    @Log(title = "人脸数据", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public Boolean editSave(FaceInfo info)
    {
        return rowResult(infoService.updateById(info));
    }


    @ApiOperation(value = "删除人脸数据")
    @RequiresPermissions("face:info:remove")
    @Log(title = "人脸数据", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public Boolean remove(String ids) {
        return infoService.removeByIds(Arrays.asList(ids.split(",")));
    }
}
