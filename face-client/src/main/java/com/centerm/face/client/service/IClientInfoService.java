package com.centerm.face.client.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.centerm.face.client.domain.ClientInfo;
import java.util.List;

/**
 * 客户端Service接口
 * 
 * @author lidonghang
 * @date 2021-06-25
 */
public interface IClientInfoService extends IService<ClientInfo>{
    /**
     * 查询客户端
     * 
     * @param id 客户端ID
     * @return 客户端
     */
    ClientInfo selectClientInfoById(Long id);

    /**
     * 查询客户端列表
     * 
     * @param clientInfo 客户端
     * @return 客户端集合
     */
    List<ClientInfo> selectClientInfoList(ClientInfo clientInfo);

    /**
     * 根据clientId查询客户端信息
     * @param clientId 客户端编号
     * @author lidonghang
     * @date 2021/6/28 09:34
     * @return 客户端信息
     **/
    ClientInfo getByClientId(String clientId);
}
