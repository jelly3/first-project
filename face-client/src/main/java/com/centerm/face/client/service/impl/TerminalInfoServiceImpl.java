package com.centerm.face.client.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import org.miser.common.utils.data.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.centerm.face.client.mapper.TerminalInfoMapper;
import com.centerm.face.client.domain.TerminalInfo;
import com.centerm.face.client.service.ITerminalInfoService;
import  org.miser.common.core.support.Convert;
import java.util.List;

/**
 * 终端Service业务层处理
 * 
 * @author lidonghang
 * @date 2021-06-25
 */
@Service
public class TerminalInfoServiceImpl extends ServiceImpl<TerminalInfoMapper, TerminalInfo>  implements ITerminalInfoService {
 

    /**
     * 查询终端
     * 
     * @param id 终端ID
     * @return 终端
     */
    @Override
    public TerminalInfo selectTerminalInfoById(Long id){
        return baseMapper.selectTerminalInfoById(id);
    }

    /**
     * 查询终端列表
     * 
     * @param terminalInfo 终端
     * @return 终端
     */
    @Override
    public List<TerminalInfo> selectTerminalInfoList(TerminalInfo terminalInfo){
        return baseMapper.selectTerminalInfoList(terminalInfo);
    }
}
