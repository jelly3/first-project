package com.centerm.face.client.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;

import org.miser.common.constant.Constants;
import org.miser.common.utils.data.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.centerm.face.client.mapper.ClientInfoMapper;
import com.centerm.face.client.domain.ClientInfo;
import com.centerm.face.client.service.IClientInfoService;
import  org.miser.common.core.support.Convert;
import java.util.List;

/**
 * 客户端Service业务层处理
 * 
 * @author lidonghang
 * @date 2021-06-25
 */
@Service
public class ClientInfoServiceImpl extends ServiceImpl<ClientInfoMapper, ClientInfo>  implements IClientInfoService {

    @Override
    public ClientInfo selectClientInfoById(Long id){
        return baseMapper.selectClientInfoById(id);
    }

    @Override
    public List<ClientInfo> selectClientInfoList(ClientInfo clientInfo){
        return baseMapper.selectClientInfoList(clientInfo);
    }

    @Override
    public ClientInfo getByClientId(String clientId) {

        return getOne(new QueryWrapper<ClientInfo>()
                .lambda()
                .eq(ClientInfo::getStatus, Constants.YES)
                .eq(ClientInfo::getClientId,clientId));
    }
}
