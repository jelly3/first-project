package com.centerm.face.client.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import org.miser.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import java.util.Date;
import org.miser.common.core.domain.BaseEntity;

/**
 * 客户端对象 face_client_info
 *
 * @author lidonghang
 * @date 2021-06-25
 */
@ApiModel(value = "客户端对象")
@Data
@ToString
@TableName("face_client_info")
@Accessors(chain = true)
public class ClientInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;


    /** 自增主键 */
    @ApiModelProperty(value = "${comment}")
    private Long id;

    /** 所属商户 */
    @Excel(name = "所属商户")
    @ApiModelProperty(value = "所属商户")
    private Long merchantId;

    /** 客户端编号 */
    @Excel(name = "客户端编号")
    @ApiModelProperty(value = "客户端编号")
    private String clientId;

    /** 客户端名称 */
    @Excel(name = "客户端名称")
    @ApiModelProperty(value = "客户端名称")
    private String clientName;

    /** 客户端密钥 */
    @Excel(name = "客户端密钥")
    @ApiModelProperty(value = "客户端密钥")
    private String clientSecret;

    /** 状态（0：无效，1：有效） */
    @Excel(name = "状态", readConverterExp = "0=：无效，1：有效")
    @ApiModelProperty(value = "状态")
    private Integer status;





}
