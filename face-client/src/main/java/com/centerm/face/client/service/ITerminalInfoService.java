package com.centerm.face.client.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.centerm.face.client.domain.TerminalInfo;
import java.util.List;

/**
 * 终端Service接口
 * 
 * @author lidonghang
 * @date 2021-06-25
 */
public interface ITerminalInfoService extends IService<TerminalInfo>{
    /**
     * 查询终端
     * 
     * @param id 终端ID
     * @return 终端
     */
    public TerminalInfo selectTerminalInfoById(Long id);

    /**
     * 查询终端列表
     * 
     * @param terminalInfo 终端
     * @return 终端集合
     */
    public List<TerminalInfo> selectTerminalInfoList(TerminalInfo terminalInfo);

}
