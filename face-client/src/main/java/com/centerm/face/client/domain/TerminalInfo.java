package com.centerm.face.client.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import org.miser.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import java.util.Date;
import org.miser.common.core.domain.BaseEntity;

/**
 * 终端对象 face_terminal_info
 *
 * @author lidonghang
 * @date 2021-06-25
 */
@ApiModel(value = "终端对象")
@Data
@ToString
@TableName("face_terminal_info")
@Accessors(chain = true)
public class TerminalInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;


    /** 自增主键 */
    @ApiModelProperty(value = "${comment}")
    private Long id;

    /** 商户编号 */
    @Excel(name = "商户编号")
    @ApiModelProperty(value = "商户编号")
    private Long merchantId;

    /** 终端序列号 */
    @Excel(name = "终端序列号")
    @ApiModelProperty(value = "终端序列号")
    private String terminalSn;

    /** 所属的客户端编号 */
    @Excel(name = "所属的客户端编号")
    @ApiModelProperty(value = "所属的客户端编号")
    private String clientId;

    /** 终端名 */
    @Excel(name = "终端名")
    @ApiModelProperty(value = "终端名")
    private String terminalName;

    /** 状态（0：无效，1：有效） */
    @Excel(name = "状态", readConverterExp = "0=：无效，1：有效")
    @ApiModelProperty(value = "状态")
    private Integer status;





}
