package com.centerm.face.client.mapper;

import com.centerm.face.client.domain.ClientInfo;
import java.util.List;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 客户端Mapper接口
 * 
 * @author lidonghang
 * @date 2021-06-25
 */
@Repository
public interface ClientInfoMapper  extends BaseMapper<ClientInfo>{
    /**
     * 查询客户端
     * 
     * @param id 客户端ID
     * @return 客户端
     */
    public ClientInfo selectClientInfoById(Long id);

    /**
     * 查询客户端列表
     * 
     * @param clientInfo 客户端
     * @return 客户端集合
     */
    public List<ClientInfo> selectClientInfoList(ClientInfo clientInfo);


}
