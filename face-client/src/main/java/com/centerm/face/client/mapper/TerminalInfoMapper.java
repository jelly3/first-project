package com.centerm.face.client.mapper;

import com.centerm.face.client.domain.TerminalInfo;
import java.util.List;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 终端Mapper接口
 * 
 * @author lidonghang
 * @date 2021-06-25
 */
@Repository
public interface TerminalInfoMapper  extends BaseMapper<TerminalInfo>{
    /**
     * 查询终端
     * 
     * @param id 终端ID
     * @return 终端
     */
    public TerminalInfo selectTerminalInfoById(Long id);

    /**
     * 查询终端列表
     * 
     * @param terminalInfo 终端
     * @return 终端集合
     */
    public List<TerminalInfo> selectTerminalInfoList(TerminalInfo terminalInfo);


}
